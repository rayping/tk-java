package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Shopcat;
import info.batcloud.fanli.core.enums.ShopcatStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ShopcatRepository extends CrudRepository<Shopcat, Long> {

    List<Shopcat> findByParentIdAndStatusNotOrderByIdxAsc(long parentId, ShopcatStatus shopcatStatus);

    int countByParentId(long parentId);

    List<Shopcat> findByTitleLikeAndStatusNotOrderByIdxAsc(String title, ShopcatStatus shopcatStatus);

    List<Shopcat> findByParentIdInAndStatusNot(List<Long> parentIds, ShopcatStatus shopcatStatus);

    List<Shopcat> findByParentIdAndStatusOrderByIdxAsc(long parentId, ShopcatStatus status);

    List<Shopcat> findByParentIdInAndStatusOrderByIdxAsc(List<Long> parentIdList, ShopcatStatus status);

    int countByParentIdAndTitle(long parentId, String title);

    int countByParentIdAndTitleAndIdNotAndStatusNot(long parentId, String title, long id, ShopcatStatus status);

    List<Shopcat> findByPathStartingWith(String path);

    List<Shopcat> findByPathStartingWithAndStatusOrderByIdAsc(String path, ShopcatStatus status);

    List<Shopcat> findByStatus(ShopcatStatus status);

    @Query(value = "SELECT * FROM shopcat s  WHERE s.`status`='ENABLED' and NOT exists(SELECT 1 FROM shopcat s1 WHERE s1.parent_id=s.id)", nativeQuery = true)
    List<Shopcat> findEnabledLeaf();

    List<Shopcat> findByIconIsNullAndStatusNot(ShopcatStatus status);
}
