package info.batcloud.fanli.core.domain;

public class TbkItemBuyParams {
    private String adzoneId;
    private String unionId;
    private String pid;
    private String clickUrl;
    private String tljSendUrl;
    private String tljRightsId;
    private String specialId;

    public String getSpecialId() {
        return specialId;
    }

    public void setSpecialId(String specialId) {
        this.specialId = specialId;
    }

    public String getTljSendUrl() {
        return tljSendUrl;
    }

    public void setTljSendUrl(String tljSendUrl) {
        this.tljSendUrl = tljSendUrl;
    }

    public String getTljRightsId() {
        return tljRightsId;
    }

    public void setTljRightsId(String tljRightsId) {
        this.tljRightsId = tljRightsId;
    }

    public String getAdzoneId() {
        return adzoneId;
    }

    public void setAdzoneId(String adzoneId) {
        this.adzoneId = adzoneId;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getClickUrl() {
        return clickUrl;
    }

    public void setClickUrl(String clickUrl) {
        this.clickUrl = clickUrl;
    }
}
