package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum CommissionOrderStatus implements EnumTitle {

    WAIT_PAY, PAID, WAIT_SETTLE, SETTLED, INVALID;


    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, "", null);
    }

}
