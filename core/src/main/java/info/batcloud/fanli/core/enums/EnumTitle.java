package info.batcloud.fanli.core.enums;

public interface EnumTitle {

    String getTitle();

}
