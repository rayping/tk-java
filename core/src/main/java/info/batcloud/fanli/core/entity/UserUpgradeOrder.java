package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.UserLevel;
import info.batcloud.fanli.core.enums.UserLevelUpgradeOrderStatus;
import info.batcloud.fanli.core.enums.WalletFlowDetailType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@DiscriminatorValue("UserUpgradeOrder")
public class UserUpgradeOrder extends Order{

    private int month;

    @Enumerated(EnumType.STRING)
    private UserLevel userLevel;

    @Enumerated(EnumType.STRING)
    private UserLevelUpgradeOrderStatus status;

    public UserLevel getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(UserLevel userLevel) {
        this.userLevel = userLevel;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public UserLevelUpgradeOrderStatus getStatus() {
        return status;
    }

    public void setStatus(UserLevelUpgradeOrderStatus status) {
        this.status = status;
    }

    @Override
    public WalletFlowDetailType walletFlowDetailType() {
        return WalletFlowDetailType.PLUS_UPGRADE_ORDER_PAY;
    }
}
