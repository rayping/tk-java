package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.constants.CacheNameConstants;
import info.batcloud.fanli.core.domain.TaobaoMaterial;
import info.batcloud.fanli.core.dto.TaobaoMaterialCatDTO;
import info.batcloud.fanli.core.entity.TaobaoMaterialCat;
import info.batcloud.fanli.core.enums.TaobaoMaterialCatStatus;
import info.batcloud.fanli.core.repository.TaobaoMaterialCatRepository;
import info.batcloud.fanli.core.service.TaobaoMaterialCatService;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = CacheNameConstants.TAOBAO_MATERIAL)
public class TaobaoMaterialCatServiceImpl implements TaobaoMaterialCatService {

    @Inject
    private TaobaoMaterialCatRepository taobaoMaterialCatRepository;

    @Override
    public List<TaobaoMaterialCatDTO> findAll() {
        return of(taobaoMaterialCatRepository.findByStatus(TaobaoMaterialCatStatus.VALID));
    }

    @Override
    @Cacheable(key = "#id")
    public TaobaoMaterialCatDTO findById(long id) {
        TaobaoMaterialCat taobaoMaterialCat = taobaoMaterialCatRepository.findOne(id);
        if (taobaoMaterialCat == null) {
            return null;
        }
        TaobaoMaterialCatDTO dto = new TaobaoMaterialCatDTO();
        BeanUtils.copyProperties(taobaoMaterialCat, dto);
        return dto;
    }

    @Override
    public TaobaoMaterialCatDTO addTaobaoMaterialCat(String name, TaobaoMaterialCatStatus status) {
        TaobaoMaterialCat taobaoMaterialCat = new TaobaoMaterialCat();
        taobaoMaterialCat.setName(name);
        taobaoMaterialCat.setCreateTime(new Date());
        taobaoMaterialCat.setStatus(status);
        taobaoMaterialCatRepository.save(taobaoMaterialCat);
        return of(taobaoMaterialCat);
    }

    @Override
    @CacheEvict(key = "#id+''")
    public void updateTaobaoMaterialCat(long id, String name) {
        TaobaoMaterialCat taobaoMaterialCat = taobaoMaterialCatRepository.findOne(id);
        taobaoMaterialCat.setName(name);
        taobaoMaterialCatRepository.save(taobaoMaterialCat);
    }

    @Override
    @CacheEvict(key = "#id+''")
    public void deleteById(long id) {
        TaobaoMaterialCat taobaoMaterialCat = taobaoMaterialCatRepository.findOne(id);
        taobaoMaterialCat.setStatus(TaobaoMaterialCatStatus.DELETED);
        taobaoMaterialCatRepository.save(taobaoMaterialCat);
    }

    @Override
    @CacheEvict(key = "'MATERIAL_LIST_' + #id")
    public void setMaterialList(long id, List<TaobaoMaterial> list) {
        TaobaoMaterialCat taobaoMaterialCat = taobaoMaterialCatRepository.findOne(id);
        taobaoMaterialCat.setMaterialList(list);
        taobaoMaterialCatRepository.save(taobaoMaterialCat);
    }

    @Override
    @Cacheable(key = "'MATERIAL_LIST_' + #id")
    public List<TaobaoMaterial> getMaterialList(long id) {
        TaobaoMaterialCat taobaoMaterialCat = taobaoMaterialCatRepository.findOne(id);
        return taobaoMaterialCat.getMaterialList();
    }

    private List<TaobaoMaterialCatDTO> of(List<TaobaoMaterialCat> taobaoMaterialCatList) {
        return taobaoMaterialCatList.stream().map(c -> of(c)).collect(Collectors.toList());
    }

    private TaobaoMaterialCatDTO of(TaobaoMaterialCat taobaoMaterialCat) {
        TaobaoMaterialCatDTO dto = new TaobaoMaterialCatDTO();
        BeanUtils.copyProperties(taobaoMaterialCat, dto);
        return dto;
    }
}
