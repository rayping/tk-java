package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Region;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RegionRepository extends CrudRepository<Region, Long> {

    List<Region> findByParentId(long parentId);

    List<Region> findByLevel(int level);

}
