package info.batcloud.fanli.core.domain;

import java.io.Serializable;

public class TaobaoMaterial implements Serializable {

    private String name;

    private String materialId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }
}
