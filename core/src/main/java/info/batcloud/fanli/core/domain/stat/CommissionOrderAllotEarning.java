package info.batcloud.fanli.core.domain.stat;

public class CommissionOrderAllotEarning {

    private Long commissionOrderId;
    //总人数
    private int userCommissionOrderNum;
    //总比例
    private float totalUserCommissionRate;
    //总佣金
    private float totalUserCommissionFee;
    //总奖励比例
    private float totalUserRewardRate;
    //总奖励
    private float totalUserRewardFee;
    //总结算佣金
    private float totalSettledCommissionFee;
    //总结算奖励
    private float totalSettledRewardFee;
    //总结算金额
    private float totalSettledFee;

    public float getTotalSettledFee() {
        return totalSettledFee;
    }

    public void setTotalSettledFee(float totalSettledFee) {
        this.totalSettledFee = totalSettledFee;
    }

    public Long getCommissionOrderId() {
        return commissionOrderId;
    }

    public void setCommissionOrderId(Long commissionOrderId) {
        this.commissionOrderId = commissionOrderId;
    }

    public int getUserCommissionOrderNum() {
        return userCommissionOrderNum;
    }

    public void setUserCommissionOrderNum(int userCommissionOrderNum) {
        this.userCommissionOrderNum = userCommissionOrderNum;
    }

    public float getTotalUserCommissionRate() {
        return totalUserCommissionRate;
    }

    public void setTotalUserCommissionRate(float totalUserCommissionRate) {
        this.totalUserCommissionRate = totalUserCommissionRate;
    }

    public float getTotalUserRewardRate() {
        return totalUserRewardRate;
    }

    public void setTotalUserRewardRate(float totalUserRewardRate) {
        this.totalUserRewardRate = totalUserRewardRate;
    }

    public float getTotalSettledCommissionFee() {
        return totalSettledCommissionFee;
    }

    public void setTotalSettledCommissionFee(float totalSettledCommissionFee) {
        this.totalSettledCommissionFee = totalSettledCommissionFee;
    }

    public float getTotalSettledRewardFee() {
        return totalSettledRewardFee;
    }

    public void setTotalSettledRewardFee(float totalSettledRewardFee) {
        this.totalSettledRewardFee = totalSettledRewardFee;
    }

    public float getTotalUserCommissionFee() {
        return totalUserCommissionFee;
    }

    public void setTotalUserCommissionFee(float totalUserCommissionFee) {
        this.totalUserCommissionFee = totalUserCommissionFee;
    }

    public float getTotalUserRewardFee() {
        return totalUserRewardFee;
    }

    public void setTotalUserRewardFee(float totalUserRewardFee) {
        this.totalUserRewardFee = totalUserRewardFee;
    }
}
