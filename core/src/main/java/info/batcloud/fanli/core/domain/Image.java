package info.batcloud.fanli.core.domain;

import info.batcloud.fanli.core.helper.OSSImageHelper;

public class Image {

    private String key;

    public Image() {

    }

    public Image(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return OSSImageHelper.toUrl(this.key);
    }

    public String getLargeUrl() {
        return OSSImageHelper.toLargeUrl(this.key);
    }

    public String getMediumUrl() {
        return OSSImageHelper.toMediumUrl(this.key);
    }

    public String getSmallUrl() {
        return OSSImageHelper.toSmallUrl(this.key);
    }

    public String getTinyUrl() {
        return OSSImageHelper.toTinyUrl(this.key);
    }

}
