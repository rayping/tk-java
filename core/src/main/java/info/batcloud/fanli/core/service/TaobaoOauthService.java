package info.batcloud.fanli.core.service;


import info.batcloud.fanli.core.dto.TaobaoOauthDTO;

import java.util.List;

public interface TaobaoOauthService {

    String findAccessTokenByUserIdAndTaobaoUserId(long userId, String taobaoUserId);

    List<TaobaoOauthDTO> findByUserId(long userId);

    class AppAuthParam {
        private String avatarUrl;
        private String nick;
        private String openId;
        private String openSid;
        private String topAccessToken;
        private String topAuthCode;

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public String getOpenId() {
            return openId;
        }

        public void setOpenId(String openId) {
            this.openId = openId;
        }

        public String getOpenSid() {
            return openSid;
        }

        public void setOpenSid(String openSid) {
            this.openSid = openSid;
        }

        public String getTopAccessToken() {
            return topAccessToken;
        }

        public void setTopAccessToken(String topAccessToken) {
            this.topAccessToken = topAccessToken;
        }

        public String getTopAuthCode() {
            return topAuthCode;
        }

        public void setTopAuthCode(String topAuthCode) {
            this.topAuthCode = topAuthCode;
        }
    }
}


