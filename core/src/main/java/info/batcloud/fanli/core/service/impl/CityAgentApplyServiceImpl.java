package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.constants.MessageKeyConstants;
import info.batcloud.fanli.core.entity.CityAgentApply;
import info.batcloud.fanli.core.enums.AgentApplyStatus;
import info.batcloud.fanli.core.exception.BizException;
import info.batcloud.fanli.core.repository.CityAgentApplyRepository;
import info.batcloud.fanli.core.repository.UserRepository;
import info.batcloud.fanli.core.service.CityAgentApplyService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Date;

@Service
public class CityAgentApplyServiceImpl implements CityAgentApplyService {

    @Inject
    private CityAgentApplyRepository cityAgentApplyRepository;

    @Inject
    private UserRepository userRepository;

    @Override
    public void apply(ApplyParam param) {
        if(cityAgentApplyRepository.countByUserIdAndStatus(param.getUserId(),
                AgentApplyStatus.WAIT_VERIFY) > 0) {
            throw new BizException(MessageKeyConstants.AGENT_APPLY_IS_WAIT_VERIFY);
        }
        CityAgentApply agentApply = new CityAgentApply();
        agentApply.setCityId(param.getCityId());
        agentApply.setUser(userRepository.findOne(param.getUserId()));
        agentApply.setCreateTime(new Date());
        agentApply.setContent(param.getContent());
        agentApply.setPhone(param.getPhone());
        agentApply.setStatus(AgentApplyStatus.WAIT_VERIFY);
        agentApply.setContactName(param.getContactName());
        cityAgentApplyRepository.save(agentApply);
    }
}
