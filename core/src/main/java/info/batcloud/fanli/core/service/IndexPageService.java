package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.dto.AdvDTO;
import info.batcloud.fanli.core.operation.center.domain.PageJump;
import info.batcloud.fanli.core.operation.center.domain.PicAdv;
import info.batcloud.fanli.core.operation.center.domain.TabbarNav;
import info.batcloud.fanli.core.settings.IndexFloorSetting;

import java.util.List;

public interface IndexPageService {

    Banner banner();

    List<PicAdv> quickNavList();

    List<Floor> floorList();

    List<AdvDTO> quickNavAdvList();

    List<TabbarNav> tabbarNavList();

    void clearCache();

    class Banner {
        private List<AdvDTO> advList;

        public List<AdvDTO> getAdvList() {
            return advList;
        }

        public void setAdvList(List<AdvDTO> advList) {
            this.advList = advList;
        }

    }

    class CommissionItemModel {
        private String picUrl;
        private Long id;
        private String title;
        private Float price;
        private Float couponValue;
        private Float originPrice;

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Float getPrice() {
            return price;
        }

        public void setPrice(Float price) {
            this.price = price;
        }

        public Float getCouponValue() {
            return couponValue;
        }

        public void setCouponValue(Float couponValue) {
            this.couponValue = couponValue;
        }

        public Float getOriginPrice() {
            return originPrice;
        }

        public void setOriginPrice(Float originPrice) {
            this.originPrice = originPrice;
        }
    }

    class Floor {
        private String title;

        private boolean showTitle;

        private String titlePic;

        private int titlePicWidth;

        private int titlePicHeight;

        private List<Row> rowList;

        public String getTitlePic() {
            return titlePic;
        }

        public void setTitlePic(String titlePic) {
            this.titlePic = titlePic;
        }

        public int getTitlePicWidth() {
            return titlePicWidth;
        }

        public void setTitlePicWidth(int titlePicWidth) {
            this.titlePicWidth = titlePicWidth;
        }

        public int getTitlePicHeight() {
            return titlePicHeight;
        }

        public void setTitlePicHeight(int titlePicHeight) {
            this.titlePicHeight = titlePicHeight;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public boolean isShowTitle() {
            return showTitle;
        }

        public void setShowTitle(boolean showTitle) {
            this.showTitle = showTitle;
        }

        public List<Row> getRowList() {
            return rowList;
        }

        public void setRowList(List<Row> rowList) {
            this.rowList = rowList;
        }
    }

    class Row {
        private Integer height;
        private List<Showcase> showcaseList;

        public List<Showcase> getShowcaseList() {
            return showcaseList;
        }

        public Integer getHeight() {
            return height;
        }

        public void setHeight(Integer height) {
            this.height = height;
        }

        public void setShowcaseList(List<Showcase> showcaseList) {
            this.showcaseList = showcaseList;
        }
    }

    class Showcase extends PageJump {
        private String title;
        private String slogan;
        private boolean showTitle;
        private boolean alignCenter;
        private String pic;
        private int picWidth;
        private int picHeight;
        private Float width;
        private boolean showItem;
        private List<CommissionItemModel> commissionItemList;

        private boolean showPrice;
        private boolean showSalePrice;

        private int itemShowNum;

        private int rowItemNum;

        private int rowNum;

        private IndexFloorSetting.Showcase.ShowType showType;

        private IndexFloorSetting.PicLayout picLayout;

        public int getItemShowNum() {
            return itemShowNum;
        }

        public void setItemShowNum(int itemShowNum) {
            this.itemShowNum = itemShowNum;
        }

        public int getRowItemNum() {
            return rowItemNum;
        }

        public void setRowItemNum(int rowItemNum) {
            this.rowItemNum = rowItemNum;
        }

        public int getRowNum() {
            return rowNum;
        }

        public void setRowNum(int rowNum) {
            this.rowNum = rowNum;
        }

        public IndexFloorSetting.Showcase.ShowType getShowType() {
            return showType;
        }

        public void setShowType(IndexFloorSetting.Showcase.ShowType showType) {
            this.showType = showType;
        }

        public IndexFloorSetting.PicLayout getPicLayout() {
            return picLayout;
        }

        public void setPicLayout(IndexFloorSetting.PicLayout picLayout) {
            this.picLayout = picLayout;
        }

        public boolean isShowItem() {
            return showItem;
        }

        public void setShowItem(boolean showItem) {
            this.showItem = showItem;
        }

        public boolean isShowTitle() {
            return showTitle;
        }

        public void setShowTitle(boolean showTitle) {
            this.showTitle = showTitle;
        }

        public boolean isAlignCenter() {
            return alignCenter;
        }

        public void setAlignCenter(boolean alignCenter) {
            this.alignCenter = alignCenter;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public int getPicWidth() {
            return picWidth;
        }

        public void setPicWidth(int picWidth) {
            this.picWidth = picWidth;
        }

        public int getPicHeight() {
            return picHeight;
        }

        public void setPicHeight(int picHeight) {
            this.picHeight = picHeight;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlogan() {
            return slogan;
        }

        public void setSlogan(String slogan) {
            this.slogan = slogan;
        }

        public Float getWidth() {
            return width;
        }

        public void setWidth(Float width) {
            this.width = width;
        }

        public List<CommissionItemModel> getCommissionItemList() {
            return commissionItemList;
        }

        public void setCommissionItemList(List<CommissionItemModel> commissionItemList) {
            this.commissionItemList = commissionItemList;
        }

        public boolean isShowPrice() {
            return showPrice;
        }

        public void setShowPrice(boolean showPrice) {
            this.showPrice = showPrice;
        }

        public boolean isShowSalePrice() {
            return showSalePrice;
        }

        public void setShowSalePrice(boolean showSalePrice) {
            this.showSalePrice = showSalePrice;
        }
    }
}
