package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.FlashSaleItemStatus;
import info.batcloud.fanli.core.enums.UserLevel;
import org.apache.commons.lang3.time.DateUtils;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.Date;

public class FlashSaleItemDTO {

    private Long id;

    private String date;

    private String season; //场次

    private CommissionItemDTO item;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private FlashSaleItemStatus status;

    private String slogan;

    private Integer totalNum;

    private Integer drawnNum;

    private Date lastDrawnTime;

    private UserLevel userLevel;

    private boolean userRemind;

    private boolean userLevelFit;

    private float returnFee;

    private boolean drawn;

    private String startTip;

    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public float getReturnFee() {
        return returnFee;
    }

    public void setReturnFee(float returnFee) {
        this.returnFee = returnFee;
    }

    public String getStartTip() {
        return startTip;
    }

    public void setStartTip(String startTip) {
        this.startTip = startTip;
    }

    public boolean isDrawn() {
        return drawn;
    }

    public void setDrawn(boolean drawn) {
        this.drawn = drawn;
    }

    public boolean isUserLevelFit() {
        return userLevelFit;
    }

    public void setUserLevelFit(boolean userLevelFit) {
        this.userLevelFit = userLevelFit;
    }

    public boolean isUserRemind() {
        return userRemind;
    }

    public void setUserRemind(boolean userRemind) {
        this.userRemind = userRemind;
    }

    public String getUserLevelTitle() {
        return userLevel == null ? null : userLevel.getTitle();
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getDrawnNum() {
        return drawnNum;
    }

    public void setDrawnNum(Integer drawnNum) {
        this.drawnNum = drawnNum;
    }

    public Date getLastDrawnTime() {
        return lastDrawnTime;
    }

    public void setLastDrawnTime(Date lastDrawnTime) {
        this.lastDrawnTime = lastDrawnTime;
    }

    public UserLevel getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(UserLevel userLevel) {
        this.userLevel = userLevel;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public CommissionItemDTO getItem() {
        return item;
    }

    public void setItem(CommissionItemDTO item) {
        this.item = item;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public FlashSaleItemStatus getStatus() {
        return status;
    }

    public void setStatus(FlashSaleItemStatus status) {
        this.status = status;
    }

    public Integer getRemainSeconds() {
        String startTimeStr = this.getDate() + " " + this.getSeason();
        Date startTime = null;
        try {
            startTime = DateUtils.parseDate(startTimeStr.replaceAll(" +", " "), "yyyy-MM-dd HH:mm");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Float.valueOf((startTime.getTime() - System.currentTimeMillis()) / 1000).intValue();
    }

    @Override
    public String toString() {
        return "FlashSaleItemDTO{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", season='" + season + '\'' +
                ", item=" + item +
                ", createTime=" + createTime +
                ", status=" + status +
                ", slogan='" + slogan + '\'' +
                ", totalNum=" + totalNum +
                ", drawnNum=" + drawnNum +
                ", lastDrawnTime=" + lastDrawnTime +
                ", userLevel=" + userLevel +
                ", userRemind=" + userRemind +
                ", userLevelFit=" + userLevelFit +
                ", returnFee=" + returnFee +
                ", drawn=" + drawn +
                ", startTip='" + startTip + '\'' +
                ", label='" + label + '\'' +
                '}';
    }
}
