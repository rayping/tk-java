package info.batcloud.fanli.core.service;

import java.io.OutputStream;

public interface CommissionItemSharePicGenService {

    String genSharePicSvgXml(long itemId,long userId, String picUrl);

    void genSharePic(long itemId, long userId, String picUrl, OutputStream os);

}
