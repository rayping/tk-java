package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum SearchFrom implements EnumTitle, EnumHelp {

    TAOBAO, JD, PDD, SITE;
    public String title;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

    @Override
    public String getHelp() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + ".help." + this.name(), null, null);
    }
}
