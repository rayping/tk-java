package info.batcloud.fanli.core.domain;

import info.batcloud.fanli.core.enums.EcomPlat;

public class MaterialConfig {
    private Long channelId;
    private Float startPrice;
    private Float endPrice;
    private EcomPlat ecomPlat;
    private Boolean freeShipment;
    private Float minCommissionFee;
    private Boolean coupon;
    private Long catId;
    private Integer minSales;
    private Boolean recommend;

    public Boolean getRecommend() {
        return recommend;
    }

    public void setRecommend(Boolean recommend) {
        this.recommend = recommend;
    }

    public Integer getMinSales() {
        return minSales;
    }

    public void setMinSales(Integer minSales) {
        this.minSales = minSales;
    }

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public Boolean getCoupon() {
        return coupon;
    }

    public void setCoupon(Boolean coupon) {
        this.coupon = coupon;
    }

    public Float getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Float startPrice) {
        this.startPrice = startPrice;
    }

    public Float getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(Float endPrice) {
        this.endPrice = endPrice;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public Boolean getFreeShipment() {
        return freeShipment;
    }

    public void setFreeShipment(Boolean freeShipment) {
        this.freeShipment = freeShipment;
    }

    public Float getMinCommissionFee() {
        return minCommissionFee;
    }

    public void setMinCommissionFee(Float minCommissionFee) {
        this.minCommissionFee = minCommissionFee;
    }
}