package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.enums.TimeUnit;

public interface UserUpgradeOrderService {

    long checkAndCreateUserUpgradeOrder(long userId, TimeUnit timeUnit);
}
