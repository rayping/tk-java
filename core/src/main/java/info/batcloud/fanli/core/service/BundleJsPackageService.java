package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.BundleJsPackageDTO;
import info.batcloud.fanli.core.enums.AppPlatform;

public interface BundleJsPackageService {

    BundleJsPackageDTO findLatestByPlatformAndAppVersion(AppPlatform platform, String appVersion);

    BundleJsPackageDTO findLatestByPlatform(AppPlatform platform);

    void saveBundleJsPackage(BundleJsPackageAddDto bundleJsPackageAddDto);

    Paging<BundleJsPackageDTO> search(SearchParam param);

    void publishBundleJsPackage(long id);

    void deleteById(long id);

    void setUseCache(long id, boolean useCache);
    void setIosShow(long id, boolean iosShow);

    void setShowUpdate(long id, boolean showUpdate);

    class BundleJsPackageAddDto {
        private String version;
        private String platformVersion;
        private AppPlatform platform;
        private String fileUrl;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getPlatformVersion() {
            return platformVersion;
        }

        public void setPlatformVersion(String platformVersion) {
            this.platformVersion = platformVersion;
        }

        public AppPlatform getPlatform() {
            return platform;
        }

        public void setPlatform(AppPlatform platform) {
            this.platform = platform;
        }

        public String getFileUrl() {
            return fileUrl;
        }

        public void setFileUrl(String fileUrl) {
            this.fileUrl = fileUrl;
        }
    }

    class SearchParam extends PagingParam {

        private AppPlatform platform;

        public AppPlatform getPlatform() {
            return platform;
        }

        public void setPlatform(AppPlatform platform) {
            this.platform = platform;
        }
    }

}
