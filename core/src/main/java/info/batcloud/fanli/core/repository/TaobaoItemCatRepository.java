package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.TaobaoItemCat;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface TaobaoItemCatRepository extends CrudRepository<TaobaoItemCat, Long>, JpaSpecificationExecutor<TaobaoItemCat> {

    TaobaoItemCat findByNameAndParentCid(String name, long parentCid);

}
