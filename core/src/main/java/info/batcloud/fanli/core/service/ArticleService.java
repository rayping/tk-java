package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.ArticleDTO;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public interface ArticleService {

    long createArticle(ArticleCreateParam param);

    void addShareTimes(long itemSelectionId, int times);

    void updateArticle(long id, ArticleUpdateParam param);

    void deleteById(long id);

    Paging<ArticleDTO> search(SearchParam param);

    class SearchParam extends PagingParam {
        private String keywords;

        private Sort sort;

        public Sort getSort() {
            return sort;
        }

        public void setSort(Sort sort) {
            this.sort = sort;
        }

        public enum Sort {
            ID_DESC,
            SHARE_TIMES_DESC
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }
    }

    class ArticleCreateParam {

        @NotNull
        private List<String> picList = new ArrayList<>();
        @NotNull
        private String content;

        public List<String> getPicList() {
            return picList;
        }

        public void setPicList(List<String> picList) {
            this.picList = picList;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    class ArticleUpdateParam {

        @NotNull
        private List<String> picList = new ArrayList<>();
        @NotNull
        private String content;

        public List<String> getPicList() {
            return picList;
        }

        public void setPicList(List<String> picList) {
            this.picList = picList;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

}
