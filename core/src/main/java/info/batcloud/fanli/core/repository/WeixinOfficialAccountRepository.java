package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.WeixinOfficialAccount;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface WeixinOfficialAccountRepository extends CrudRepository<WeixinOfficialAccount, Long> {

    List<WeixinOfficialAccount> findByUserIdAndDeletedIsFalse(long userId);

    void deleteByUserIdAndId(long userId, long id);

}
