package info.batcloud.fanli.core.constants;

public class CacheNameConstants {

    public static final String SYSTEM_SETTING = "SYSTEM_SETTING";

    public static final String TAOBAO_ITEM_CAT = "TAOBAO_ITEM_CAT";

    public static final String INDEX_PAGE = "INDEX_PAGE";
    public static final String WEIXIN_OFFICIAL_ACCOUNT = "WeixinOfficialAccount";

    public static final String CHANNEL = "CHANNEL";
    public static final String TAOBAO_MATERIAL = "TAOBAO_MATERIAL";

    public static final String SHOPCAT = "SHOPCAT";

    public static final String USER = "USER";
    public static final String BUNDLE_JS_PACKAGE = "BUNDLE_JS_PACKAGE";
    public static final String COMMISSION_ITEM = "COMMISSION_ITEM";
    public static final String USER_AUTHORIZE = "USER_AUTHORIZE";

    public static final String AGENT = "AGENT";

    public static final String REGION = "REGION";
    public static final String USER_SERVICE_SUBSCRIPTION = "REGION";

    public static final String MANAGER_PERMISSION = "MANAGER_PERMISSION";
    public static final String OSS = "OSS";
}
