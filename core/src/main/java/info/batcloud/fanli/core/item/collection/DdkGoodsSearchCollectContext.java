package info.batcloud.fanli.core.item.collection;

public class DdkGoodsSearchCollectContext extends Context{

    private String keyword;

    private Long optId;

    private Integer sortType;

    private boolean withCoupon;

    private Long catId;

    private String catName;

    private Integer merchantType;

    private Integer zsDuoId;

    private String goodsIdList;

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Long getOptId() {
        return optId;
    }

    public void setOptId(Long optId) {
        this.optId = optId;
    }

    public Integer getSortType() {
        return sortType;
    }

    public void setSortType(Integer sortType) {
        this.sortType = sortType;
    }

    public boolean isWithCoupon() {
        return withCoupon;
    }

    public void setWithCoupon(boolean withCoupon) {
        this.withCoupon = withCoupon;
    }

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public Integer getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(Integer merchantType) {
        this.merchantType = merchantType;
    }

    public Integer getZsDuoId() {
        return zsDuoId;
    }

    public void setZsDuoId(Integer zsDuoId) {
        this.zsDuoId = zsDuoId;
    }

    public String getGoodsIdList() {
        return goodsIdList;
    }

    public void setGoodsIdList(String goodsIdList) {
        this.goodsIdList = goodsIdList;
    }
}
