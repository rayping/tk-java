package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class ItemSelectionSetting implements Serializable {

    private int minSales;

    public int getMinSales() {
        return minSales;
    }

    public void setMinSales(int minSales) {
        this.minSales = minSales;
    }
}
