package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.FlashSaleItemDTO;
import info.batcloud.fanli.core.enums.FlashSaleItemSort;
import info.batcloud.fanli.core.enums.FlashSaleItemStatus;
import info.batcloud.fanli.core.enums.UserLevel;

import java.util.Arrays;

public interface FlashSaleItemService {

    Paging<FlashSaleItemDTO> search(SearchParam param);

    //根据id查询
    FlashSaleItemDTO findById(Long id);

    //修改
    void updateFlashSaleItem(Long id,FlashSaleItemUpdateParam param);

    void addFlashSaleItem(FlashSaleItemAddParam param);

    void setStatus(long id, FlashSaleItemStatus status);

    void setSlogan(long id, String slogan);

    boolean draw(long id, long userId);

    boolean checkDrawn(long id, long userId);

    class FlashSaleItemAddParam {
        private String date;
        private String season;
        private Long[] itemId;
        private Float[] returnFee;
        private FlashSaleItemStatus status;
        private String[] label;

        private String[] slogan;
        private Integer[] totalNum;

        private UserLevel[] userLevel;

        public String[] getLabel() {
            return label;
        }

        public void setLabel(String[] label) {
            this.label = label;
        }

        public Float[] getReturnFee() {
            return returnFee;
        }

        public void setReturnFee(Float[] returnFee) {
            this.returnFee = returnFee;
        }

        public UserLevel[] getUserLevel() {
            return userLevel;
        }

        public void setUserLevel(UserLevel[] userLevel) {
            this.userLevel = userLevel;
        }

        public Integer[] getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(Integer[] totalNum) {
            this.totalNum = totalNum;
        }

        public FlashSaleItemStatus getStatus() {
            return status;
        }

        public void setStatus(FlashSaleItemStatus status) {
            this.status = status;
        }

        public Long[] getItemId() {
            return itemId;
        }

        public void setItemId(Long[] itemId) {
            this.itemId = itemId;
        }

        public String[] getSlogan() {
            return slogan;
        }

        public void setSlogan(String[] slogan) {
            this.slogan = slogan;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getSeason() {
            return season;
        }

        public void setSeason(String season) {
            this.season = season;
        }

    }

    class FlashSaleItemUpdateParam{
        private String slogan;//标语
        private String label;//标签
        private Float returnFee;//返金额
        private Integer totalNum;//免单数
        private UserLevel userLevel;//会员级别

        public String getSlogan() {
            return slogan;
        }

        public void setSlogan(String slogan) {
            this.slogan = slogan;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Float getReturnFee() {
            return returnFee;
        }

        public void setReturnFee(Float returnFee) {
            this.returnFee = returnFee;
        }

        public Integer getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(Integer totalNum) {
            this.totalNum = totalNum;
        }

        public UserLevel getUserLevel() {
            return userLevel;
        }

        public void setUserLevel(UserLevel userLevel) {
            this.userLevel = userLevel;
        }
    }

    class SearchParam extends PagingParam {
        private String date;
        private String season;
        private Boolean finished;

        private FlashSaleItemStatus status;

        private FlashSaleItemSort sort;

        private Long checkRemindUserId;

        public Boolean getFinished() {
            return finished;
        }

        public void setFinished(Boolean finished) {
            this.finished = finished;
        }

        public Long getCheckRemindUserId() {
            return checkRemindUserId;
        }

        public void setCheckRemindUserId(Long checkRemindUserId) {
            this.checkRemindUserId = checkRemindUserId;
        }

        public FlashSaleItemStatus getStatus() {
            return status;
        }

        public void setStatus(FlashSaleItemStatus status) {
            this.status = status;
        }

        public FlashSaleItemSort getSort() {
            return sort;
        }

        public void setSort(FlashSaleItemSort sort) {
            this.sort = sort;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getSeason() {
            return season;
        }

        public void setSeason(String season) {
            this.season = season;
        }
    }

}
