package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.enums.UserLevel;
import info.batcloud.fanli.core.service.CommissionService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.CommissionAllotSetting;
import info.batcloud.fanli.core.settings.CommissionSettlementSetting;
import info.batcloud.fanli.core.settings.UserLevelProfitPerformanceSetting;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class CommissionServiceImpl implements CommissionService {

    @Inject
    private SystemSettingService systemSettingService;

    @Override
    public float determineCommissionRate(float remainCommissionRate, UserLevel userLevel) {
        UserLevelProfitPerformanceSetting userLevelProfitPerformanceSetting = systemSettingService.findActiveSetting(UserLevelProfitPerformanceSetting.class);
        return userLevelProfitPerformanceSetting.findByUserLevel(userLevel).getCommissionRate() * remainCommissionRate / 100;
    }

    @Override
    public float determineRewardCommissionFee(float value) {
        CommissionSettlementSetting settlementSetting = systemSettingService.findActiveSetting(CommissionSettlementSetting.class);
        return settlementSetting.getRewardCommissionRate() * value / 100;
    }

    @Override
    public float determineRewardCommissionFee(float rewardCommissionRate, float value) {
        return rewardCommissionRate * value / 100;
    }
}
