package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.CollectStatus;
import info.batcloud.fanli.core.enums.CollectionSourceStatus;
import info.batcloud.fanli.core.enums.CollectionSourceType;

import javax.persistence.*;
import java.util.Date;

@Entity
public class CollectionSource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Enumerated(EnumType.STRING)
    private CollectionSourceStatus status;

    private Date createTime;

    private Date lastCollectStartTime;

    private Date lastCollectEndTime;

    private long collectSeconds;//运行秒数

    @Enumerated(EnumType.STRING)
    private CollectStatus collectStatus;

    @Enumerated(EnumType.STRING)
    private CollectionSourceType type;

    private String collectMsg;

    private Date startTime;

    private Date stopTime;

    private String keyword; //采集关键字

    private long cateId;

    //抓取的最大页数
    private int maxPage;

    //每页抓取的数量
    private int pageSize;

    public long getCateId() {
        return cateId;
    }

    public void setCateId(long cateId) {
        this.cateId = cateId;
    }

    public String getTypeTitle() {
        return getType() == null ? null : getType().title;
    }

    public CollectionSourceType getType() {
        return type;
    }

    public void setType(CollectionSourceType type) {
        this.type = type;
    }

    public String getCollectMsg() {
        return collectMsg;
    }

    public void setCollectMsg(String collectMsg) {
        this.collectMsg = collectMsg;
    }

    public CollectStatus getCollectStatus() {
        return collectStatus;
    }

    public void setCollectStatus(CollectStatus collectStatus) {
        this.collectStatus = collectStatus;
    }

    public long getCollectSeconds() {
        return collectSeconds;
    }

    public void setCollectSeconds(long collectSeconds) {
        this.collectSeconds = collectSeconds;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CollectionSourceStatus getStatus() {
        return status;
    }

    public void setStatus(CollectionSourceStatus status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastCollectStartTime() {
        return lastCollectStartTime;
    }

    public void setLastCollectStartTime(Date lastCollectStartTime) {
        this.lastCollectStartTime = lastCollectStartTime;
    }

    public Date getLastCollectEndTime() {
        return lastCollectEndTime;
    }

    public void setLastCollectEndTime(Date lastCollectEndTime) {
        this.lastCollectEndTime = lastCollectEndTime;
    }

    public String toString() {
        return String.format("id:%d, keyword:%s", getId(), getKeyword());
    }

}
