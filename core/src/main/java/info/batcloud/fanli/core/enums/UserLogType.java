package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum UserLogType implements EnumTitle{

    LOGIN, ACTIVE, USER_LEVEL_CHANGE;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "."
                + this.name(), null, "", null);
    }
}
