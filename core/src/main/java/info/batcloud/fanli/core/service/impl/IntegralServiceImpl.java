package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.service.IntegralService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.IntegralSetting;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class IntegralServiceImpl implements IntegralService {

    @Inject
    private SystemSettingService systemSettingService;

    @Override
    public float toMoney(float integral) {
        IntegralSetting integralSetting = systemSettingService.findActiveSetting(IntegralSetting.class);
        return integralSetting.getEachIntegralExchangeToMoney() * integral;
    }

    @Override
    public float toIntegral(float money) {
        IntegralSetting integralSetting = systemSettingService.findActiveSetting(IntegralSetting.class);
        return money / integralSetting.getEachIntegralExchangeToMoney();
    }
}
