package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.entity.Region;

import java.util.List;

public interface RegionService {

    List<Region> findByParentId(long parentId);

    List<Region> findByLevel(int level);

    Region findById(long id);

    boolean sync();
}
