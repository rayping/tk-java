package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.WalletFlowDetailDTO;
import info.batcloud.fanli.core.enums.WalletFlowDetailType;
import info.batcloud.fanli.core.enums.WalletValueType;

public interface WalletFlowDetailService {

    Paging<WalletFlowDetailDTO> search(SearchParam param);

    class SearchParam extends PagingParam {
        private Long userId;
        private WalletValueType valueType;
        private WalletFlowDetailType type;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public WalletValueType getValueType() {
            return valueType;
        }

        public void setValueType(WalletValueType valueType) {
            this.valueType = valueType;
        }

        public WalletFlowDetailType getType() {
            return type;
        }

        public void setType(WalletFlowDetailType type) {
            this.type = type;
        }
    }

}
