package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.SellerPromotionItemDepositOrder;
import info.batcloud.fanli.core.enums.SellerPromotionItemDepositOrderStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SellerPromotionItemDepositOrderRepository extends PagingAndSortingRepository<SellerPromotionItemDepositOrder, Long>, JpaSpecificationExecutor<SellerPromotionItemDepositOrder> {
    int countBySellerPromotionItemIdAndStatus(long sellerPromotionItemId, SellerPromotionItemDepositOrderStatus status);
    int countByAlipayTransferTradeNo(String alipayTransferTradeNo);
}
