package info.batcloud.fanli.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.ItemSelectionStatus;
import info.batcloud.fanli.core.enums.ItemSelectionType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

//选品库
@Entity
public class ItemSelection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private Channel channel;
    private boolean clearChannelItem;
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private Shopcat shopcat;
    @Enumerated(EnumType.STRING)
    private EcomPlat ecomPlat;
    private boolean recommend;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastSyncTime; //最后同步时间
    @Enumerated(EnumType.STRING)
    private ItemSelectionStatus status;
    private boolean autoFetch;
    private int maxPage;

    //最后更新时间
    private Date lastFetchTime;
    //最后更新数量
    private int lastFetchNum;
    private String errorMsg;
    private String config;

    @Enumerated(EnumType.STRING)
    private ItemSelectionType type;

    public boolean isClearChannelItem() {
        return clearChannelItem;
    }

    public void setClearChannelItem(boolean clearChannelItem) {
        this.clearChannelItem = clearChannelItem;
    }

    public boolean isRecommend() {
        return recommend;
    }

    public void setRecommend(boolean recommend) {
        this.recommend = recommend;
    }

    public ItemSelectionType getType() {
        return type;
    }

    public void setType(ItemSelectionType type) {
        this.type = type;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Date getLastFetchTime() {
        return lastFetchTime;
    }

    public void setLastFetchTime(Date lastFetchTime) {
        this.lastFetchTime = lastFetchTime;
    }

    public int getLastFetchNum() {
        return lastFetchNum;
    }

    public void setLastFetchNum(int lastFetchNum) {
        this.lastFetchNum = lastFetchNum;
    }

    public boolean isAutoFetch() {
        return autoFetch;
    }

    public void setAutoFetch(boolean autoFetch) {
        this.autoFetch = autoFetch;
    }

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public Shopcat getShopcat() {
        return shopcat;
    }

    public void setShopcat(Shopcat shopcat) {
        this.shopcat = shopcat;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastSyncTime() {
        return lastSyncTime;
    }

    public void setLastSyncTime(Date lastSyncTime) {
        this.lastSyncTime = lastSyncTime;
    }

    public ItemSelectionStatus getStatus() {
        return status;
    }

    public void setStatus(ItemSelectionStatus status) {
        this.status = status;
    }
}
