package info.batcloud.fanli.core;

import com.alibaba.fastjson.parser.ParserConfig;
import com.zaxxer.hikari.HikariDataSource;
import info.batcloud.fanli.core.config.AlipayConfig;
import info.batcloud.fanli.core.config.FanliConfig;
import info.batcloud.fanli.core.constants.JosConstatns;
import info.batcloud.fanli.core.context.StaticContext;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.SearchFrom;
import info.batcloud.fanli.core.fastjson.parser.deserializer.EnumDeserializer;
import info.batcloud.fanli.core.jos.JosApp;
import info.batcloud.fanli.core.weixin.WxaConfig;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@EnableAutoConfiguration
@ServletComponentScan({"com.ctospace.archit.servlet", "info.batcloud.fanli"})
@EnableCaching
@ComponentScan({"info.batcloud.fanli"})
@EnableTransactionManagement
@EntityScan(basePackages = "info.batcloud.fanli.core.entity")
@EnableJpaRepositories(basePackages = "info.batcloud.fanli.core.repository")
@EnableScheduling
public class CommonConfig {

    static {
        ParserConfig.getGlobalInstance().putDeserializer(EcomPlat.class, new EnumDeserializer(EcomPlat.class));
        ParserConfig.getGlobalInstance().putDeserializer(SearchFrom.class, new EnumDeserializer(SearchFrom.class));
    }

    @Bean
    @ConfigurationProperties("fanli")
    public FanliConfig fanliConfig() {
        return new FanliConfig();
    }

    @Bean
    @ConfigurationProperties(prefix = "datasource")
    public DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        return dataSource;
    }

    @Bean
    @ConfigurationProperties("alipay")
    public AlipayConfig alipayConfig() {
        return new AlipayConfig();
    }

    @Bean
    @ConfigurationProperties("weixin.wxa")
    public WxaConfig wxaConfig() {
        return new WxaConfig();
    }

    @Bean(name = JosConstatns.UNION_APP)
    @ConfigurationProperties("jos.union")
    public JosApp unionApp() {
        return new JosApp();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    //i18n
    @Bean
    public ReloadableResourceBundleMessageSource resourceBundleMessageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("classpath:messages/enums", "classpath:messages/exceptions", "classpath:messages/messages");
        messageSource.setDefaultEncoding("utf8");
        messageSource.setAlwaysUseMessageFormat(true);
        StaticContext.messageSource = messageSource;
        return messageSource;
    }

}
