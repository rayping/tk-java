package info.batcloud.fanli.core.client;

import info.batcloud.fanli.core.domain.TaobaoLoginResult;
import info.batcloud.fanli.core.domain.utam.AuctionCode;
import info.batcloud.fanli.core.domain.utam.req.*;
import info.batcloud.fanli.core.domain.utam.res.AddItemIdToSelectionResponse;
import info.batcloud.fanli.core.domain.utam.res.ItemSelectionDeleteItemResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("taobao-union-server")
public interface TaobaoUnionClient {

    @PostMapping("/taobao-union/adzone")
    AdzoneCreateResult createAdzone(@RequestParam("newAdzoneName") String newAdzoneName);

    @PostMapping("/taobao-union/auction-code")
    @ResponseBody
    AuctionCode createAuctionCode(@RequestBody AuctionCodeCreateRequest request);

    @PostMapping("/taobao-union/fetch-order")
    boolean fetchOrder(@RequestBody TbkOrderFetchRequest request);

    @GetMapping("/taobao-union/fetch-recommend-coupon")
    boolean fetchRecommendCoupon();

    @PostMapping("/taobao-union/fetch-pull-new-details")
    boolean fetchPullNewDetails(@RequestBody FetchPullNewParam param);

    @PostMapping("/taobao-union/refresh-connection")
    boolean refreshConnection();

    @PostMapping("/taobao-union/test-connection-with-login")
    TaobaoLoginResult testConnectionWithLogin();

    @PostMapping("/taobao-union/login")
    boolean login();

    @PostMapping("/taobao-union/init")
    boolean init();

    @PostMapping("/taobao-union/add-item-id-to-selection")
    AddItemIdToSelectionResponse addItemIdToSelection(AddItemIdToSelectionRequest request);

    @PostMapping("/taobao-union/clear-item-selection")
    boolean clearItemSelection(@RequestParam("selectionId") long selectionId);

    @PostMapping("/taobao-union/delete-item-from-selection")
    ItemSelectionDeleteItemResponse deleteItemFromSelection(@RequestBody ItemSelectionDeleteItemRequest request);

    class AdzoneCreateResult {
        private String pid;
        private long siteId;
        private long adzoneId;

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public long getSiteId() {
            return siteId;
        }

        public void setSiteId(long siteId) {
            this.siteId = siteId;
        }

        public long getAdzoneId() {
            return adzoneId;
        }

        public void setAdzoneId(long adzoneId) {
            this.adzoneId = adzoneId;
        }
    }
}
