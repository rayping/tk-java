package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum FlashSaleOrderStatus implements EnumTitle{

    WAIT_VERIFY,
    WAIT_RECEIVE_GOODS,
    WAIT_SETTLE,
    SETTLED;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
