package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.CommissionOrder;
import info.batcloud.fanli.core.enums.CommissionOrderStatus;
import info.batcloud.fanli.core.enums.EcomPlat;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface CommissionOrderRepository extends CrudRepository<CommissionOrder, Long>, JpaSpecificationExecutor<CommissionOrder> {

    List<CommissionOrder> findByEcomPlatInAndOrderNoIn(List<EcomPlat> ecomPlatList, List<String> orderNoList);
    List<CommissionOrder> findTop1000ByStatusOrderByIdAsc(CommissionOrderStatus status);
    List<CommissionOrder> findTop1000ByStatusAndPlatSettledTimeBeforeOrderByIdAsc(CommissionOrderStatus status, Date endTime);
    List<CommissionOrder> findTop1000ByStatusAndEcomPlatInOrderByIdAsc(CommissionOrderStatus status, EcomPlat... ecomPlats);
    int countByUserIdAndStatusIn(long userId, CommissionOrderStatus... status);
}
