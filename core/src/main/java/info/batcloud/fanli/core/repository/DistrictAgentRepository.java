package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.DistrictAgent;
import info.batcloud.fanli.core.enums.AgentStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DistrictAgentRepository extends PagingAndSortingRepository<DistrictAgent, Long>, JpaSpecificationExecutor<DistrictAgent> {

    DistrictAgent findByDistrictIdAndStatusIsNot(long cityId, AgentStatus status);

    List<DistrictAgent> findByCityIdAndStatusIsNot(long city, AgentStatus status);

    DistrictAgent findByUserIdAndStatusIsNot(long userId, AgentStatus status);

}
