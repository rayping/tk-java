package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.ServiceItemDTO;
import info.batcloud.fanli.core.enums.Service;
import info.batcloud.fanli.core.enums.ServiceItemStatus;
import info.batcloud.fanli.core.enums.TimeUnit;

public interface ServiceItemService {

    Paging<ServiceItemDTO> search(SearchParam param);

    void saveServiceItem(ServiceItemSaveParam param);

    void setStatus(long id, ServiceItemStatus status);

    void updateServiceItem(long id, ServiceItemUpdateParam param);

    class ServiceItemUpdateParam {
        private Service service;
        private TimeUnit timeUnit;
        private ServiceItemStatus status;
        private float price;

        public Service getService() {
            return service;
        }

        public void setService(Service service) {
            this.service = service;
        }

        public TimeUnit getTimeUnit() {
            return timeUnit;
        }

        public void setTimeUnit(TimeUnit timeUnit) {
            this.timeUnit = timeUnit;
        }

        public ServiceItemStatus getStatus() {
            return status;
        }

        public void setStatus(ServiceItemStatus status) {
            this.status = status;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }
    }

    class ServiceItemSaveParam {
        private Service service;
        private TimeUnit timeUnit;
        private ServiceItemStatus status;
        private float price;

        public Service getService() {
            return service;
        }

        public void setService(Service service) {
            this.service = service;
        }

        public TimeUnit getTimeUnit() {
            return timeUnit;
        }

        public void setTimeUnit(TimeUnit timeUnit) {
            this.timeUnit = timeUnit;
        }

        public ServiceItemStatus getStatus() {
            return status;
        }

        public void setStatus(ServiceItemStatus status) {
            this.status = status;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }
    }

    class SearchParam extends PagingParam {
        private Service service;
        private ServiceItemStatus status;

        public Service getService() {
            return service;
        }

        public void setService(Service service) {
            this.service = service;
        }

        public ServiceItemStatus getStatus() {
            return status;
        }

        public void setStatus(ServiceItemStatus status) {
            this.status = status;
        }
    }

}
