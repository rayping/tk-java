package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.domain.Result;
import info.batcloud.fanli.core.dto.WalletDTO;
import info.batcloud.fanli.core.enums.WalletFlowDetailType;

public interface WalletService {

    WalletChangeResult addIntegral(long userId, float integral, WalletFlowDetailType flowDetailType, String... context);

    WalletChangeResult addMoney(long userId, float money, WalletFlowDetailType flowDetailType, String... context);

    WalletDTO findByUserId(long userId);

    float exchangeMoneyToIntegral(float money);

    WalletConsumeResult consumeMoney(long userId, float money, WalletFlowDetailType flowDetailType, String... context);

    WalletConsumeResult consumeIntegral(long userId, float integral, WalletFlowDetailType flowDetailType, String... context);

    class WalletConsumeResult extends Result {

        private WalletChangeResult walletChangeResult;

        public WalletChangeResult getWalletChangeResult() {
            return walletChangeResult;
        }

        public void setWalletChangeResult(WalletChangeResult walletChangeResult) {
            this.walletChangeResult = walletChangeResult;
        }
    }

    class WalletChangeResult {
        private long walletDetailId;
        private WalletDTO walletDTO;

        public WalletDTO getWalletDTO() {
            return walletDTO;
        }

        public void setWalletDTO(WalletDTO walletDTO) {
            this.walletDTO = walletDTO;
        }

        public long getWalletDetailId() {
            return walletDetailId;
        }

        public void setWalletDetailId(long walletDetailId) {
            this.walletDetailId = walletDetailId;
        }
    }
}
