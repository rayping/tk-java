package info.batcloud.fanli.core.service;

public interface UrlMappingService {

    String getCommissionItemShareUrl(long commissionItemId, long userId);

    String getCommissionItemDescUrl(long commissionItemId);

    String getTaobaoNewerShareUrl(String pid);

    String getSellerPromotionItemSearchBuyDescUrl(long sellerPromotionItemId);

    String getHomeWebsiteUrl();

    String getWeixinInvitationJumpUrl();

    String getWeixinInvitationBindUrl(String invitationCode);

    String getQrcodeUrl(String text);

    String flashSaleDrawHelperUrl();
}
