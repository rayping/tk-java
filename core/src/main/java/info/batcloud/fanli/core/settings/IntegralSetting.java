package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class IntegralSetting implements Serializable {

    //基础签到积分
    private int firstSignIntegral;

    //连续签到积分增量
    private int increaseSignIntegral;

    //佣金结算获取积分（根据佣金金额，每1元获得积分）
    private int commissionEachFeeIntegral;

    //邀请好友获得积分
    private int inviteEachUserIntegral;

    //每积分兑换多少钱
    private float eachIntegralExchangeToMoney;

    public float getEachIntegralExchangeToMoney() {
        return eachIntegralExchangeToMoney;
    }

    public void setEachIntegralExchangeToMoney(float eachIntegralExchangeToMoney) {
        this.eachIntegralExchangeToMoney = eachIntegralExchangeToMoney;
    }

    public int getCommissionEachFeeIntegral() {
        return commissionEachFeeIntegral;
    }

    public void setCommissionEachFeeIntegral(int commissionEachFeeIntegral) {
        this.commissionEachFeeIntegral = commissionEachFeeIntegral;
    }

    public int getInviteEachUserIntegral() {
        return inviteEachUserIntegral;
    }

    public void setInviteEachUserIntegral(int inviteEachUserIntegral) {
        this.inviteEachUserIntegral = inviteEachUserIntegral;
    }

    public int getFirstSignIntegral() {
        return firstSignIntegral;
    }

    public void setFirstSignIntegral(int firstSignIntegral) {
        this.firstSignIntegral = firstSignIntegral;
    }

    public int getIncreaseSignIntegral() {
        return increaseSignIntegral;
    }

    public void setIncreaseSignIntegral(int increaseSignIntegral) {
        this.increaseSignIntegral = increaseSignIntegral;
    }

}
