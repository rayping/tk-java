package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.service.HotSearchService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.HotSearchSetting;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HotSearchServiceImpl implements HotSearchService {

    @Inject
    private SystemSettingService systemSettingService;

    @Override
    public List<String> findHotKeywords() {
        HotSearchSetting setting = systemSettingService.findActiveSetting(HotSearchSetting.class);
        String keywords = setting.getHotKeywords();
        if (StringUtils.isBlank(keywords)) {
            return new ArrayList<>();
        }
        return Arrays.stream(keywords.trim().split("[\\n\\s]+")).filter(o -> StringUtils.isNotBlank(o)).collect(Collectors.toList());
    }
}
