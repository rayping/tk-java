package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum  CommissionOrderSort implements EnumTitle{

    ID_DESC, COMMISSION_RATE_DESC, COMMISSION_FEE_DESC, COMMISSION_RATE_ASC,
    COMMISSION_FEE_ASC, SETTLED_TIME_DESC;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}

