package info.batcloud.fanli.core.dto;

import info.batcloud.fanli.core.domain.TaobaoMaterial;
import info.batcloud.fanli.core.enums.TaobaoMaterialCatStatus;

import java.util.Date;
import java.util.List;

public class TaobaoMaterialCatDTO {

    private Long id;

    private String name;

    private Date createTime;

    private TaobaoMaterialCatStatus status;

    private List<TaobaoMaterial> materialList;

    public List<TaobaoMaterial> getMaterialList() {
        return materialList;
    }

    public void setMaterialList(List<TaobaoMaterial> materialList) {
        this.materialList = materialList;
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public TaobaoMaterialCatStatus getStatus() {
        return status;
    }

    public void setStatus(TaobaoMaterialCatStatus status) {
        this.status = status;
    }
}
