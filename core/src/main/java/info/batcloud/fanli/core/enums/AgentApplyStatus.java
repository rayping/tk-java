package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum AgentApplyStatus implements EnumTitle{

    WAIT_VERIFY,
    VERIFY_FAIL,
    VERIFY_SUCCESS;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
