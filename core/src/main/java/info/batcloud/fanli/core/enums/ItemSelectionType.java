package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum ItemSelectionType implements EnumTitle {
    TBK_DG_OPTIMUS_MATERIAL, OPTIMUS_MATERIAL, TBK_DG_MATERIAL_OPTIONAL, JD_QUERY_COUPON_GOODS, DDK_GOODS, TAOBAO_SELECTION, DATAOKE;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }
}
