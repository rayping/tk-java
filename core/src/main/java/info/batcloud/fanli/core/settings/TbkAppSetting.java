package info.batcloud.fanli.core.settings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TbkAppSetting implements Serializable {

    private List<TbkApp> tbkAppList = new ArrayList<>();

    public List<TbkApp> getTbkAppList() {
        return tbkAppList;
    }

    public void setTbkAppList(List<TbkApp> tbkAppList) {
        this.tbkAppList = tbkAppList;
    }

    public static class TbkApp implements Serializable{
        private String appKey;
        private String appSecret;
        private String redirectUrl;
        private Long siteId;
        private String name;

        public String getRedirectUrl() {
            return redirectUrl;
        }

        public void setRedirectUrl(String redirectUrl) {
            this.redirectUrl = redirectUrl;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAppKey() {
            return appKey;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }

        public String getAppSecret() {
            return appSecret;
        }

        public void setAppSecret(String appSecret) {
            this.appSecret = appSecret;
        }

        public Long getSiteId() {
            return siteId;
        }

        public void setSiteId(Long siteId) {
            this.siteId = siteId;
        }
    }

}
