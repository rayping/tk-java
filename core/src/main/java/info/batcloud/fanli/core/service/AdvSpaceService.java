package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.AdvDTO;
import info.batcloud.fanli.core.dto.AdvSpaceDTO;

import java.util.Date;
import java.util.List;

public interface AdvSpaceService {

    void saveAdvSpace(AdvSpaceAddParam param);

    void updateAdvSpace(AdvSpaceUpdateParam param);

    void validById(long id);

    void invalidById(long id);

    void deleteById(long id);

    AdvSpaceDTO findByAdvCode(String advCode);

    Paging<AdvSpaceDTO> search(SearchParam param);

    List<AdvSpaceDTO> findList();

    List<AdvDTO> advListByAdvSpaceId(long advSpaceId);

    List<AdvDTO> advListByAdvSpaceCode(String advCode);

    class SearchParam extends PagingParam {
        private String name;
        private Date minStartTime;
        private Date maxStartTime;
        private Boolean valid;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Date getMinStartTime() {
            return minStartTime;
        }

        public void setMinStartTime(Date minStartTime) {
            this.minStartTime = minStartTime;
        }

        public Date getMaxStartTime() {
            return maxStartTime;
        }

        public void setMaxStartTime(Date maxStartTime) {
            this.maxStartTime = maxStartTime;
        }

        public Boolean getValid() {
            return valid;
        }

        public void setValid(Boolean valid) {
            this.valid = valid;
        }
    }

    class AdvSpaceUpdateParam {
        private Long id;
        private String name;
        private String code;
        private boolean valid;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }
    }
    class AdvSpaceAddParam {

        private String name;
        private String code;
        private boolean valid;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }
    }
    
}
