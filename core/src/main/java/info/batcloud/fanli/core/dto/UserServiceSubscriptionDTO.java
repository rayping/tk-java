package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.Service;
import info.batcloud.fanli.core.enums.UserServiceSubscriptionStatus;

import java.io.Serializable;
import java.util.Date;

public class UserServiceSubscriptionDTO implements Serializable {

    private Long id;

    private Long userId;

    private String userPhone;

    private Service service;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date expireTime; //过期时间

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    private UserServiceSubscriptionStatus status;

    public String getServiceTitle() {
        return service == null ? null : service.getTitle();
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public UserServiceSubscriptionStatus getStatus() {
        return status;
    }

    public void setStatus(UserServiceSubscriptionStatus status) {
        this.status = status;
    }
}
