package info.batcloud.fanli.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.jd.open.api.sdk.DefaultJdClient;
import com.jd.open.api.sdk.JdClient;
import com.jd.open.api.sdk.JdException;
import com.jd.open.api.sdk.request.cps.*;
import com.jd.open.api.sdk.response.cps.*;
import info.batcloud.fanli.core.constants.JosConstatns;
import info.batcloud.fanli.core.jos.JosApp;
import info.batcloud.fanli.core.service.JdUnionApiService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.service.UserService;
import info.batcloud.fanli.core.service.UserSettingService;
import info.batcloud.fanli.core.settings.JdAuthSetting;
import info.batcloud.fanli.core.settings.JdUnionSetting;
import info.batcloud.laxiaoke.open.domain.jd.union.*;
import info.batcloud.laxiaoke.open.request.jd.union.*;
import info.batcloud.laxiaoke.open.response.jd.union.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class JdUnionApiServiceImpl implements JdUnionApiService {

    @Inject
    private UserSettingService userSettingService;

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    @Qualifier(JosConstatns.UNION_APP)
    private JosApp unionApp;

    @Inject
    private UserService userService;

    @Override
    public CouponGetCodeBySubUnionIdResponse getCodeBySubUnionId(CouponGetCodeBySubUnionIdRequest request) throws JdException {
        JdClient client = getClient();
        ServicePromotionCouponGetCodeBySubUnionIdRequest req = new ServicePromotionCouponGetCodeBySubUnionIdRequest();
        BeanUtils.copyProperties(request, req);
        ServicePromotionCouponGetCodeBySubUnionIdResponse response = client.execute(req);
        GetCodeBySubUnionIdResult result = JSON.parseObject(response.getGetcodebysubunionidResult(), GetCodeBySubUnionIdResult.class);
        CouponGetCodeBySubUnionIdResponse res = new CouponGetCodeBySubUnionIdResponse();
        res.setData(result);
        return res;
    }

    @Override
    public CouponGetCodeByUnionIdResponse getCodeByUnionId(CouponGetCodeByUnionIdRequest request) throws JdException {
        JdClient client = getClient();
        JdUnionSetting jdUnionSetting = systemSettingService.findActiveSetting(JdUnionSetting.class);
        ServicePromotionCouponGetCodeByUnionIdRequest req = new ServicePromotionCouponGetCodeByUnionIdRequest();
        BeanUtils.copyProperties(request, req);
        req.setUnionId(jdUnionSetting.getUnionId());
        req.setPositionId(jdUnionSetting.getPositionId());
        ServicePromotionCouponGetCodeByUnionIdResponse response = client.execute(req);
        GetCodeByUnionIdResult result = JSON.parseObject(response.getGetcodebyunionidResult(), GetCodeByUnionIdResult.class);
        CouponGetCodeByUnionIdResponse res = new CouponGetCodeByUnionIdResponse();
        res.setData(result);
        return res;
    }

    @Override
    public WxsqGetCodeBySubUnionIdResponse getCodeBySubUnionId(WxsqGetCodeBySubUnionIdRequest request) throws JdException {
        ServicePromotionWxsqGetCodeBySubUnionIdRequest req = new ServicePromotionWxsqGetCodeBySubUnionIdRequest();
        BeanUtils.copyProperties(request, req);
        ServicePromotionWxsqGetCodeBySubUnionIdResponse response = getClient().execute(req);
        GetCodeBySubUnionIdResult result = JSON.parseObject(response.getGetcodebysubunionidResult(), GetCodeBySubUnionIdResult.class);
        WxsqGetCodeBySubUnionIdResponse res = new WxsqGetCodeBySubUnionIdResponse();
        res.setData(result);
        return res;
    }

    @Override
    public QueryCouponGoodsResponse queryCouponGoodsResult(QueryCouponGoodsRequest request) throws JdException {
        UnionSearchQueryCouponGoodsRequest req = new UnionSearchQueryCouponGoodsRequest();
        BeanUtils.copyProperties(request, req);
        UnionSearchQueryCouponGoodsResponse response = getClient().execute(req);
        CouponGoodsResult goodsResult = JSON.parseObject(response.getQueryCouponGoodsResult(), CouponGoodsResult.class);
        QueryCouponGoodsResponse res = new QueryCouponGoodsResponse();
        res.setData(goodsResult);
        return res;
    }

    @Override
    public QueryOrderResponse queryOrderResult(QueryOrderRequest request) throws JdException {
        UnionServiceQueryOrderListRequest req = new UnionServiceQueryOrderListRequest();
        BeanUtils.copyProperties(request, req);
        UnionServiceQueryOrderListResponse response = getClient().execute(req);
        QueryOrderResponse res = new QueryOrderResponse();
        UnionOrderResult result = JSON.parseObject(response.getResult(), UnionOrderResult.class);
        res.setData(result);
        return res;
    }

    @Override
    public GoodsInfoResponse getGoodsInfo(GoodsInfoRequest request) throws JdException {
        ServicePromotionGoodsInfoRequest req = new ServicePromotionGoodsInfoRequest();
        BeanUtils.copyProperties(request, req);
        ServicePromotionGoodsInfoResponse response = getClient().execute(req);
        ServicePromotionGoodsInfoResult result = JSON.parseObject(response.getGetpromotioninfoResult(), ServicePromotionGoodsInfoResult.class);
        GoodsInfoResponse res = new GoodsInfoResponse();
        res.setData(result);
        return res;
    }

    @Override
    public GoodsCategoryQueryResponse queryGoodsCategory(GoodsCategoryQueryRequest request) throws JdException {
        UnionSearchGoodsCategoryQueryRequest req = new UnionSearchGoodsCategoryQueryRequest();
        BeanUtils.copyProperties(request, req);
        UnionSearchGoodsCategoryQueryResponse response = getClient().execute(req);
        GoodsCategoryResult result = JSON.parseObject(response.getQuerygoodscategoryResult(), GoodsCategoryResult.class);
        GoodsCategoryQueryResponse res = new GoodsCategoryQueryResponse();
        res.setData(result);
        return res;
    }

    @Override
    public CreatePromotionSiteBatchResponse createPromotionSiteBatch(CreatePromotionSiteBatchRequest request) throws JdException {
        JdUnionSetting jdUnionSetting = systemSettingService.findActiveSetting(JdUnionSetting.class);
        ServicePromotionCreatePromotionSiteBatchRequest req = new ServicePromotionCreatePromotionSiteBatchRequest();
        req.setUnionId(jdUnionSetting.getUnionId());
        req.setKey(jdUnionSetting.getKey());
        req.setUnionType(1);
        req.setType(2);
        req.setSpaceName(request.getSpaceName());
        ServicePromotionCreatePromotionSiteBatchResponse response = getClient().execute(req);
        CreatePromotionSiteBatchResult result = JSON.parseObject(response.getCreatePromotionSiteResult(), CreatePromotionSiteBatchResult.class);
        CreatePromotionSiteBatchResponse res = new CreatePromotionSiteBatchResponse();
        res.setData(result);
        return res;
    }

    private JdClient getClient() {
        JdAuthSetting jdAuthSetting = systemSettingService.findActiveSetting(JdAuthSetting.class);
        JdClient client = new DefaultJdClient(unionApp.getServerUrl(), jdAuthSetting.getAccessToken(),
                unionApp.getAppKey(), unionApp.getAppSecret());
        return client;
    }
}
