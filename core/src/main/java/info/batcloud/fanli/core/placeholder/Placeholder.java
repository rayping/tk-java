package info.batcloud.fanli.core.placeholder;

public interface Placeholder {

    String replace(ReplaceContext replaceContext);

}
