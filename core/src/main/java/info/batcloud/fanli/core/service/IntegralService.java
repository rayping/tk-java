package info.batcloud.fanli.core.service;

public interface IntegralService {

    float toMoney(float integral);

    float toIntegral(float money);

}
