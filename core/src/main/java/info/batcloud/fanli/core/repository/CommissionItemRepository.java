package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.CommissionItem;
import info.batcloud.fanli.core.enums.CommissionItemStatus;
import info.batcloud.fanli.core.enums.EcomPlat;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface CommissionItemRepository extends PagingAndSortingRepository<CommissionItem, Long>,
        JpaSpecificationExecutor<CommissionItem> {

    CommissionItem findBySourceItemIdAndEcomPlat(long sourceItemId, EcomPlat from);

    @Modifying
    @Query("update CommissionItem ci set ci.descImgs = ?2 where ci.id = ?1")
    void updateDescImgsById(long id, String descImgs);

    List<CommissionItem> findBySourceItemIdInAndEcomPlatIn(List<Long> sourceItemIds, List<EcomPlat> from);

    List<CommissionItem> findByIdIn(List<Long> ids);

    List<CommissionItem> findTop1000ByDelistTimeLessThanAndStatus(Date time, CommissionItemStatus status);

}
