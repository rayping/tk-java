package info.batcloud.fanli.core.service;

import java.util.Date;
import java.util.List;

public interface UserSettingService {

    long saveSetting(long userId, Object setting, Date expireTime);

    long saveSetting(long userId, String key, String content, Date expireTime);

    long saveSetting(long userId, Class clazz, String content, Date expireTime);

    <T> T findSetting(long userId, Class<T> clazz);

    <T> T findSetting(long userId, String key);

    void deleteSetting(long userId, String key);

    void deleteSetting(long userId, Class clazz);

    <T> List<T> findList(String key);

    <T> List<T> findList(Class<T> clazz);
}
