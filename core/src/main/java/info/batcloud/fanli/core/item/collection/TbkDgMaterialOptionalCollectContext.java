package info.batcloud.fanli.core.item.collection;

import info.batcloud.fanli.core.enums.TbkDgMaterialOptionalSort;

public class TbkDgMaterialOptionalCollectContext extends Context{

    private Long adzoneId;
    private String q;
    private Long startDsr;
    private Platform platform;
    private Long endTkRate;
    private Long startTkRate;
    private Float endPrice;
    private Float startPrice;
    private Boolean tmall;
    private Boolean overseas;

    private TbkDgMaterialOptionalSort sort;
    private String itemloc;
    private String cat;
    private Boolean hasCoupon;
    private String ip;
    private Boolean needFreeShipment;
    private Boolean needPrepay;
    private Boolean includePayRate30;
    private Boolean includeGoodRate;
    private Boolean includeRfdRate;
    private int npxLevel;

    private Long materialId;

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }

    public Long getAdzoneId() {
        return adzoneId;
    }

    public void setAdzoneId(Long adzoneId) {
        this.adzoneId = adzoneId;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public Long getStartDsr() {
        return startDsr;
    }

    public void setStartDsr(Long startDsr) {
        this.startDsr = startDsr;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public Long getEndTkRate() {
        return endTkRate;
    }

    public void setEndTkRate(Long endTkRate) {
        this.endTkRate = endTkRate;
    }

    public Long getStartTkRate() {
        return startTkRate;
    }

    public void setStartTkRate(Long startTkRate) {
        this.startTkRate = startTkRate;
    }

    public Float getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(Float endPrice) {
        this.endPrice = endPrice;
    }

    public Float getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Float startPrice) {
        this.startPrice = startPrice;
    }

    public Boolean getTmall() {
        return tmall;
    }

    public void setTmall(Boolean tmall) {
        this.tmall = tmall;
    }

    public Boolean getOverseas() {
        return overseas;
    }

    public void setOverseas(Boolean overseas) {
        this.overseas = overseas;
    }

    public TbkDgMaterialOptionalSort getSort() {
        return sort;
    }

    public void setSort(TbkDgMaterialOptionalSort sort) {
        this.sort = sort;
    }

    public String getItemloc() {
        return itemloc;
    }

    public void setItemloc(String itemloc) {
        this.itemloc = itemloc;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public Boolean getHasCoupon() {
        return hasCoupon;
    }

    public void setHasCoupon(Boolean hasCoupon) {
        this.hasCoupon = hasCoupon;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Boolean getNeedFreeShipment() {
        return needFreeShipment;
    }

    public void setNeedFreeShipment(Boolean needFreeShipment) {
        this.needFreeShipment = needFreeShipment;
    }

    public Boolean getNeedPrepay() {
        return needPrepay;
    }

    public void setNeedPrepay(Boolean needPrepay) {
        this.needPrepay = needPrepay;
    }

    public Boolean getIncludePayRate30() {
        return includePayRate30;
    }

    public void setIncludePayRate30(Boolean includePayRate30) {
        this.includePayRate30 = includePayRate30;
    }

    public Boolean getIncludeGoodRate() {
        return includeGoodRate;
    }

    public void setIncludeGoodRate(Boolean includeGoodRate) {
        this.includeGoodRate = includeGoodRate;
    }

    public Boolean getIncludeRfdRate() {
        return includeRfdRate;
    }

    public void setIncludeRfdRate(Boolean includeRfdRate) {
        this.includeRfdRate = includeRfdRate;
    }

    public int getNpxLevel() {
        return npxLevel;
    }

    public void setNpxLevel(int npxLevel) {
        this.npxLevel = npxLevel;
    }

    public enum Platform {
        PC(1l), WL(2l);
        public long value;

        Platform(long value) {
            this.value = value;
        }
    }

}
