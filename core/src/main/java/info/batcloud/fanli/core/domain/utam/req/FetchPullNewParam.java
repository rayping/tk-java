package info.batcloud.fanli.core.domain.utam.req;

import com.ctospace.archit.common.pagination.PagingParam;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class FetchPullNewParam extends PagingParam {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

}
