package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.enums.UserLevel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserLevelProfitPerformanceSetting implements Serializable {

    private List<UserLevelProfitPerformance> userLevelProfitList = new ArrayList<>();

    public List<UserLevelProfitPerformance> getUserLevelProfitList() {
        return userLevelProfitList;
    }

    public void setUserLevelProfitList(List<UserLevelProfitPerformance> userLevelProfitList) {
        this.userLevelProfitList = userLevelProfitList;
    }

    public UserLevelProfitPerformance findByUserLevel(UserLevel userLevel) {
        for (UserLevelProfitPerformance userLevelProfit : userLevelProfitList) {
            if (userLevelProfit.getUserLevel() == userLevel) {
                return userLevelProfit;
            }
        }
        return null;
    }

    public static class UserLevelProfitPerformance implements Serializable{
        private UserLevel userLevel;
        private float commissionRate;//佣金比例
        private float salary;//薪水

        private MonthlyMission monthlyMission;

        public UserLevel getUserLevel() {
            return userLevel;
        }

        public void setUserLevel(UserLevel userLevel) {
            this.userLevel = userLevel;
        }

        public float getCommissionRate() {
            return commissionRate;
        }

        public void setCommissionRate(float commissionRate) {
            this.commissionRate = commissionRate;
        }

        public float getSalary() {
            return salary;
        }

        public void setSalary(float salary) {
            this.salary = salary;
        }

        public MonthlyMission getMonthlyMission() {
            return monthlyMission;
        }

        public void setMonthlyMission(MonthlyMission monthlyMission) {
            this.monthlyMission = monthlyMission;
        }
    }

    public static class MonthlyMission implements Serializable{
        private float commissionFee;
        private float missionReward; //任务奖励

        public float getMissionReward() {
            return missionReward;
        }

        public void setMissionReward(float missionReward) {
            this.missionReward = missionReward;
        }

        public float getCommissionFee() {
            return commissionFee;
        }

        public void setCommissionFee(float commissionFee) {
            this.commissionFee = commissionFee;
        }
    }
}
