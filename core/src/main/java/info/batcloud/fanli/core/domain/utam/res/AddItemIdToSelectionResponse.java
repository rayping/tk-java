package info.batcloud.fanli.core.domain.utam.res;

import info.batcloud.fanli.core.domain.ApiResponse;

public class AddItemIdToSelectionResponse extends ApiResponse<AddItemIdToSelectionResponse.Data> {

    public static class Data {
       private int addFailNum;
       private int addSuccessNum;

        public int getAddFailNum() {
            return addFailNum;
        }

        public void setAddFailNum(int addFailNum) {
            this.addFailNum = addFailNum;
        }

        public int getAddSuccessNum() {
            return addSuccessNum;
        }

        public void setAddSuccessNum(int addSuccessNum) {
            this.addSuccessNum = addSuccessNum;
        }
    }
}
