package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class TbkSessionSetting implements Serializable {

    private String koussSession;

    private String koussServer;


    public String getKoussServer() {
        return koussServer;
    }

    public void setKoussServer(String koussServer) {
        this.koussServer = koussServer;
    }

    public String getKoussSession() {
        return koussSession;
    }

    public void setKoussSession(String koussSession) {
        this.koussSession = koussSession;
    }
}
