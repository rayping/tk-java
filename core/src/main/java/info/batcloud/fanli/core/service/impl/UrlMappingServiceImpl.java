package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.enums.UrlMappingCodes;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.service.UrlMappingService;
import info.batcloud.fanli.core.settings.UrlMappingSetting;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class UrlMappingServiceImpl implements UrlMappingService {

    @Inject
    private SystemSettingService systemSettingService;

    @Override
    public String getCommissionItemShareUrl(long commissionItemId, long userId) {
        UrlMappingSetting setting = systemSettingService.findActiveSetting(UrlMappingSetting.class);
        UrlMappingSetting.UrlMapping um = setting.getUrlByCode(UrlMappingCodes.COMMISSION_ITEM_SHARE_PAGE.name());
        if (um == null || StringUtils.isBlank(um.getUrl())) {
            return null;
        }
        String url = um.getUrl().replace("{id}", commissionItemId + "")
                .replace("{userId}", userId + "");
        return cleanUrl(url);
    }

    @Override
    public String getCommissionItemDescUrl(long commissionItemId) {
        UrlMappingSetting setting = systemSettingService.findActiveSetting(UrlMappingSetting.class);
        UrlMappingSetting.UrlMapping um = setting.getUrlByCode(UrlMappingCodes.COMMISSION_ITEM_DESC_PAGE.name());
        if (um == null || StringUtils.isBlank(um.getUrl())) {
            return null;
        }
        String url = um.getUrl().replace("{id}", commissionItemId + "");
        return cleanUrl(url);
    }

    @Override
    public String getTaobaoNewerShareUrl(String pid) {
        UrlMappingSetting setting = systemSettingService.findActiveSetting(UrlMappingSetting.class);
        UrlMappingSetting.UrlMapping um = setting.getUrlByCode(UrlMappingCodes.TAOBAO_NEWER_SHARE_PAGE.name());
        return cleanUrl(um.getUrl().replace("{pid}", pid));
    }

    @Override
    public String getSellerPromotionItemSearchBuyDescUrl(long sellerPromotionItemId) {
        UrlMappingSetting setting = systemSettingService.findActiveSetting(UrlMappingSetting.class);
        UrlMappingSetting.UrlMapping um = setting.getUrlByCode(UrlMappingCodes.SELLER_PROMOTION_ITEM_SEARCH_BUY_DESC_PAGE.name());
        if (um == null || StringUtils.isBlank(um.getUrl())) {
            return null;
        }
        String url = um.getUrl().replace("{id}", sellerPromotionItemId + "");
        return cleanUrl(url);
    }

    @Override
    public String getHomeWebsiteUrl() {
        UrlMappingSetting setting = systemSettingService.findActiveSetting(UrlMappingSetting.class);
        UrlMappingSetting.UrlMapping um = setting.getUrlByCode(UrlMappingCodes.HOME.name());
        if (um == null || StringUtils.isBlank(um.getUrl())) {
            return null;
        }
        return um.getUrl();
    }

    @Override
    public String getWeixinInvitationJumpUrl() {
        UrlMappingSetting setting = systemSettingService.findActiveSetting(UrlMappingSetting.class);
        UrlMappingSetting.UrlMapping um = setting.getUrlByCode(UrlMappingCodes.WEIXIN_INVITATION_JUMP_PAGE.name());
        if (um == null || StringUtils.isBlank(um.getUrl())) {
            return null;
        }
        return um.getUrl();
    }

    @Override
    public String getWeixinInvitationBindUrl(String invitationCode) {
        UrlMappingSetting setting = systemSettingService.findActiveSetting(UrlMappingSetting.class);
        UrlMappingSetting.UrlMapping um = setting.getUrlByCode(UrlMappingCodes.WEIXIN_INVITATION_BIND_PAGE.name());
        if (um == null || StringUtils.isBlank(um.getUrl())) {
            return null;
        }
        String url = um.getUrl().replace("{invitationCode}", invitationCode + "");
        return cleanUrl(url);
    }

    @Override
    public String getQrcodeUrl(String text) {
        UrlMappingSetting setting = systemSettingService.findActiveSetting(UrlMappingSetting.class);
        UrlMappingSetting.UrlMapping um = setting.getUrlByCode(UrlMappingCodes.QR_CODE_GENERATE_URL.name());
        if (um == null || StringUtils.isBlank(um.getUrl())) {
            return null;
        }
        String url = um.getUrl().replace("{text}", text);
        return cleanUrl(url);
    }

    @Override
    public String flashSaleDrawHelperUrl() {
        UrlMappingSetting setting = systemSettingService.findActiveSetting(UrlMappingSetting.class);
        UrlMappingSetting.UrlMapping um = setting.getUrlByCode(UrlMappingCodes.FLASH_SALE_DRAW_URL.name());
        if (um == null || StringUtils.isBlank(um.getUrl())) {
            return null;
        }
        return cleanUrl(um.getUrl());
    }

    private static String cleanUrl(String url) {
        return url;
    }
}
