package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.AgentType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@DiscriminatorValue("CityAgent")
public class CityAgent extends Agent {

    @NotNull
    private Long cityId;

    @NotNull
    private Long provinceId;

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    @Override
    public AgentType getAgentType() {
        return AgentType.CITY;
    }
}
