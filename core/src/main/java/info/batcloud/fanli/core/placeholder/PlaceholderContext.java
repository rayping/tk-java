package info.batcloud.fanli.core.placeholder;

import java.util.HashMap;
import java.util.Map;

public class PlaceholderContext {

    private final static Map<String, Placeholder> PLACEHOLDER_MAP = new HashMap<>();

    public static void register(String key, Placeholder placeholder) {
        PLACEHOLDER_MAP.put(key, placeholder);
    }

    public static Placeholder getByKey(String key) {
        return PLACEHOLDER_MAP.get(key);
    }

}
