package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.OutCatToShopcat;
import info.batcloud.fanli.core.enums.ItemSelectionPlat;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OutCatToShopcatRepository extends CrudRepository<OutCatToShopcat, Long> {

    List<OutCatToShopcat> findByShopcatId(long shopcatId);

    List<OutCatToShopcat> findByPlatAndCatId(ItemSelectionPlat plat, long catId);
}
