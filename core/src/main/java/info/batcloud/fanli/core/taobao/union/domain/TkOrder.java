package info.batcloud.fanli.core.taobao.union.domain;

import info.batcloud.fanli.core.excel.stereotype.XslCol;

import java.util.Date;

public class TkOrder {

    @XslCol(title = "创建时间")
    private Date createTime;

    @XslCol(title = "商品信息")
    private String itemTitle;

    @XslCol(title = "商品ID")
    private String itemId;

    @XslCol(title = "掌柜旺旺")
    private String sellerName;

    @XslCol(title = "所属店铺")
    private String shopTitle;

    @XslCol(title = "商品数")
    private int itemNum;

    @XslCol(title = "商品单价")
    private Float price;

    @XslCol(title = "订单状态")
    private String status;

    @XslCol(title = "订单类型")
    private String type;

    @XslCol(title = "收入比率")
    private String incomeRate;

    @XslCol(title = "分成比率")
    private String shareRate;

    @XslCol(title = "付款金额")
    private float payFee;

    @XslCol(title = "效果预估")
    private float estimateFee;

    @XslCol(title = "结算金额")
    private float settleFee;

    @XslCol(title = "佣金金额")
    private float commissionFee;

    @XslCol(title = "预估收入")
    private float estimateIncome;

    @XslCol(title = "佣金比率")
    private String commissionRate;

    @XslCol(title = "订单编号")
    private String orderNo;

    @XslCol(title = "广告位ID")
    private Long adzoneId;

    @XslCol(title = "结算时间")
    private Date settledTime;

    @XslCol(title = "关系id")
    private String relationId;

    @XslCol(title = "会员运营id")
    private String specialId;

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    public String getSpecialId() {
        return specialId;
    }

    public void setSpecialId(String specialId) {
        this.specialId = specialId;
    }

    public Date getSettledTime() {
        return settledTime;
    }

    public void setSettledTime(Date settledTime) {
        this.settledTime = settledTime;
    }

    public float getEstimateFee() {
        return estimateFee;
    }

    public void setEstimateFee(float estimateFee) {
        this.estimateFee = estimateFee;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public int getItemNum() {
        return itemNum;
    }

    public void setItemNum(int itemNum) {
        this.itemNum = itemNum;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIncomeRate() {
        return incomeRate;
    }

    public void setIncomeRate(String incomeRate) {
        this.incomeRate = incomeRate;
    }

    public String getShareRate() {
        return shareRate;
    }

    public void setShareRate(String shareRate) {
        this.shareRate = shareRate;
    }

    public Float getPayFee() {
        return payFee;
    }

    public void setPayFee(Float payFee) {
        this.payFee = payFee;
    }

    public Float getSettleFee() {
        return settleFee;
    }

    public void setSettleFee(Float settleFee) {
        this.settleFee = settleFee;
    }

    public Float getCommissionFee() {
        return commissionFee;
    }

    public void setCommissionFee(Float commissionFee) {
        this.commissionFee = commissionFee;
    }

    public float getEstimateIncome() {
        return estimateIncome;
    }

    public void setEstimateIncome(float estimateIncome) {
        this.estimateIncome = estimateIncome;
    }

    public String getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(String commissionRate) {
        this.commissionRate = commissionRate;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Long getAdzoneId() {
        return adzoneId;
    }

    public void setAdzoneId(Long adzoneId) {
        this.adzoneId = adzoneId;
    }
}
