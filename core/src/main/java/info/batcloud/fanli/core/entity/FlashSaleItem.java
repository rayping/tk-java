package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.FlashSaleItemStatus;
import info.batcloud.fanli.core.enums.UserLevel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class  FlashSaleItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String date;

    @NotNull
    private String season; //场次

    private String label;

    private String slogan;

    @NotNull
    private Float returnFee;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private CommissionItem item;

    @NotNull
    private Date createTime;

    @NotNull
    private Date startTime;

    @Enumerated(EnumType.STRING)
    private FlashSaleItemStatus status;

    @NotNull
    private Integer totalNum;

    @NotNull
    private Integer drawnNum;

    private Date lastDrawnTime;

    @NotNull
    @Enumerated(EnumType.STRING)
    private UserLevel userLevel;

    @Version
    private Integer version;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Float getReturnFee() {
        return returnFee;
    }

    public void setReturnFee(Float returnFee) {
        this.returnFee = returnFee;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UserLevel getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(UserLevel userLevel) {
        this.userLevel = userLevel;
    }

    public Date getLastDrawnTime() {
        return lastDrawnTime;
    }

    public void setLastDrawnTime(Date lastDrawnTime) {
        this.lastDrawnTime = lastDrawnTime;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getDrawnNum() {
        return drawnNum;
    }

    public void setDrawnNum(Integer drawnNum) {
        this.drawnNum = drawnNum;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public FlashSaleItemStatus getStatus() {
        return status;
    }

    public void setStatus(FlashSaleItemStatus status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public CommissionItem getItem() {
        return item;
    }

    public void setItem(CommissionItem item) {
        this.item = item;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "FlashSaleItem{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", season='" + season + '\'' +
                ", label='" + label + '\'' +
                ", slogan='" + slogan + '\'' +
                ", returnFee=" + returnFee +
                ", item=" + item +
                ", createTime=" + createTime +
                ", startTime=" + startTime +
                ", status=" + status +
                ", totalNum=" + totalNum +
                ", drawnNum=" + drawnNum +
                ", lastDrawnTime=" + lastDrawnTime +
                ", userLevel=" + userLevel +
                ", version=" + version +
                '}';
    }
}
