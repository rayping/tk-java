package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class Taobao1111Setting implements Serializable {

    private String superRedPacketClickUrl;

    private String superRedPacketToken;

    private String superRedPacketText;

    public String getSuperRedPacketClickUrl() {
        return superRedPacketClickUrl;
    }

    public void setSuperRedPacketClickUrl(String superRedPacketClickUrl) {
        this.superRedPacketClickUrl = superRedPacketClickUrl;
    }

    public String getSuperRedPacketToken() {
        return superRedPacketToken;
    }

    public void setSuperRedPacketToken(String superRedPacketToken) {
        this.superRedPacketToken = superRedPacketToken;
    }

    public String getSuperRedPacketText() {
        return superRedPacketText;
    }

    public void setSuperRedPacketText(String superRedPacketText) {
        this.superRedPacketText = superRedPacketText;
    }
}
