package info.batcloud.fanli.core.dto;

import javax.validation.constraints.NotNull;

public class CollectionSourceUpdateDTO {

    @NotNull
    private String title;

    //抓取的最大页数
    @NotNull
    private int maxPage;

    //每页抓取的数量
    @NotNull
    private int pageSize;

    private String keyword;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
