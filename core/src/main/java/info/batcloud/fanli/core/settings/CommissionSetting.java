package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class CommissionSetting implements Serializable {

    private Float retainCommissionRate; //预留的佣金，给用户的佣金结算需要去除该部分的。

    private Float agentRetainCommissionRate; //代理商的预留佣金，该部分应当从总预留佣金结算里分配

    public Float getRetainCommissionRate() {
        return retainCommissionRate;
    }

    public Float getAgentRetainCommissionRate() {
        return agentRetainCommissionRate;
    }

    public void setAgentRetainCommissionRate(Float agentRetainCommissionRate) {
        this.agentRetainCommissionRate = agentRetainCommissionRate;
    }

    public void setRetainCommissionRate(Float retainCommissionRate) {
        this.retainCommissionRate = retainCommissionRate;
    }

}
