package info.batcloud.fanli.core.service;

import com.taobao.api.ApiException;

public interface TbkService {

    String transformToToken(String text, String url) throws ApiException;

}
