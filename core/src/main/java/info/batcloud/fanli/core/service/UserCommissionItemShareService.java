package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.dto.UserCommissionItemShareDTO;

public interface UserCommissionItemShareService {

    UserCommissionItemShareDTO findValidUserCommissionItemShare(long commissionItemId, long userId);

    String findUserCommissionItemShareUrl(long commissionItemId, long userId);

}
