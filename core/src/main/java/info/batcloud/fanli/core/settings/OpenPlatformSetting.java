package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class OpenPlatformSetting implements Serializable {

    private String serverUrl;

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }
}
