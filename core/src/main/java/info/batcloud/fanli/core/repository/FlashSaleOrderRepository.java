package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.FlashSaleOrder;
import info.batcloud.fanli.core.enums.FlashSaleOrderStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface FlashSaleOrderRepository extends PagingAndSortingRepository<FlashSaleOrder, Long>,
        JpaSpecificationExecutor<FlashSaleOrder> {

    int countByUserIdAndFlashSaleItemId(long userId, long flashSaleITemId);

    int countByUserIdAndFlashSaleItemIdAndCreateTimeBetween(long id, long activityId, Date startTime, Date endTime);

    int countByUserIdAndItemIdAndStatus(long userId, long itemId, FlashSaleOrderStatus status);

    FlashSaleOrder findByUserIdAndItemId(long userId, long itemId);
    FlashSaleOrder findByUserIdAndFlashSaleItemId(long userId, long flashSaleItemId);
    FlashSaleOrder findByCommissionOrderId(long commissionOrderId);

    List<FlashSaleOrder> findByStatus(FlashSaleOrderStatus status);

    List<FlashSaleOrder> findByUserIdAndFlashSaleItemIdIn(long userId, List<Long> idList);
    int countByUserIdAndFlashSaleItemDateAndFlashSaleItemSeason(long userId, String date, String season);
}
