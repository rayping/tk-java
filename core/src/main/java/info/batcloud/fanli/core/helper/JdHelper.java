package info.batcloud.fanli.core.helper;

import org.apache.commons.lang.StringUtils;

public class JdHelper {

    public static String imageUrl(String imageUrl) {
        if(imageUrl.startsWith("http")) {
            return imageUrl;
        } else {
            return "https://img12.360buyimg.com/n1/" + imageUrl;
        }
    }

    public static String completeUrl(String url) {
        if(StringUtils.isBlank(url)) {
            return null;
        }
        if(url.startsWith("http")) {
            return url;
        }
        if(url.startsWith("//")) {
            return "https:" + url;
        }
        return "https://" + url;
    }

}
