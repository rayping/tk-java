package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Wallet;
import org.springframework.data.repository.CrudRepository;

public interface WalletRepository extends CrudRepository<Wallet, Long>{

    Wallet findByUserId(long userId);

}
