package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.entity.Crowd;
import info.batcloud.fanli.core.entity.User;
import info.batcloud.fanli.core.enums.CrowdStatus;
import info.batcloud.fanli.core.enums.UserLevel;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public interface CrowdService {

    void createCrowd(CrowdCreateParam param);

    void updateCrowd(long id, CrowdUpdateParam param);

    boolean checkUser(User user, Crowd crowd);
    boolean checkUser(long userId, long crowdId);

    List<Crowd> listAll();
    List<Crowd> listAll(CrowdStatus status);

    void setStatus(long id, CrowdStatus status);

    class CrowdUpdateParam {
        private CrowdStatus status;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date minCreateTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date maxCreateTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date minActivityTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date maxActivityTime;

        private UserLevel userLevel;

        public CrowdStatus getStatus() {
            return status;
        }

        public void setStatus(CrowdStatus status) {
            this.status = status;
        }

        public Date getMinCreateTime() {
            return minCreateTime;
        }

        public void setMinCreateTime(Date minCreateTime) {
            this.minCreateTime = minCreateTime;
        }

        public Date getMaxCreateTime() {
            return maxCreateTime;
        }

        public void setMaxCreateTime(Date maxCreateTime) {
            this.maxCreateTime = maxCreateTime;
        }

        public Date getMinActivityTime() {
            return minActivityTime;
        }

        public void setMinActivityTime(Date minActivityTime) {
            this.minActivityTime = minActivityTime;
        }

        public Date getMaxActivityTime() {
            return maxActivityTime;
        }

        public void setMaxActivityTime(Date maxActivityTime) {
            this.maxActivityTime = maxActivityTime;
        }

        public UserLevel getUserLevel() {
            return userLevel;
        }

        public void setUserLevel(UserLevel userLevel) {
            this.userLevel = userLevel;
        }
    }

    class CrowdCreateParam {
        private String name;
        private CrowdStatus status;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date minCreateTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date maxCreateTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date minActivityTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date maxActivityTime;

        private UserLevel userLevel;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public CrowdStatus getStatus() {
            return status;
        }

        public void setStatus(CrowdStatus status) {
            this.status = status;
        }

        public Date getMinCreateTime() {
            return minCreateTime;
        }

        public void setMinCreateTime(Date minCreateTime) {
            this.minCreateTime = minCreateTime;
        }

        public Date getMaxCreateTime() {
            return maxCreateTime;
        }

        public void setMaxCreateTime(Date maxCreateTime) {
            this.maxCreateTime = maxCreateTime;
        }

        public Date getMinActivityTime() {
            return minActivityTime;
        }

        public void setMinActivityTime(Date minActivityTime) {
            this.minActivityTime = minActivityTime;
        }

        public Date getMaxActivityTime() {
            return maxActivityTime;
        }

        public void setMaxActivityTime(Date maxActivityTime) {
            this.maxActivityTime = maxActivityTime;
        }

        public UserLevel getUserLevel() {
            return userLevel;
        }

        public void setUserLevel(UserLevel userLevel) {
            this.userLevel = userLevel;
        }
    }

}
