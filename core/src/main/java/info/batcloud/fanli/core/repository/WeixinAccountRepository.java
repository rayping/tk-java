package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.WeixinAccount;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WeixinAccountRepository extends PagingAndSortingRepository<WeixinAccount, Long> {

    WeixinAccount findByOpenId(String openId);

}
