package info.batcloud.fanli.core.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import info.batcloud.fanli.core.enums.SellerPromotionItemStatus;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//卖家返利商品
@Entity
@DiscriminatorValue("SellerPromotionItem")
public class SellerPromotionItem extends Item {

    @NotNull
    private Long userId;

    @NotNull
    private String logisticsCompany;

    @NotNull
    private String keywords;

    private int totalNum;

    private int totalSaleNum;

    private int remainNum;

    //日放单数量
    private int dayLimitNum;

    @Enumerated(EnumType.STRING)
    private SellerPromotionItemStatus status;

    @NotNull
    private Date startTime;

    //担保金
    private float guaranteeFee;

    //单个返利金额
    private float rebateFee;

    //总共返利金额，已经返利的金额
    private float totalRebateFee;

    //搜索下单截图
    @NotNull
    private String screenshots;

    private String verifyRemark;

    public String getVerifyRemark() {
        return verifyRemark;
    }

    public void setVerifyRemark(String verifyRemark) {
        this.verifyRemark = verifyRemark;
    }

    public float getTotalRebateFee() {
        return totalRebateFee;
    }

    public void setTotalRebateFee(float totalRebateFee) {
        this.totalRebateFee = totalRebateFee;
    }

    public float getRebateFee() {
        return rebateFee;
    }

    public void setRebateFee(float rebateFee) {
        this.rebateFee = rebateFee;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getTotalSaleNum() {
        return totalSaleNum;
    }

    public void setTotalSaleNum(int totalSaleNum) {
        this.totalSaleNum = totalSaleNum;
    }

    public int getRemainNum() {
        return remainNum;
    }

    public void setRemainNum(int remainNum) {
        this.remainNum = remainNum;
    }

    public int getDayLimitNum() {
        return dayLimitNum;
    }

    public void setDayLimitNum(int dayLimitNum) {
        this.dayLimitNum = dayLimitNum;
    }

    public SellerPromotionItemStatus getStatus() {
        return status;
    }

    public void setStatus(SellerPromotionItemStatus status) {
        this.status = status;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public float getGuaranteeFee() {
        return guaranteeFee;
    }

    public void setGuaranteeFee(float guaranteeFee) {
        this.guaranteeFee = guaranteeFee;
    }

    public String getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(String screenshots) {
        this.screenshots = screenshots;
    }

    public void setScreenshotList(List<String> list) {
        this.setScreenshots(JSON.toJSONString(list));
    }

    public List<String> getScreenshotList() {
        if (StringUtils.isBlank(this.getScreenshots())) {
            return new ArrayList<>();
        }
        return JSON.parseObject(this.getScreenshots(), new TypeReference<List<String>>() {
        });
    }

}
