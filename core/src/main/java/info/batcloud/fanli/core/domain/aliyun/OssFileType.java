package info.batcloud.fanli.core.domain.aliyun;

public enum OssFileType {

    DIRECTORY, IMAGE;

}
