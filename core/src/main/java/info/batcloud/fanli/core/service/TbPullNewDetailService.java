package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.TbPullNewDetailAddDTO;
import info.batcloud.fanli.core.dto.TbPullNewDetailDTO;

import java.util.List;

public interface TbPullNewDetailService {

    void saveTbPullNewDetailList(List<TbPullNewDetailAddDTO> itemList);

    void saveTbPullNewDetailListSync(List<TbPullNewDetailAddDTO> itemList);

    PullNewStat statUserByMonth(long userId, int year, int month);

    PullNewStat statByUserId(long userId);

    Paging<TbPullNewDetailDTO> search(SearchParam param);

    class SearchParam extends PagingParam {
        private Long userId;
        private Long status;
        private Integer year;
        private Integer month;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getStatus() {
            return status;
        }

        public void setStatus(Long status) {
            this.status = status;
        }

        public Integer getYear() {
            return year;
        }

        public void setYear(Integer year) {
            this.year = year;
        }

        public Integer getMonth() {
            return month;
        }

        public void setMonth(Integer month) {
            this.month = month;
        }
    }

    class PullNewStat {
        //已注册
        private int registerNum;

        //已锁定
        private int lockedNum;

        //已激活
        private int activatedNum;

        //已购买数量
        private int boughtNum;

        //已收货数量
        private int completeNum;

        private int validNum;

        /**
         * 预估奖励金是新激活用户*每个用户的奖励金额
         */
        private float estimatedRewards;//预估奖励金

        public int getValidNum() {
            return validNum;
        }

        public void setValidNum(int validNum) {
            this.validNum = validNum;
        }

        public float getEstimatedRewards() {
            return estimatedRewards;
        }

        public void setEstimatedRewards(float estimatedRewards) {
            this.estimatedRewards = estimatedRewards;
        }

        public int getLockedNum() {
            return lockedNum;
        }

        public void setLockedNum(int lockedNum) {
            this.lockedNum = lockedNum;
        }

        public int getRegisterNum() {
            return registerNum;
        }

        public void setRegisterNum(int registerNum) {
            this.registerNum = registerNum;
        }

        public int getActivatedNum() {
            return activatedNum;
        }

        public void setActivatedNum(int activatedNum) {
            this.activatedNum = activatedNum;
        }

        public int getBoughtNum() {
            return boughtNum;
        }

        public void setBoughtNum(int boughtNum) {
            this.boughtNum = boughtNum;
        }

        public int getCompleteNum() {
            return completeNum;
        }

        public void setCompleteNum(int completeNum) {
            this.completeNum = completeNum;
        }
    }
}
