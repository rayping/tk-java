package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Agent;
import info.batcloud.fanli.core.enums.AgentStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AgentRepository extends PagingAndSortingRepository<Agent, Long>, JpaSpecificationExecutor<Agent> {

    List<Agent> findByUserIdAndStatusIsNot(long userId, AgentStatus status);

}
