package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.ItemImg;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemImgRepository extends CrudRepository<ItemImg, Long>{

    List<ItemImg> findByItemId(long itemId);

    @Query("select url from ItemImg where itemId = ?1")
    List<String> findUrlByItemId(long itemId);

    int deleteByItemIdAndUrl(long itemId, String url);
}
