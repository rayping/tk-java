package info.batcloud.fanli.core.settings;


import info.batcloud.fanli.core.enums.EcomPlat;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class UrlSchemeSetting implements Serializable{

    private List<UrlScheme> urlSchemeList;

    public UrlScheme find(EcomPlat ecomPlat, Page page) {
        if (urlSchemeList == null) {
            return null;
        }
        for (UrlScheme urlScheme : urlSchemeList) {
            if (urlScheme.getEcomPlat() == ecomPlat && urlScheme.getPage() == page) {
                return urlScheme;
            }
        }
        return null;
    }

    public String findUrl(EcomPlat ecomPlat, Page page, String value) {
        UrlScheme urlScheme = find(ecomPlat, page);
        if(urlScheme == null) {
            return value;
        }
        try {
            return urlScheme.getUrl().replaceAll("\\{placeholder}", URLEncoder.encode(value, "utf8"));
        } catch (UnsupportedEncodingException e) {
            return value;
        }
    }

    public List<UrlScheme> getUrlSchemeList() {
        return urlSchemeList;
    }

    public void setUrlSchemeList(List<UrlScheme> urlSchemeList) {
        this.urlSchemeList = urlSchemeList;
    }

    public static class UrlScheme implements Serializable {
        private EcomPlat ecomPlat;
        private Page page;
        private String url;

        public EcomPlat getEcomPlat() {
            return ecomPlat;
        }

        public void setEcomPlat(EcomPlat ecomPlat) {
            this.ecomPlat = ecomPlat;
        }

        public Page getPage() {
            return page;
        }

        public void setPage(Page page) {
            this.page = page;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public enum Page {
        SEARCH, ITEM_DETAIL
    }

}
