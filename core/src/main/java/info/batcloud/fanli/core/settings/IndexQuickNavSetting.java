package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.operation.center.domain.PicAdv;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class IndexQuickNavSetting implements Serializable {

    private List<PicAdv> quickNavList = new ArrayList<>();

    public List<PicAdv> getQuickNavList() {
        return quickNavList;
    }

    public void setQuickNavList(List<PicAdv> quickNavList) {
        this.quickNavList = quickNavList;
    }
}
