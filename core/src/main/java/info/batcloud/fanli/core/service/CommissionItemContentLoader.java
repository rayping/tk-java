package info.batcloud.fanli.core.service;

import java.util.List;

public interface CommissionItemContentLoader {

    List loadDescImgList(long itemId);

}
