package info.batcloud.fanli.core.enums;

public enum CollectionSourceType {

    KEYWORD("关键字"), SHOPCAT("店铺类目");
    public String title;

    CollectionSourceType(String title) {
        this.title = title;
    }

}
