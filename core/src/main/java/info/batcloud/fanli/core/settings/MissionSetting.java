package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class MissionSetting implements Serializable {

    //用户任务分配数量
    private int userMissionAssignNum;

    public int getUserMissionAssignNum() {
        return userMissionAssignNum;
    }

    public void setUserMissionAssignNum(int userMissionAssignNum) {
        this.userMissionAssignNum = userMissionAssignNum;
    }
}
