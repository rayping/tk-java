package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.dto.WeixinOfficialAccountDTO;
import info.batcloud.fanli.core.enums.WeixinOfficialAccountType;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface WeixinOfficialAccountService {

    List<WeixinOfficialAccountDTO> findByUserId(long userId);

    WeixinOfficialAccountDTO findById(long id);

    WeixinOfficialAccountDTO addWeixinOfficialAccount(WeixinOfficalAccountAddParam param);

    void updateWeixinOfficialAccount(long id, WeixinOfficalAccountUpdateParam param);

    void deleteByUserIdAndId(long userId, long id);

    class WeixinOfficalAccountUpdateParam {
        @NotNull
        private Long userId;

        @NotNull
        private String token;

        @NotNull
        private String encodingAESKey;

        @NotNull
        private String name;

        @NotNull
        private WeixinOfficialAccountType type;

        public WeixinOfficialAccountType getType() {
            return type;
        }

        public void setType(WeixinOfficialAccountType type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getEncodingAESKey() {
            return encodingAESKey;
        }

        public void setEncodingAESKey(String encodingAESKey) {
            this.encodingAESKey = encodingAESKey;
        }
    }

    class WeixinOfficalAccountAddParam {
        @NotNull
        private Long userId;

        @NotNull
        private String token;

        @NotNull
        private String encodingAESKey;

        @NotNull
        private String name;

        @NotNull
        private WeixinOfficialAccountType type;

        public WeixinOfficialAccountType getType() {
            return type;
        }

        public void setType(WeixinOfficialAccountType type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getEncodingAESKey() {
            return encodingAESKey;
        }

        public void setEncodingAESKey(String encodingAESKey) {
            this.encodingAESKey = encodingAESKey;
        }
    }
}
