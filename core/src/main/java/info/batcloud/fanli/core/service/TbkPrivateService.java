package info.batcloud.fanli.core.service;

import com.taobao.api.ApiException;

/**
 * @author lvling
 * 淘宝客私域管理
 * */
public interface TbkPrivateService {

    String findUserSpecialId(long userId);

    String generateUserSpecialId(long userId);
    String generateUserRelationId(long userId);

}
