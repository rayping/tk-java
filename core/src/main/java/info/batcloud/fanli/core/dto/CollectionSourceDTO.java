package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.CollectStatus;
import info.batcloud.fanli.core.enums.CollectionSourceStatus;
import info.batcloud.fanli.core.enums.CollectionSourceType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;

public class CollectionSourceDTO {

    private Long id;

    private String title;

    @Enumerated(EnumType.STRING)
    private CollectionSourceStatus status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastCollectStartTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastCollectEndTime;

    private Long collectSeconds;

    private CollectStatus collectStatus;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date stopTime;

    //抓取的最大页数
    private int maxPage;

    //每页抓取的数量
    private int pageSize;

    private String keyword;

    private CollectionSourceType type;

    public CollectionSourceType getType() {
        return type;
    }

    public void setType(CollectionSourceType type) {
        this.type = type;
    }

    public String getTypeTitle() {
        return getType() == null ? null : getType().title;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CollectionSourceStatus getStatus() {
        return status;
    }

    public void setStatus(CollectionSourceStatus status) {
        this.status = status;
    }

    public String getStatusTitle() {
        return getStatus() == null ? null : getStatus().title;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastCollectStartTime() {
        return lastCollectStartTime;
    }

    public void setLastCollectStartTime(Date lastCollectStartTime) {
        this.lastCollectStartTime = lastCollectStartTime;
    }

    public Date getLastCollectEndTime() {
        return lastCollectEndTime;
    }

    public void setLastCollectEndTime(Date lastCollectEndTime) {
        this.lastCollectEndTime = lastCollectEndTime;
    }

    public Long getCollectSeconds() {
        return collectSeconds;
    }

    public void setCollectSeconds(Long collectSeconds) {
        this.collectSeconds = collectSeconds;
    }

    public CollectStatus getCollectStatus() {
        return collectStatus;
    }

    public void setCollectStatus(CollectStatus collectStatus) {
        this.collectStatus = collectStatus;
    }

    public String getCollectStatusTitle() {
        return getCollectStatus() == null ? null : getCollectStatus().title;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}
