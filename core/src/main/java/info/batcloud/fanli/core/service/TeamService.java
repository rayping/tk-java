package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.Member;
import info.batcloud.fanli.core.enums.MemberType;

public interface TeamService {

    Paging<Member> findUserMemberShip(MemberFindParam param);

    long findUserMemberShipNum(long userId);

    MemberNumStat statUserMemberNum(long userId);

    class MemberNumStat {
        private long total;
        private long direct;
        private long indirect;

        public long getTotal() {
            return total;
        }

        public void setTotal(long total) {
            this.total = total;
        }

        public long getDirect() {
            return direct;
        }

        public void setDirect(long direct) {
            this.direct = direct;
        }

        public long getIndirect() {
            return indirect;
        }

        public void setIndirect(long indirect) {
            this.indirect = indirect;
        }
    }

    class MemberFindParam extends PagingParam {
        private Long userId;
        private String phone;
        private MemberType type;

        public MemberType getType() {
            return type;
        }

        public void setType(MemberType type) {
            this.type = type;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
