package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class SellerPromotionItemSetting implements Serializable {

    //用户购买锁定天数，购买几天后才可以重复购买
    private int userBuyLockedDays;

    //用户最多未完成的抢购数量
    private int userRushBuyLimitNum;

    //自动返现超时天数
    private int autoRebateTimeoutDays;

    //订单超时释放分钟数
    private int orderHoldTimeoutMinutes;

    //待审核订单自动审核天数
    private int waitVerifyTimeoutDays;

    //审核失败订单自动处理天数
    private int verifyFailTimeoutDays;

    public int getWaitVerifyTimeoutDays() {
        return waitVerifyTimeoutDays;
    }

    public void setWaitVerifyTimeoutDays(int waitVerifyTimeoutDays) {
        this.waitVerifyTimeoutDays = waitVerifyTimeoutDays;
    }

    public int getVerifyFailTimeoutDays() {
        return verifyFailTimeoutDays;
    }

    public void setVerifyFailTimeoutDays(int verifyFailTimeoutDays) {
        this.verifyFailTimeoutDays = verifyFailTimeoutDays;
    }

    public int getOrderHoldTimeoutMinutes() {
        return orderHoldTimeoutMinutes;
    }

    public void setOrderHoldTimeoutMinutes(int orderHoldTimeoutMinutes) {
        this.orderHoldTimeoutMinutes = orderHoldTimeoutMinutes;
    }

    public int getAutoRebateTimeoutDays() {
        return autoRebateTimeoutDays;
    }

    public void setAutoRebateTimeoutDays(int autoRebateTimeoutDays) {
        this.autoRebateTimeoutDays = autoRebateTimeoutDays;
    }

    public int getUserRushBuyLimitNum() {
        return userRushBuyLimitNum;
    }

    public void setUserRushBuyLimitNum(int userRushBuyLimitNum) {
        this.userRushBuyLimitNum = userRushBuyLimitNum;
    }

    public int getUserBuyLockedDays() {
        return userBuyLockedDays;
    }

    public void setUserBuyLockedDays(int userBuyLockedDays) {
        this.userBuyLockedDays = userBuyLockedDays;
    }
}
