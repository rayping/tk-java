package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.enums.UserLevel;
import info.batcloud.fanli.core.enums.UserUpgradePayType;
import info.batcloud.fanli.core.enums.TimeUnit;

import java.io.Serializable;

public class UserUpgradeSetting implements Serializable {

    private UserLevel directlyUpgradeLevel;

    private boolean openDirectlyUpgrade;

    private Plus plus;

    private Carrier carrier;

    private Chief chief;

    public Plus getPlus() {
        return plus;
    }

    public void setPlus(Plus plus) {
        this.plus = plus;
    }

    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public Chief getChief() {
        return chief;
    }

    public void setChief(Chief chief) {
        this.chief = chief;
    }

    public boolean isOpenDirectlyUpgrade() {
        return openDirectlyUpgrade;
    }

    public void setOpenDirectlyUpgrade(boolean openDirectlyUpgrade) {
        this.openDirectlyUpgrade = openDirectlyUpgrade;
    }

    public UserLevel getDirectlyUpgradeLevel() {
        return directlyUpgradeLevel;
    }

    public void setDirectlyUpgradeLevel(UserLevel directlyUpgradeLevel) {
        this.directlyUpgradeLevel = directlyUpgradeLevel;
    }

    public static class Plus extends Upgrade {
        private int inviteUserNum;
        /**
         * 直推订单数量
         * */
        private int directOrderNum;

        private boolean allotUpgradeFee;

        //上级奖励金
        private float fatherRewardFee;
        //上级的上级奖励金
        private float grandRewardFee;

        //运营商奖励金
        private float carrierRewardFee;
        //运营商上级奖励金
        private float carrierSuperRewardFee;

        //总监奖励金
        private float chiefRewardFee;
        //上级总监奖励金
        private float chiefSuperRewardFee;

        //市级代理奖励金
        private float cityAgentRewardFee;
        //区级代理奖励金
        private float districtAgentRewardFee;
        //服务商代理奖励金
        private float relationAgentRewardFee;

        public int getDirectOrderNum() {
            return directOrderNum;
        }

        public void setDirectOrderNum(int directOrderNum) {
            this.directOrderNum = directOrderNum;
        }

        public boolean isAllotUpgradeFee() {
            return allotUpgradeFee;
        }

        public int getInviteUserNum() {
            return inviteUserNum;
        }

        public void setInviteUserNum(int inviteUserNum) {
            this.inviteUserNum = inviteUserNum;
        }

        public void setAllotUpgradeFee(boolean allotUpgradeFee) {
            this.allotUpgradeFee = allotUpgradeFee;
        }

        public float getFatherRewardFee() {
            return fatherRewardFee;
        }

        public void setFatherRewardFee(float fatherRewardFee) {
            this.fatherRewardFee = fatherRewardFee;
        }

        public float getGrandRewardFee() {
            return grandRewardFee;
        }

        public void setGrandRewardFee(float grandRewardFee) {
            this.grandRewardFee = grandRewardFee;
        }

        public float getCarrierRewardFee() {
            return carrierRewardFee;
        }

        public void setCarrierRewardFee(float carrierRewardFee) {
            this.carrierRewardFee = carrierRewardFee;
        }

        public float getCarrierSuperRewardFee() {
            return carrierSuperRewardFee;
        }

        public void setCarrierSuperRewardFee(float carrierSuperRewardFee) {
            this.carrierSuperRewardFee = carrierSuperRewardFee;
        }

        public float getChiefRewardFee() {
            return chiefRewardFee;
        }

        public void setChiefRewardFee(float chiefRewardFee) {
            this.chiefRewardFee = chiefRewardFee;
        }

        public float getChiefSuperRewardFee() {
            return chiefSuperRewardFee;
        }

        public void setChiefSuperRewardFee(float chiefSuperRewardFee) {
            this.chiefSuperRewardFee = chiefSuperRewardFee;
        }

        public float getCityAgentRewardFee() {
            return cityAgentRewardFee;
        }

        public void setCityAgentRewardFee(float cityAgentRewardFee) {
            this.cityAgentRewardFee = cityAgentRewardFee;
        }

        public float getDistrictAgentRewardFee() {
            return districtAgentRewardFee;
        }

        public void setDistrictAgentRewardFee(float districtAgentRewardFee) {
            this.districtAgentRewardFee = districtAgentRewardFee;
        }

        public float getRelationAgentRewardFee() {
            return relationAgentRewardFee;
        }

        public void setRelationAgentRewardFee(float relationAgentRewardFee) {
            this.relationAgentRewardFee = relationAgentRewardFee;
        }
    }

    public static class Carrier extends Upgrade {
        private int directPlusUserNum;
        private int teamPlusUserNum;

        public int getDirectPlusUserNum() {
            return directPlusUserNum;
        }

        public void setDirectPlusUserNum(int directPlusUserNum) {
            this.directPlusUserNum = directPlusUserNum;
        }

        public int getTeamPlusUserNum() {
            return teamPlusUserNum;
        }

        public void setTeamPlusUserNum(int teamPlusUserNum) {
            this.teamPlusUserNum = teamPlusUserNum;
        }
    }

    public static class Chief extends Upgrade {
        private int directCarrierNum;
        private int teamCarrierNum;

        public int getDirectCarrierNum() {
            return directCarrierNum;
        }

        public void setDirectCarrierNum(int directCarrierNum) {
            this.directCarrierNum = directCarrierNum;
        }

        public int getTeamCarrierNum() {
            return teamCarrierNum;
        }

        public void setTeamCarrierNum(int teamCarrierNum) {
            this.teamCarrierNum = teamCarrierNum;
        }
    }

    public static class Upgrade implements Serializable{
        private float priceForForever;
        private float pricePerMonth;
        private float pricePerYear;
        private UserUpgradePayType payType;
        private boolean useIntegral;
        private boolean useMoney;

        public boolean isUseMoney() {
            return useMoney;
        }

        public void setUseMoney(boolean useMoney) {
            this.useMoney = useMoney;
        }

        public boolean isUseIntegral() {
            return useIntegral;
        }

        public void setUseIntegral(boolean useIntegral) {
            this.useIntegral = useIntegral;
        }

        public float getPriceByTimeUnit(TimeUnit timeUnit) {
            switch (timeUnit) {
                case MONTH:
                    return pricePerMonth;
                case YEAR:
                    return pricePerYear;
                case FOREVER:
                    return priceForForever;
                default:
                    return 0;
            }
        }

        public UserUpgradePayType getPayType() {
            return payType;
        }

        public void setPayType(UserUpgradePayType payType) {
            this.payType = payType;
        }

        public float getPriceForForever() {
            return priceForForever;
        }

        public void setPriceForForever(float priceForForever) {
            this.priceForForever = priceForForever;
        }

        public float getPricePerMonth() {
            return pricePerMonth;
        }

        public void setPricePerMonth(float pricePerMonth) {
            this.pricePerMonth = pricePerMonth;
        }

        public float getPricePerYear() {
            return pricePerYear;
        }

        public void setPricePerYear(float pricePerYear) {
            this.pricePerYear = pricePerYear;
        }
    }

}
