package info.batcloud.fanli.core.service;

import java.util.List;
import java.util.Map;

public interface ChannelCommissionItemService {

    Map<Long, List<Long>> findChannelIdListByItemIdList(List<Long> itemIds);

    void addCommissionItemToChannel(long itemId, long channelId);

    void deleteByChannelId(long channelId);
}
