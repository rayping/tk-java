package info.batcloud.fanli.core.service;

import java.io.OutputStream;

public interface TaobaoNewerService {

    ShareInfo findUserShareInfo(long userId);
    ShareInfo findShareInfoByPid(String pid);

    String genSharePicSvgXml(String url);

    void genSharePic(String url, OutputStream os);

    class ShareInfo {
        private String shortUrl;
        private String url;
        private String qrCodeUrl;
        private String kl;
        private String text;
        private String pid;

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getShortUrl() {
            return shortUrl;
        }

        public void setShortUrl(String shortUrl) {
            this.shortUrl = shortUrl;
        }

        public String getQrCodeUrl() {
            return qrCodeUrl;
        }

        public void setQrCodeUrl(String qrCodeUrl) {
            this.qrCodeUrl = qrCodeUrl;
        }

        public String getKl() {
            return kl;
        }

        public void setKl(String kl) {
            this.kl = kl;
        }
    }

}
