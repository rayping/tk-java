package info.batcloud.fanli.core.service;

public interface InvitationCodeService {

    void generate(long userId);

}
