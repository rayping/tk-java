package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.entity.WeixinAccount;
import info.batcloud.fanli.core.repository.WeixinAccountRepository;
import info.batcloud.fanli.core.service.WeixinAccountService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class WeixinAccountServiceImpl implements WeixinAccountService {

    @Inject
    private WeixinAccountRepository weixinAccountRepository;

    @Override
    public WeixinAccount findAccountByOpenid(String openid) {
        return weixinAccountRepository.findByOpenId(openid);
    }

}
