package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.UserAgentApplicationStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
public class UserAgentApplication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    private Date applyTime;

    @Enumerated(EnumType.STRING)
    private UserAgentApplicationStatus status;

    public UserAgentApplicationStatus getStatus() {
        return status;
    }

    public void setStatus(UserAgentApplicationStatus status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }
}
