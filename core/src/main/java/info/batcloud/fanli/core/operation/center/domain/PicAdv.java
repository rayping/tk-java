package info.batcloud.fanli.core.operation.center.domain;

import info.batcloud.fanli.core.helper.OSSImageHelper;

import java.io.Serializable;

public class PicAdv extends Adv implements Serializable {

    private String pic;

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPicUrl() {
        return OSSImageHelper.toUrl(getPic());
    }
}
