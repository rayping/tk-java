package info.batcloud.fanli.core.entity;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import info.batcloud.fanli.core.enums.CommissionItemStatus;
import org.apache.commons.lang.StringUtils;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@DiscriminatorValue("CommissionItem")
public class CommissionItem extends Item {

    private int sales;

    @Enumerated(EnumType.STRING)
    private CommissionItemStatus status;

    private String sourceItemClickUrl;

    private Long sourceCatId;

    private String itemPlace; //货源地

    private Float commissionRate;

    private boolean highCommission;

    private boolean coupon;

    private String couponClickUrl;

    private Date couponStartTime;

    private Date couponEndTime;

    private String couponInfo;

    private Integer couponTotalCount;

    private Integer couponRemainCount;

    private Float couponValue;

    private boolean recommend;

    //是否京东自营
    private boolean jdSale;

    private String descImgs;

    private boolean tbkZt;

    public boolean isTbkZt() {
        return tbkZt;
    }

    public void setTbkZt(boolean tbkZt) {
        this.tbkZt = tbkZt;
    }

    public void setDescImgList(List<String> list) {
        this.setDescImgs(JSON.toJSONString(list));
    }

    public List<String> getDescImgList() {
        if (StringUtils.isBlank(this.getDescImgs())) {
            return new ArrayList<>();
        }
        return JSON.parseObject(this.getDescImgs(), new TypeReference<List<String>>() {
        });
    }

    public String getDescImgs() {
        return descImgs;
    }

    public void setDescImgs(String descImgs) {
        this.descImgs = descImgs;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public CommissionItemStatus getStatus() {
        return status;
    }

    public void setStatus(CommissionItemStatus status) {
        this.status = status;
    }

    public String getSourceItemClickUrl() {
        return sourceItemClickUrl;
    }

    public void setSourceItemClickUrl(String sourceItemClickUrl) {
        this.sourceItemClickUrl = sourceItemClickUrl;
    }

    public Long getSourceCatId() {
        return sourceCatId;
    }

    public void setSourceCatId(Long sourceCatId) {
        this.sourceCatId = sourceCatId;
    }

    public String getItemPlace() {
        return itemPlace;
    }

    public void setItemPlace(String itemPlace) {
        this.itemPlace = itemPlace;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public boolean isHighCommission() {
        return highCommission;
    }

    public void setHighCommission(boolean highCommission) {
        this.highCommission = highCommission;
    }

    public boolean isCoupon() {
        return coupon;
    }

    public void setCoupon(boolean coupon) {
        this.coupon = coupon;
    }

    public String getCouponClickUrl() {
        return couponClickUrl;
    }

    public void setCouponClickUrl(String couponClickUrl) {
        this.couponClickUrl = couponClickUrl;
    }

    public Date getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(Date couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public Date getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(Date couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public String getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }

    public Integer getCouponTotalCount() {
        return couponTotalCount;
    }

    public void setCouponTotalCount(Integer couponTotalCount) {
        this.couponTotalCount = couponTotalCount;
    }

    public Integer getCouponRemainCount() {
        return couponRemainCount;
    }

    public void setCouponRemainCount(Integer couponRemainCount) {
        this.couponRemainCount = couponRemainCount;
    }

    public Float getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(Float couponValue) {
        this.couponValue = couponValue;
    }

    public boolean isRecommend() {
        return recommend;
    }

    public void setRecommend(boolean recommend) {
        this.recommend = recommend;
    }

    public boolean isJdSale() {
        return jdSale;
    }

    public void setJdSale(boolean jdSale) {
        this.jdSale = jdSale;
    }

}
