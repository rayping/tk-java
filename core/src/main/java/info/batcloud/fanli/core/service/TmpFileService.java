package info.batcloud.fanli.core.service;

import java.io.File;

public interface TmpFileService {

    File createFile(String path);

}
