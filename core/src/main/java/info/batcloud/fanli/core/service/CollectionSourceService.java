package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.dto.CollectionSourceAddDTO;
import info.batcloud.fanli.core.dto.CollectionSourceDTO;
import info.batcloud.fanli.core.dto.CollectionSourceUpdateDTO;

import java.util.List;

public interface CollectionSourceService {

    List<CollectionSourceDTO> findAll();

    void saveCollectionSource(CollectionSourceAddDTO source);

    void updateCollectionSource(long id, CollectionSourceUpdateDTO source);
}
