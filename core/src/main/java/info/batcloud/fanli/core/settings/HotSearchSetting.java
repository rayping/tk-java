package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class HotSearchSetting implements Serializable {

    private String hotKeywords;

    public String getHotKeywords() {
        return hotKeywords;
    }

    public void setHotKeywords(String hotKeywords) {
        this.hotKeywords = hotKeywords;
    }
}
