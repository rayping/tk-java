package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.OrderPayType;
import info.batcloud.fanli.core.enums.WalletFlowDetailType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@Table(name = "lxk_order")
public abstract class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    private Date createTime;

    //所需费用
    private float totalFee;

    private Date updateTime;

    private float paidFee;

    private float paidIntegral;

    private Date payTime;

    @Enumerated(EnumType.STRING)
    private OrderPayType payType;

    //转账交易号，这里仅接受支付宝的支付交易订单号
    private String alipayTransferTradeNo;

    @Version
    private int version;

    public String getAlipayTransferTradeNo() {
        return alipayTransferTradeNo;
    }

    public void setAlipayTransferTradeNo(String alipayTransferTradeNo) {
        this.alipayTransferTradeNo = alipayTransferTradeNo;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public abstract WalletFlowDetailType walletFlowDetailType();

    public float getPaidIntegral() {
        return paidIntegral;
    }

    public void setPaidIntegral(float paidIntegral) {
        this.paidIntegral = paidIntegral;
    }

    public OrderPayType getPayType() {
        return payType;
    }

    public void setPayType(OrderPayType payType) {
        this.payType = payType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public float getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(float totalFee) {
        this.totalFee = totalFee;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public float getPaidFee() {
        return paidFee;
    }

    public void setPaidFee(float paidFee) {
        this.paidFee = paidFee;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }
}
