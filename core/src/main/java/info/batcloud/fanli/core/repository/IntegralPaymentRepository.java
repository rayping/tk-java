package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.IntegralPayment;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IntegralPaymentRepository extends PagingAndSortingRepository<IntegralPayment, Long> {
}
