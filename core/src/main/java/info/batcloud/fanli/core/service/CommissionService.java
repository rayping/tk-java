package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.enums.UserLevel;

public interface CommissionService {

    /**
     * 根据层数判断佣金比例，从1开始
     * */
    float determineCommissionRate(float remainCommissionRate, UserLevel userLevel);

    float determineRewardCommissionFee(float value);
    float determineRewardCommissionFee(float rewardCommissionRate, float value);
}
