package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum UserWithdrawStatus implements EnumTitle {

    WAIT_VERIFY, WAIT_TRANSFER, RETRY_TRANSFER,
    ON_TRANSFER, VERIFY_FAIL,
    TRANSFER_SUCCESS,
    TRANSFER_FAIL;

    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
