package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.ItemSelection;
import info.batcloud.fanli.core.enums.ItemSelectionStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ItemSelectionRepository extends PagingAndSortingRepository<ItemSelection, Long>, JpaSpecificationExecutor<ItemSelection> {

    List<ItemSelection> findByStatusIsNot(ItemSelectionStatus status);

    List<ItemSelection> findByStatusAndAutoFetch(ItemSelectionStatus status, boolean autoFetch);

}
