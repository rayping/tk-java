package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum EcomPlat implements EnumTitle{

    TAOBAO, JD, TMALL, PDD;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }
}
