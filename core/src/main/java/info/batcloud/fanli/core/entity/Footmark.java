package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.FootmarkType;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Footmark {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private Item item;

    private Date createTime;

    @Enumerated(EnumType.STRING)
    private FootmarkType type;

    public FootmarkType getType() {
        return type;
    }

    public void setType(FootmarkType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
