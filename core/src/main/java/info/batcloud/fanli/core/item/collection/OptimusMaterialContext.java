package info.batcloud.fanli.core.item.collection;

public class OptimusMaterialContext extends Context{

    private Long materialId;

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }
}
