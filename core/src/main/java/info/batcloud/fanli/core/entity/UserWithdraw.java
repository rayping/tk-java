package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.FundTransferType;
import info.batcloud.fanli.core.enums.UserWithdrawStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
public class UserWithdraw {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private User user;

    private Float money;

    private Date createTime;

    @Enumerated(EnumType.STRING)
    private UserWithdrawStatus status;

    @Enumerated(EnumType.STRING)
    private FundTransferType fundTransferType;

    private String payeeAccount;

    private String payeeName;

    //操作时间
    private Date updateTime;

    private Long walletFlowDetailId;

    private Long fundTransferOrderId;

    private String remark;

    private String payDate;

    private String alipayOrderId;

    public FundTransferType getFundTransferType() {
        return fundTransferType;
    }

    public void setFundTransferType(FundTransferType fundTransferType) {
        this.fundTransferType = fundTransferType;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getAlipayOrderId() {
        return alipayOrderId;
    }

    public void setAlipayOrderId(String alipayOrderId) {
        this.alipayOrderId = alipayOrderId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getFundTransferOrderId() {
        return fundTransferOrderId;
    }

    public void setFundTransferOrderId(Long fundTransferOrderId) {
        this.fundTransferOrderId = fundTransferOrderId;
    }

    public String getPayeeAccount() {
        return payeeAccount;
    }

    public void setPayeeAccount(String payeeAccount) {
        this.payeeAccount = payeeAccount;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public Long getWalletFlowDetailId() {
        return walletFlowDetailId;
    }

    public void setWalletFlowDetailId(Long walletFlowDetailId) {
        this.walletFlowDetailId = walletFlowDetailId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Float getMoney() {
        return money;
    }

    public void setMoney(Float money) {
        this.money = money;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public UserWithdrawStatus getStatus() {
        return status;
    }

    public void setStatus(UserWithdrawStatus status) {
        this.status = status;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
