package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.AppPlatform;
import info.batcloud.fanli.core.enums.BundleJsPackageStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
public class BundleJsPackage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private boolean useCache;
    @Enumerated(EnumType.STRING)
    private AppPlatform platform;

    private String platformVersion;

    @Enumerated(EnumType.STRING)
    private BundleJsPackageStatus status;

    private Date createTime;

    private String version;

    private boolean iosShow;

    private boolean showUpdate;

    public boolean isShowUpdate() {
        return showUpdate;
    }

    public void setShowUpdate(boolean showUpdate) {
        this.showUpdate = showUpdate;
    }

    public boolean isIosShow() {
        return iosShow;
    }

    public void setIosShow(boolean iosShow) {
        this.iosShow = iosShow;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    public BundleJsPackageStatus getStatus() {
        return status;
    }

    public void setStatus(BundleJsPackageStatus status) {
        this.status = status;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isUseCache() {
        return useCache;
    }

    public void setUseCache(boolean useCache) {
        this.useCache = useCache;
    }

    public AppPlatform getPlatform() {
        return platform;
    }

    public void setPlatform(AppPlatform platform) {
        this.platform = platform;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
