package info.batcloud.fanli.core.enums;

public enum CollectionSourceStatus {

    STARTED("采集中"), STOPPED("已停止"), DELETED("已删除");
    public String title;

    CollectionSourceStatus(String title) {
        this.title = title;
    }

}
