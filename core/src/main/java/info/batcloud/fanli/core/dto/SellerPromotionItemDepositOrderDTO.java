package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.OrderPayType;
import info.batcloud.fanli.core.enums.SellerPromotionItemDepositOrderStatus;

import java.util.Date;

public class SellerPromotionItemDepositOrderDTO {

    private Long id;

    private Long userId;

    private String userPhone;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    //所需费用
    private float totalFee;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    private float paidFee;

    private float paidIntegral;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date payTime;

    private OrderPayType payType;

    //转账交易号，这里仅接受支付宝的支付交易订单号
    private String alipayTransferTradeNo;

    private String sellerPromotionItemTitle;

    private String sellerPromotionItemPicUrl;

    private String sellerPromotionItemShopTitle;

    private float sellerPromotionItemPrice;

    private float sellerPromotionItemOriginPrice;

    private String sellerPromotionItemUrl;

    private EcomPlat sellerPromotionItemEcomPlat;

    private Long sellerPromotionItemId;

    private SellerPromotionItemDepositOrderStatus status;

    private float rebateFee;

    private String verifyRemark;

    private int num;

    public String getVerifyRemark() {
        return verifyRemark;
    }

    public void setVerifyRemark(String verifyRemark) {
        this.verifyRemark = verifyRemark;
    }

    public String getSellerPromotionItemEcomPlatTitle() {
        return sellerPromotionItemEcomPlat == null ? null : sellerPromotionItemEcomPlat.getTitle();
    }

    public String getSellerPromotionItemUrl() {
        return sellerPromotionItemUrl;
    }

    public void setSellerPromotionItemUrl(String sellerPromotionItemUrl) {
        this.sellerPromotionItemUrl = sellerPromotionItemUrl;
    }

    public EcomPlat getSellerPromotionItemEcomPlat() {
        return sellerPromotionItemEcomPlat;
    }

    public void setSellerPromotionItemEcomPlat(EcomPlat sellerPromotionItemEcomPlat) {
        this.sellerPromotionItemEcomPlat = sellerPromotionItemEcomPlat;
    }

    public float getSellerPromotionItemOriginPrice() {
        return sellerPromotionItemOriginPrice;
    }

    public void setSellerPromotionItemOriginPrice(float sellerPromotionItemOriginPrice) {
        this.sellerPromotionItemOriginPrice = sellerPromotionItemOriginPrice;
    }

    public float getSellerPromotionItemPrice() {
        return sellerPromotionItemPrice;
    }

    public void setSellerPromotionItemPrice(float sellerPromotionItemPrice) {
        this.sellerPromotionItemPrice = sellerPromotionItemPrice;
    }

    public float getRebateFee() {
        return rebateFee;
    }

    public void setRebateFee(float rebateFee) {
        this.rebateFee = rebateFee;
    }

    public String getSellerPromotionItemShopTitle() {
        return sellerPromotionItemShopTitle;
    }

    public void setSellerPromotionItemShopTitle(String sellerPromotionItemShopTitle) {
        this.sellerPromotionItemShopTitle = sellerPromotionItemShopTitle;
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public String getPayTypeTitle() {
        return payType == null ? null : payType.getTitle();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public float getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(float totalFee) {
        this.totalFee = totalFee;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public float getPaidFee() {
        return paidFee;
    }

    public void setPaidFee(float paidFee) {
        this.paidFee = paidFee;
    }

    public float getPaidIntegral() {
        return paidIntegral;
    }

    public void setPaidIntegral(float paidIntegral) {
        this.paidIntegral = paidIntegral;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public OrderPayType getPayType() {
        return payType;
    }

    public void setPayType(OrderPayType payType) {
        this.payType = payType;
    }

    public String getAlipayTransferTradeNo() {
        return alipayTransferTradeNo;
    }

    public void setAlipayTransferTradeNo(String alipayTransferTradeNo) {
        this.alipayTransferTradeNo = alipayTransferTradeNo;
    }

    public String getSellerPromotionItemTitle() {
        return sellerPromotionItemTitle;
    }

    public void setSellerPromotionItemTitle(String sellerPromotionItemTitle) {
        this.sellerPromotionItemTitle = sellerPromotionItemTitle;
    }

    public String getSellerPromotionItemPicUrl() {
        return sellerPromotionItemPicUrl;
    }

    public void setSellerPromotionItemPicUrl(String sellerPromotionItemPicUrl) {
        this.sellerPromotionItemPicUrl = sellerPromotionItemPicUrl;
    }

    public Long getSellerPromotionItemId() {
        return sellerPromotionItemId;
    }

    public void setSellerPromotionItemId(Long sellerPromotionItemId) {
        this.sellerPromotionItemId = sellerPromotionItemId;
    }

    public SellerPromotionItemDepositOrderStatus getStatus() {
        return status;
    }

    public void setStatus(SellerPromotionItemDepositOrderStatus status) {
        this.status = status;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
