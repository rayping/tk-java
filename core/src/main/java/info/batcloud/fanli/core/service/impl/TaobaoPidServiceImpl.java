package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.entity.TaobaoPid;
import info.batcloud.fanli.core.repository.TaobaoPidRepository;
import info.batcloud.fanli.core.service.TaobaoPidService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Date;

@Service
public class TaobaoPidServiceImpl implements TaobaoPidService {

    @Inject
    private TaobaoPidRepository taobaoPidRepository;

    @Override
    public TaobaoPid fetch() {
        TaobaoPid pid = taobaoPidRepository.findTopByUsedOrderByIdAsc(false);
        pid.setUsed(true);
        taobaoPidRepository.save(pid);
        return pid;
    }

    @Override
    public void save(String value) {
        TaobaoPid p = new TaobaoPid();
        p.setValue(value);
        p.setCreateTime(new Date());
        p.setUsed(false);
        String[] tmps = value.split("_");
        p.setTaobaoUnionId(Long.valueOf(tmps[1]));
        p.setSiteId(Long.valueOf(tmps[2]));
        p.setAdzoneId(Long.valueOf(tmps[3]));
        taobaoPidRepository.save(p);
    }
}
