package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.service.ActivityService;
import info.batcloud.fanli.core.service.FlashSaleOrderService;
import info.batcloud.fanli.core.service.FreeChargeActivityOrderService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class ActivityServiceImpl implements ActivityService {

    @Inject
    private FlashSaleOrderService flashSaleOrderService;

    @Override
    public boolean isCommissionItemJoinFlashSaleWithUser(long userId, long commissionItemId) {
        /**
         * TODO
         * 当前只检查一个活动，后续可能会检查多个活动，在这里修改
         * */
        return flashSaleOrderService.checkHasWaitVerifyOrderByUserIdAndItemId(userId, commissionItemId);
    }
}
