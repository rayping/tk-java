package info.batcloud.fanli.core.service;

public interface UserAgentApplicationService {

    boolean applyFreeAgent(long userId);

    int findTodayRemainFreeAgentNum();
}
