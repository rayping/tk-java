package info.batcloud.fanli.core.component;

import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.LaxiaokeOpenSetting;
import info.batcloud.laxiaoke.open.DefaultLxkClient;
import info.batcloud.laxiaoke.open.LxkClient;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class LxkClientFactory {

    @Inject
    private SystemSettingService systemSettingService;

    public LxkClient getLxkClient() {
        LaxiaokeOpenSetting laxiaokeOpenSetting = systemSettingService.findActiveSetting(LaxiaokeOpenSetting.class);
        LxkClient client = new DefaultLxkClient(laxiaokeOpenSetting.getApiUrl(), laxiaokeOpenSetting.getOpenId());
        return client;
    }

}
