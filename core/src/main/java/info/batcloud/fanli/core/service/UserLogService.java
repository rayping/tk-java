package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.enums.UserLogType;

import java.util.Date;

public interface UserLogService {

    LogResult log(LogParam param);

    class LogResult {
        private int totalTimes;
        private Date lastLogTime;

        public Date getLastLogTime() {
            return lastLogTime;
        }

        public void setLastLogTime(Date lastLogTime) {
            this.lastLogTime = lastLogTime;
        }

        public int getTotalTimes() {
            return totalTimes;
        }

        public void setTotalTimes(int totalTimes) {
            this.totalTimes = totalTimes;
        }
    }
    class LogParam {
        private Long userId;
        private UserLogType type;
        private String ip;

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public UserLogType getType() {
            return type;
        }

        public void setType(UserLogType type) {
            this.type = type;
        }
    }

}
