package info.batcloud.fanli.core.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("background-task-server")
public interface BackgroundTaskClient {

    @GetMapping("/collection-source/collect/{sourceId}")
    boolean collectItemBySourceId(@PathVariable("sourceId") long sourceId);

}
