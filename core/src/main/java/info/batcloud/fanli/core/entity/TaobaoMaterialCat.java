package info.batcloud.fanli.core.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import info.batcloud.fanli.core.domain.TaobaoMaterial;
import info.batcloud.fanli.core.enums.TaobaoMaterialCatStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 淘宝物料组
 */
@Entity
public class TaobaoMaterialCat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Date createTime;

    private String materialListJson;

    @Transient
    private List<TaobaoMaterial> materialList;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TaobaoMaterialCatStatus status;

    public TaobaoMaterialCatStatus getStatus() {
        return status;
    }

    public void setStatus(TaobaoMaterialCatStatus status) {
        this.status = status;
    }

    public List<TaobaoMaterial> getMaterialList() {
        if (materialList != null) {
            return materialList;
        }
        return materialListJson == null ? Collections.EMPTY_LIST : JSON.parseObject(materialListJson, new TypeReference<List<TaobaoMaterial>>() {
        });
    }

    public void setMaterialList(List<TaobaoMaterial> list) {
        this.materialList = list;
        this.materialListJson = JSON.toJSONString(list);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
