package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.ServiceItem;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ServiceItemRepository extends PagingAndSortingRepository<ServiceItem, Long>, JpaSpecificationExecutor<ServiceItem> {
}
