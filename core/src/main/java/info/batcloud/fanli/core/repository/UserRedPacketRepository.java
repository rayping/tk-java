package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.UserRedPacket;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;

public interface UserRedPacketRepository extends PagingAndSortingRepository<UserRedPacket, Long>, JpaSpecificationExecutor<UserRedPacket> {

    int countByUserIdAndRedPacketId(long userId, long redPacketId);

    int countByUserIdAndRedPacketIdAndCreateTimeBetween(long userId, long redPackedId, Date startTime, Date endTime);

}
