package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.UserServiceSubscription;
import info.batcloud.fanli.core.enums.Service;
import info.batcloud.fanli.core.enums.UserServiceSubscriptionStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserServiceSubscriptionRepository extends PagingAndSortingRepository<UserServiceSubscription, Long>, JpaSpecificationExecutor<UserServiceSubscription> {

    UserServiceSubscription findByUserIdAndServiceAndStatusIsNot(long userId, Service service, UserServiceSubscriptionStatus status);
    UserServiceSubscription findByUserIdAndServiceAndStatus(long userId, Service service, UserServiceSubscriptionStatus status);

    List<UserServiceSubscription> findByUserIdAndService(long userId, Service service);
}
