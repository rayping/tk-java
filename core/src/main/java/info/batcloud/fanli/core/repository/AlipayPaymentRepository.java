package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.AlipayPayment;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AlipayPaymentRepository extends PagingAndSortingRepository<AlipayPayment, Long> {
}
