package info.batcloud.pdd.sdk;

import com.alibaba.fastjson.annotation.JSONField;

public abstract class PddResponse  {

    //{"error_response":{"error_code":10002,"error_msg":"请求方法错误，仅支持POST"}}

    @JSONField( name = "error_response")
    private Error error;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public static class Error {
        @JSONField(name = "error_code")
        private int errorCode;
        @JSONField(name = "error_msg")
        private String errorMsg;

        public int getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(int errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorMsg() {
            return errorMsg;
        }

        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }
    }

}
