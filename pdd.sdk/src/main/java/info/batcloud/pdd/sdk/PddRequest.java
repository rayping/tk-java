package info.batcloud.pdd.sdk;

public interface PddRequest<T extends PddResponse> {

    String getType();

    Class<T> getResponseClass();

}
