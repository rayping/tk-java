package info.batcloud.pdd.sdk.request.ddk;

import info.batcloud.pdd.sdk.PddRequest;
import info.batcloud.pdd.sdk.response.ddk.GoodsDetailResponse;
import info.batcloud.pdd.sdk.annotation.Param;

import java.util.List;

public class GoodsDetailRequest implements PddRequest<GoodsDetailResponse> {

    @Param("goods_id_list")
    private List<Long> goodsIdList;

    public List<Long> getGoodsIdList() {
        return goodsIdList;
    }

    public void setGoodsIdList(List<Long> goodsIdList) {
        this.goodsIdList = goodsIdList;
    }

    @Override
    public String getType() {
        return "pdd.ddk.goods.detail";
    }

    @Override
    public Class<GoodsDetailResponse> getResponseClass() {
        return GoodsDetailResponse.class;
    }
}
