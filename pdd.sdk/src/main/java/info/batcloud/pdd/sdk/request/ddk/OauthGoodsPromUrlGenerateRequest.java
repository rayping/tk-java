package info.batcloud.pdd.sdk.request.ddk;

import info.batcloud.pdd.sdk.PddRequest;
import info.batcloud.pdd.sdk.annotation.Param;
import info.batcloud.pdd.sdk.response.ddk.OauthGoodsPromUrlGenerateResponse;

import java.util.List;

public class OauthGoodsPromUrlGenerateRequest implements PddRequest<OauthGoodsPromUrlGenerateResponse> {

    @Param("p_id")
    private String pid;

    @Param("goods_id_list")
    private List<Long> goodsIdList;

    @Param("generate_short_url")
    private boolean generateShortUrl;

    @Param("custom_parameters")
    private String customParameters;

    public String getCustomParameters() {
        return customParameters;
    }

    public void setCustomParameters(String customParameters) {
        this.customParameters = customParameters;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<Long> getGoodsIdList() {
        return goodsIdList;
    }

    public void setGoodsIdList(List<Long> goodsIdList) {
        this.goodsIdList = goodsIdList;
    }

    public boolean isGenerateShortUrl() {
        return generateShortUrl;
    }

    public void setGenerateShortUrl(boolean generateShortUrl) {
        this.generateShortUrl = generateShortUrl;
    }

    @Override
    public String getType() {
        return "pdd.ddk.oauth.goods.prom.url.generate";
    }

    @Override
    public Class<OauthGoodsPromUrlGenerateResponse> getResponseClass() {
        return OauthGoodsPromUrlGenerateResponse.class;
    }
}
