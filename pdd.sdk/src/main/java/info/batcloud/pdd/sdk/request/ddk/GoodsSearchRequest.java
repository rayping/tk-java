package info.batcloud.pdd.sdk.request.ddk;

import info.batcloud.pdd.sdk.PddRequest;
import info.batcloud.pdd.sdk.annotation.Param;
import info.batcloud.pdd.sdk.response.ddk.GoodsSearchResponse;

public class GoodsSearchRequest implements PddRequest<GoodsSearchResponse> {

    @Param
    private String keyword;

    @Param("opt_id")
    private Integer optId;

    @Param
    private int page;

    @Param("page_size")
    private Integer pageSize;

    @Param("sort_type")
    private int sortType;

    @Param("with_coupon")
    private boolean withCoupon;

    @Param("cat_id")
    private Long catId;

    @Param("goods_id_list")
    private String goodsIdList;

    @Param("zs_duo_id")
    private Long zsDuoId;

    @Param("merchant_type")
    private Integer merchantType;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getOptId() {
        return optId;
    }

    public void setOptId(Integer optId) {
        this.optId = optId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public int getSortType() {
        return sortType;
    }

    public void setSortType(int sortType) {
        this.sortType = sortType;
    }

    public boolean isWithCoupon() {
        return withCoupon;
    }

    public void setWithCoupon(boolean withCoupon) {
        this.withCoupon = withCoupon;
    }

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public String getGoodsIdList() {
        return goodsIdList;
    }

    public void setGoodsIdList(String goodsIdList) {
        this.goodsIdList = goodsIdList;
    }

    public Long getZsDuoId() {
        return zsDuoId;
    }

    public void setZsDuoId(Long zsDuoId) {
        this.zsDuoId = zsDuoId;
    }

    public Integer getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(Integer merchantType) {
        this.merchantType = merchantType;
    }

    @Override
    public String getType() {
        return "pdd.ddk.goods.search";
    }

    @Override
    public Class<GoodsSearchResponse> getResponseClass() {
        return GoodsSearchResponse.class;
    }
}
