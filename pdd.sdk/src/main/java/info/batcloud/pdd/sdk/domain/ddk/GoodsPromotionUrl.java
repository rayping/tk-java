package info.batcloud.pdd.sdk.domain.ddk;

import com.alibaba.fastjson.annotation.JSONField;

public class GoodsPromotionUrl {

    @JSONField(name = "we_app_web_view_short_url")
    private String weAppWebViewShortUrl;

    @JSONField(name = "we_app_web_wiew_url")
    private String weAppWebViewUrl;

    @JSONField(name = "mobile_short_url")
    private String mobileShortUrl;

    @JSONField(name = "mobile_url")
    private String mobileUrl;

    @JSONField(name = "short_url")
    private String shortUrl;

    @JSONField(name = "url")
    private String url;

    public String getWeAppWebViewShortUrl() {
        return weAppWebViewShortUrl;
    }

    public void setWeAppWebViewShortUrl(String weAppWebViewShortUrl) {
        this.weAppWebViewShortUrl = weAppWebViewShortUrl;
    }

    public String getWeAppWebViewUrl() {
        return weAppWebViewUrl;
    }

    public void setWeAppWebViewUrl(String weAppWebViewUrl) {
        this.weAppWebViewUrl = weAppWebViewUrl;
    }

    public String getMobileShortUrl() {
        return mobileShortUrl;
    }

    public void setMobileShortUrl(String mobileShortUrl) {
        this.mobileShortUrl = mobileShortUrl;
    }

    public String getMobileUrl() {
        return mobileUrl;
    }

    public void setMobileUrl(String mobileUrl) {
        this.mobileUrl = mobileUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
