-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED='0f443e6c-fa67-11e8-87f3-7cd30ad3d02c:1-706349,
16f054c3-d758-11e8-bdf5-6c92bf203fcb:1-5790536,
2adaae26-d758-11e8-a983-f84abf5e31a0:1-124174,
a042dea3-5847-11ea-b88f-6c92bf3b9b8f:1-183545,
b3578897-7b59-11ea-ab01-6c92bf3bac77:1-385522';

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `last_login_time` datetime NOT NULL,
  `login_times` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `type` varchar(20) NOT NULL,
  `open_id` varchar(100) DEFAULT NULL,
  `union_id` varchar(100) DEFAULT NULL,
  `nickname` varchar(45) DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `avatar_url` varchar(1000) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `taobao_user_nick` varchar(100) DEFAULT NULL,
  `access_token` varchar(100) DEFAULT NULL,
  `refresh_token` varchar(100) DEFAULT NULL,
  `taobao_user_id` bigint(20) DEFAULT NULL,
  `taobao_open_id` varchar(100) DEFAULT NULL,
  `expires_in` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_OPEN_ID` (`open_id`),
  KEY `IDX_UNION_ID` (`union_id`),
  KEY `IDX_OPEN_SID` (`access_token`)
) ENGINE=InnoDB AUTO_INCREMENT=774 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adv`
--

DROP TABLE IF EXISTS `adv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `create_time` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `valid` bit(1) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `pic` varchar(1000) NOT NULL,
  `page` varchar(45) NOT NULL,
  `params` varchar(500) NOT NULL,
  `pic_width` int(11) NOT NULL,
  `pic_height` int(11) NOT NULL,
  `crowd_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adv_space`
--

DROP TABLE IF EXISTS `adv_space`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adv_space` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `code` varchar(45) NOT NULL,
  `create_time` datetime NOT NULL,
  `valid` bit(1) NOT NULL,
  `deleted` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adv_space_item`
--

DROP TABLE IF EXISTS `adv_space_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adv_space_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adv_space_id` int(11) NOT NULL,
  `adv_id` int(11) NOT NULL,
  `idx` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `expire_time` datetime NOT NULL,
  `perpetual` bit(1) NOT NULL,
  `status` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_UPDATE_TIME` (`update_time`),
  KEY `IDX_EXPIRE_TIME` (`expire_time`),
  KEY `IDX_CITY` (`city_id`),
  KEY `IDX_DISTRICT` (`district_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent_apply`
--

DROP TABLE IF EXISTS `agent_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `status` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `contact_name` varchar(45) NOT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `share_times` int(11) NOT NULL,
  `fit_share_times` int(11) NOT NULL,
  `pic_list_json` text CHARACTER SET utf8 NOT NULL,
  `deleted` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bundle_js_package`
--

DROP TABLE IF EXISTS `bundle_js_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bundle_js_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform` varchar(45) NOT NULL,
  `platform_version` varchar(45) NOT NULL,
  `create_time` datetime NOT NULL,
  `version` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `use_cache` bit(1) NOT NULL DEFAULT b'0',
  `ios_show` bit(1) NOT NULL DEFAULT b'0',
  `show_update` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_PLATFORM` (`platform`),
  KEY `IDX_PLATFORM_VERSION` (`platform_version`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `create_time` datetime NOT NULL,
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `channel_commission_item`
--

DROP TABLE IF EXISTS `channel_commission_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_commission_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `commission_item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CHANNEL_ID` (`channel_id`),
  KEY `IDX_COMMISSION_ID` (`commission_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=974439 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `collection_source`
--

DROP TABLE IF EXISTS `collection_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collection_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `status` varchar(45) NOT NULL,
  `create_time` datetime NOT NULL,
  `last_collect_start_time` datetime DEFAULT NULL,
  `type` varchar(45) NOT NULL DEFAULT 'KEYWORD',
  `keyword` varchar(200) DEFAULT NULL,
  `max_page` int(11) NOT NULL,
  `page_size` int(11) NOT NULL,
  `des` varchar(1000) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `stop_time` datetime DEFAULT NULL,
  `last_collect_end_time` datetime DEFAULT NULL,
  `collect_status` varchar(45) DEFAULT NULL,
  `collect_seconds` int(11) DEFAULT '0',
  `collect_msg` text,
  `cate_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commission_order`
--

DROP TABLE IF EXISTS `commission_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commission_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL,
  `item_title` varchar(500) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `shop_title` varchar(200) NOT NULL,
  `item_num` int(11) NOT NULL,
  `price` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `ecom_plat` varchar(10) NOT NULL,
  `estimate_commission_fee` float NOT NULL,
  `estimate_commission_rate` float NOT NULL,
  `commission_rate` float NOT NULL,
  `commission_fee` float NOT NULL,
  `pay_fee` float NOT NULL,
  `order_no` varchar(45) NOT NULL,
  `item_pic_url` varchar(1000) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `commission_item_id` int(11) DEFAULT NULL,
  `settled_time` datetime DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `allot_fee` float DEFAULT NULL,
  `plat_settled_time` datetime DEFAULT NULL,
  `allocated` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `IDX_ORDER_NO` (`order_no`),
  KEY `IDX_ITEM_ID` (`item_id`),
  KEY `IDX_COMMISSION_ITEM_ID` (`commission_item_id`),
  KEY `IDX_PLAT_SETTLED_TIME` (`plat_settled_time`)
) ENGINE=InnoDB AUTO_INCREMENT=20253 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crowd`
--

DROP TABLE IF EXISTS `crowd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crowd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `min_create_time` datetime DEFAULT NULL,
  `max_create_time` datetime DEFAULT NULL,
  `min_active_time` datetime DEFAULT NULL,
  `max_active_time` datetime DEFAULT NULL,
  `user_level` varchar(45) DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flash_sale_item`
--

DROP TABLE IF EXISTS `flash_sale_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flash_sale_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `season` varchar(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `status` varchar(45) NOT NULL,
  `slogan` varchar(500) DEFAULT NULL,
  `total_num` int(11) NOT NULL,
  `drawn_num` int(11) NOT NULL,
  `last_drawn_time` datetime DEFAULT NULL,
  `user_level` varchar(45) NOT NULL,
  `version` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `return_fee` float NOT NULL DEFAULT '0',
  `label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ITEM_ID` (`item_id`),
  KEY `IDX_SEASON` (`season`),
  KEY `IDX_DATE` (`date`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flash_sale_order`
--

DROP TABLE IF EXISTS `flash_sale_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flash_sale_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `item_id` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `commission_order_Id` int(11) DEFAULT NULL,
  `status` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flash_sale_item_id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `free_fee` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_COMMISSION_ITEM_ID` (`commission_order_Id`),
  KEY `IDX_FLASH_SALE_ITEM_ID` (`flash_sale_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=371 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `footmark`
--

DROP TABLE IF EXISTS `footmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `type` varchar(45) NOT NULL DEFAULT 'HISTORY',
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_ITEM_ID` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=118757 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_charge_activity`
--

DROP TABLE IF EXISTS `free_charge_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_charge_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `freq` varchar(45) NOT NULL,
  `last_draw_time` datetime DEFAULT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `crowd_id` int(11) NOT NULL,
  `status` varchar(45) NOT NULL,
  `max_user_draw_num` int(11) NOT NULL,
  `draw_num` int(11) NOT NULL,
  `help_url` varchar(1000) DEFAULT NULL,
  `mark_icon` varchar(1000) NOT NULL,
  `material_id` int(11) NOT NULL,
  `btn_title` varchar(45) NOT NULL,
  `confirm_description` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `free_charge_activity_order`
--

DROP TABLE IF EXISTS `free_charge_activity_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `free_charge_activity_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `item_id` int(11) NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `commission_order_id` int(11) DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  `free_charge_activity_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `free_fee` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_ITEM_ID` (`item_id`),
  KEY `IDX_USER_COMMISSION_ORDER_ID` (`commission_order_id`),
  KEY `IDX_FREE_CHARGE_ACTIVITY_ID` (`free_charge_activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fund_transfer_order`
--

DROP TABLE IF EXISTS `fund_transfer_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fund_transfer_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payee_account` varchar(500) NOT NULL,
  `payee_real_name` varchar(100) NOT NULL,
  `amount` float NOT NULL,
  `payer_show_name` varchar(100) NOT NULL,
  `remark` text,
  `create_time` datetime NOT NULL,
  `pay_date` varchar(45) DEFAULT NULL,
  `alipay_order_id` varchar(45) DEFAULT NULL,
  `success` bit(1) DEFAULT NULL,
  `code` text,
  `msg` text,
  `sub_code` text,
  `sub_msg` text,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `pic_url` varchar(500) NOT NULL,
  `price` float NOT NULL,
  `origin_price` float NOT NULL,
  `shop_title` varchar(100) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `list_time` datetime DEFAULT NULL,
  `delist_time` datetime DEFAULT NULL,
  `description` text,
  `summary` text,
  `sales` int(11) DEFAULT NULL,
  `imgs` text,
  `shopcat_path` varchar(1000) DEFAULT NULL,
  `shopcat_id` int(11) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `ecom_plat` varchar(10) NOT NULL,
  `source_item_id` bigint(20) DEFAULT NULL,
  `source_cat_id` bigint(20) DEFAULT NULL,
  `item_place` varchar(45) DEFAULT NULL,
  `commission_rate` float DEFAULT NULL,
  `high_commission` bit(1) DEFAULT NULL,
  `coupon` bit(1) DEFAULT NULL,
  `coupon_click_url` varchar(5000) DEFAULT NULL,
  `coupon_start_time` datetime DEFAULT NULL,
  `coupon_end_time` datetime DEFAULT NULL,
  `coupon_info` varchar(1000) DEFAULT NULL,
  `coupon_total_count` int(11) DEFAULT NULL,
  `coupon_remain_count` int(11) DEFAULT NULL,
  `coupon_value` float DEFAULT NULL,
  `source_item_url` varchar(1000) DEFAULT NULL,
  `source_item_click_url` varchar(1000) DEFAULT NULL,
  `recommend` bit(1) DEFAULT b'0',
  `free_shipment` bit(1) DEFAULT b'0',
  `jd_sale` bit(1) DEFAULT b'0',
  `seckill` bit(1) DEFAULT b'0',
  `type` varchar(45) NOT NULL DEFAULT 'CommissionItem',
  `user_id` varchar(45) DEFAULT NULL,
  `logistics_company` varchar(500) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `total_num` int(11) DEFAULT NULL,
  `total_sale_num` int(11) DEFAULT NULL,
  `remain_num` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `day_limit_num` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `guarantee_fee` float DEFAULT NULL,
  `rebate_fee` float DEFAULT NULL,
  `total_rebate_fee` float DEFAULT NULL,
  `screenshots` text,
  `desc_imgs` text,
  `verify_remark` text,
  `white_pic_url` varchar(500) DEFAULT NULL,
  `tbk_zt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_SOURCE_ITEM_ID` (`source_item_id`),
  KEY `IDX_SHOPCAT_ID` (`shopcat_id`),
  KEY `IDX_SHOPCAT_PATH` (`shopcat_path`(255)),
  KEY `IDX_DELIST_TIME` (`delist_time`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_START_TIME` (`start_time`),
  KEY `IDX_SALES` (`sales`),
  KEY `rds_idx_0` (`type`),
  KEY `IDX_update_time` (`update_time`),
  KEY `IDX_COUPON_VALUE` (`coupon_value`),
  KEY `IDX_TBK_ZT` (`tbk_zt`)
) ENGINE=InnoDB AUTO_INCREMENT=2415603 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_order`
--

DROP TABLE IF EXISTS `item_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_title` varchar(500) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_pic_url` varchar(1000) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `source_item_id` bigint(20) DEFAULT NULL,
  `source_order_id` bigint(20) DEFAULT NULL,
  `payment` float DEFAULT NULL,
  `fanli_amount` float DEFAULT NULL,
  `remark` text,
  `status` varchar(45) NOT NULL,
  `type` varchar(20) NOT NULL,
  `ecom_platform` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_ITEM_ID` (`item_id`),
  KEY `IDX_SOURCE_ITME_ID` (`source_item_id`),
  KEY `IDX_SOURCE_ORDER_ID` (`source_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item_selection`
--

DROP TABLE IF EXISTS `item_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_selection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `source_selection_id` int(11) DEFAULT NULL,
  `ecom_plat` varchar(10) NOT NULL,
  `create_time` datetime NOT NULL,
  `last_sync_time` datetime DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `shopcat_id` int(11) DEFAULT NULL,
  `auto_fetch` bit(1) NOT NULL,
  `max_page` int(11) NOT NULL DEFAULT '0',
  `last_fetch_time` datetime DEFAULT NULL,
  `last_fetch_num` int(11) NOT NULL DEFAULT '0',
  `fetch_url` varchar(1000) DEFAULT NULL,
  `error_msg` text,
  `config` text NOT NULL,
  `type` varchar(45) NOT NULL DEFAULT 'TAOBAO_UNION_FETCH',
  `recommend` bit(1) NOT NULL DEFAULT b'0',
  `clear_channel_item` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1051 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lxk_order`
--

DROP TABLE IF EXISTS `lxk_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lxk_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `total_fee` float NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `paid_fee` float NOT NULL,
  `paid_integral` float NOT NULL,
  `pay_time` datetime DEFAULT NULL,
  `pay_type` varchar(45) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `version` int(11) NOT NULL,
  `alipay_transfer_trade_no` varchar(45) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `seller_promotion_item_id` int(11) DEFAULT NULL,
  `user_level` varchar(45) DEFAULT NULL,
  `rebate_fee` float DEFAULT NULL,
  `type` varchar(45) NOT NULL,
  `verify_remark` text,
  `seller_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_seller_promotion_item_id` (`seller_promotion_item_id`),
  KEY `IDX_ALIPAY_TRASFER_TRADE_NO` (`alipay_transfer_trade_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `create_time` varchar(45) NOT NULL,
  `locked` tinyint(1) NOT NULL,
  `admin` bit(1) NOT NULL,
  `deleted` varchar(45) NOT NULL DEFAULT '0',
  `name` varchar(45) NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `login_times` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_permission`
--

DROP TABLE IF EXISTS `manager_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `permission` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_role`
--

DROP TABLE IF EXISTS `manager_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `material`
--

DROP TABLE IF EXISTS `material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `config_json` text NOT NULL,
  `status` varchar(45) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mission`
--

DROP TABLE IF EXISTS `mission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL,
  `status` varchar(15) NOT NULL,
  `integral` int(11) NOT NULL,
  `total_times` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth`
--

DROP TABLE IF EXISTS `oauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_type` varchar(45) NOT NULL,
  `refresh_token` varchar(100) NOT NULL,
  `expiration` datetime NOT NULL,
  `access_token` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `re_expires_in` int(11) DEFAULT NULL,
  `r1expires_in` int(11) DEFAULT NULL,
  `r2expires_in` int(11) DEFAULT NULL,
  `w1expires_in` int(11) DEFAULT NULL,
  `w2expires_in` int(11) DEFAULT NULL,
  `taobao_user_id` varchar(45) DEFAULT NULL,
  `taobao_user_nick` varchar(100) DEFAULT NULL,
  `sub_taobao_user_id` varchar(45) DEFAULT NULL,
  `sub_taobao_user_nick` varchar(100) DEFAULT NULL,
  `expires_in` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `out_cat_to_shopcat`
--

DROP TABLE IF EXISTS `out_cat_to_shopcat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `out_cat_to_shopcat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `shopcat_id` int(11) NOT NULL,
  `plat` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_SHOPCAT_ID` (`shopcat_id`),
  KEY `IDX_CAT_ID` (`cat_id`),
  KEY `IDX_PLAT` (`plat`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL,
  `order_id` int(11) NOT NULL,
  `integral` float DEFAULT NULL,
  `money` float DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `equivalent_money` float DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `notify_time` datetime DEFAULT NULL,
  `notify_type` varchar(45) DEFAULT NULL,
  `notify_id` varchar(45) DEFAULT NULL,
  `app_id` varchar(45) DEFAULT NULL,
  `charset` varchar(45) DEFAULT NULL,
  `version` varchar(45) DEFAULT NULL,
  `sign_type` varchar(45) DEFAULT NULL,
  `sign` text,
  `trade_no` varchar(45) DEFAULT NULL,
  `out_trade_no` varchar(45) DEFAULT NULL,
  `out_biz_no` varchar(45) DEFAULT NULL,
  `buyer_id` varchar(45) DEFAULT NULL,
  `buyer_logon_id` varchar(45) DEFAULT NULL,
  `seller_id` varchar(45) DEFAULT NULL,
  `seller_email` varchar(45) DEFAULT NULL,
  `trade_status` varchar(45) DEFAULT NULL,
  `total_amount` float DEFAULT NULL,
  `receipt_amount` float DEFAULT NULL,
  `invoice_amount` float DEFAULT NULL,
  `buyer_pay_amount` float DEFAULT NULL,
  `point_amount` float DEFAULT NULL,
  `refund_fee` float DEFAULT NULL,
  `subject` text,
  `body` text,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_payment` datetime DEFAULT NULL,
  `gmt_refund` datetime DEFAULT NULL,
  `gmt_close` datetime DEFAULT NULL,
  `fund_bill_list` text,
  `passback_params` text,
  `voucher_detail_list` text,
  PRIMARY KEY (`id`),
  KEY `IDX_ORDER_ID` (`order_id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_TRADE_NO` (`trade_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persistent_logins`
--

DROP TABLE IF EXISTS `persistent_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pid_commission_item_share`
--

DROP TABLE IF EXISTS `pid_commission_item_share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pid_commission_item_share` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` varchar(100) NOT NULL,
  `commission_item_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `expire_time` datetime NOT NULL,
  `url` varchar(2000) NOT NULL,
  `kl` varchar(100) DEFAULT NULL,
  `short_url` varchar(100) DEFAULT NULL,
  `click_url` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`pid`),
  KEY `IDX_COMMISSION_ID` (`commission_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41118 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_packet`
--

DROP TABLE IF EXISTS `red_packet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_packet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `create_time` datetime NOT NULL,
  `total_num` int(11) NOT NULL,
  `draw_num` int(11) NOT NULL,
  `last_draw_time` datetime DEFAULT NULL,
  `type` text NOT NULL,
  `money` float NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `grant_num` int(11) NOT NULL,
  `integral` float NOT NULL,
  `max_user_grant_num` int(11) NOT NULL,
  `valid_day_num` int(11) NOT NULL,
  `freq` varchar(45) NOT NULL,
  `crowd_id` int(11) NOT NULL,
  `money_wallet_flow_detail_type` varchar(45) DEFAULT NULL,
  `integral_wallet_flow_detail_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` varchar(45) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `pinyin` varchar(45) DEFAULT NULL,
  `short_pinyin` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_LEVEL` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `name` varchar(255) NOT NULL,
  `valid` tinyint(1) DEFAULT '0',
  `update_time` datetime NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `permission` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=587 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seller_promotion_item_order`
--

DROP TABLE IF EXISTS `seller_promotion_item_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seller_promotion_item_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_promotion_item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `verify_remark` text,
  `status` varchar(45) NOT NULL,
  `out_trade_no` varchar(200) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `ecom_plat` varchar(45) NOT NULL,
  `rebate_fee` float DEFAULT NULL,
  `rebate_deadline` datetime NOT NULL,
  `seller_id` int(11) NOT NULL,
  `verify_time` datetime DEFAULT NULL,
  `verify_deadline` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_SELLER_PROMOTION_ID` (`seller_promotion_item_id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_OUT_TRADE_NO` (`out_trade_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service_item`
--

DROP TABLE IF EXISTS `service_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` varchar(200) NOT NULL,
  `create_time` datetime NOT NULL,
  `price` float NOT NULL,
  `time_unit` varchar(10) NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(45) NOT NULL,
  `content` text CHARACTER SET utf8mb4 NOT NULL,
  `type` varchar(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `expire_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_KEY` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=776 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shopcat`
--

DROP TABLE IF EXISTS `shopcat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopcat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `icon` varchar(500) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `idx` int(11) NOT NULL,
  `path` varchar(500) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `keyword` varchar(45) NOT NULL,
  `search_by_keyword` tinyint(1) NOT NULL DEFAULT '1',
  `ref_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_PATH` (`path`(255))
) ENGINE=InnoDB AUTO_INCREMENT=1248 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taobao_item_cat`
--

DROP TABLE IF EXISTS `taobao_item_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taobao_item_cat` (
  `cid` bigint(20) NOT NULL,
  `parent_cid` varchar(45) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `idx` int(11) DEFAULT NULL,
  `relation_name` text NOT NULL,
  `relation_path` text NOT NULL,
  `parent` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`cid`),
  KEY `IDX_PARENT_CID` (`parent_cid`),
  KEY `IDX_NAME` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taobao_material_cat`
--

DROP TABLE IF EXISTS `taobao_material_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taobao_material_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `create_time` datetime NOT NULL,
  `status` varchar(45) NOT NULL,
  `material_list_json` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taobao_pid`
--

DROP TABLE IF EXISTS `taobao_pid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taobao_pid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `used` bit(1) NOT NULL,
  `taobao_union_id` varchar(100) NOT NULL,
  `adzone_id` varchar(100) NOT NULL,
  `site_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_pull_new_detail`
--

DROP TABLE IF EXISTS `tb_pull_new_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pull_new_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `register_time` datetime NOT NULL,
  `bind_time` datetime DEFAULT NULL,
  `buy_time` datetime DEFAULT NULL,
  `mobile` varchar(45) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `order_tk_type` int(11) DEFAULT NULL,
  `tb_trade_parent_id` bigint(20) DEFAULT NULL,
  `adzone_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `settled` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_TB_TRADE_PARENT_ID` (`tb_trade_parent_id`),
  KEY `IDX_USER_ID` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locked` tinyint(1) NOT NULL,
  `create_time` datetime NOT NULL,
  `nickname` varchar(45) DEFAULT NULL,
  `gender` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `province` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `avatar_url` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `last_sign_time` datetime DEFAULT NULL,
  `continuous_sign_times` varchar(45) CHARACTER SET utf8 DEFAULT '0',
  `phone` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `super_user_id` int(11) DEFAULT NULL,
  `invitation_code` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `taobao_pid` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `relation_path` text CHARACTER SET utf8,
  `taobao_adzone_id` bigint(20) DEFAULT NULL,
  `weixin_open_id` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `level` varchar(45) CHARACTER SET utf8 NOT NULL DEFAULT 'ORDINARY',
  `plus_level_expire_time` datetime DEFAULT NULL,
  `relation_depth` int(11) NOT NULL DEFAULT '0',
  `district` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `level_expire_time` datetime DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `pdd_pid` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `alipay_account` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `login_times` int(11) DEFAULT NULL,
  `login_ip` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  `last_active_time` datetime DEFAULT NULL,
  `active_times` int(11) DEFAULT '0',
  `active_ip` varchar(45) DEFAULT NULL,
  `weixin_union_id` varchar(45) DEFAULT NULL,
  `level_change_time` datetime DEFAULT NULL,
  `open_id` varchar(45) DEFAULT NULL,
  `jd_position_id` int(11) DEFAULT NULL,
  `taobao_special_id` varchar(45) DEFAULT NULL,
  `taobao_relation_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  KEY `IDX_INVITATION_CODE` (`invitation_code`),
  KEY `IDX_SUPER_USER_ID` (`super_user_id`),
  KEY `IDX_TAOBAO_ADZONE_ID` (`taobao_adzone_id`),
  KEY `IDX_LEVEL_EXPIRE_TIME` (`level_expire_time`),
  KEY `IDX_RELATION_PATH` (`relation_depth`),
  KEY `IDX_RELATION_DEPTH` (`relation_depth`),
  KEY `IDX_WEIXIN_UNION_ID` (`weixin_union_id`),
  KEY `IDX_WEIXIN_OPEN_ID` (`weixin_open_id`),
  KEY `IDX_OPEN_ID` (`open_id`),
  KEY `IDX_JD_POSITION_ID` (`jd_position_id`),
  KEY `IDX_TAOBAO_RELATION_ID` (`taobao_relation_id`),
  KEY `IDX_TAOBAO_SPECIAL_ID` (`taobao_special_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1485 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_agent_application`
--

DROP TABLE IF EXISTS `user_agent_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_agent_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `apply_time` datetime NOT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_authorize`
--

DROP TABLE IF EXISTS `user_authorize`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_authorize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `authorize` varchar(100) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_AUTHORIZE` (`authorize`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_commission_order`
--

DROP TABLE IF EXISTS `user_commission_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_commission_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `commission_order_id` int(11) NOT NULL,
  `commission_rate` float DEFAULT NULL,
  `settled_fee` float DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `settled_time` datetime DEFAULT NULL,
  `allot_type` varchar(45) NOT NULL DEFAULT 'COMMON',
  `version` int(11) NOT NULL,
  `commission_reward_rate` float NOT NULL DEFAULT '0',
  `settled_commission_fee` float DEFAULT '0',
  `settled_reward_fee` float DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_COMMISSION_ORDER_ID` (`commission_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=74707 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_item_selection`
--

DROP TABLE IF EXISTS `user_item_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_item_selection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_ids` text CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `share_times` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `deleted` bit(1) NOT NULL,
  `recommend` bit(1) NOT NULL DEFAULT b'0',
  `fit_share_times` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16888 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_log`
--

DROP TABLE IF EXISTS `user_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `ip` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `total_times` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81521 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_mission`
--

DROP TABLE IF EXISTS `user_mission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_mission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mission_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `complete_times` int(11) NOT NULL,
  `complete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_red_packet`
--

DROP TABLE IF EXISTS `user_red_packet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_red_packet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `red_packet_id` int(11) NOT NULL,
  `money` float NOT NULL,
  `draw` bit(1) NOT NULL,
  `version` int(11) NOT NULL,
  `integral` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1302 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_service_subscription`
--

DROP TABLE IF EXISTS `user_service_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_service_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service` varchar(45) NOT NULL,
  `expire_time` datetime NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_withdraw`
--

DROP TABLE IF EXISTS `user_withdraw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `money` float NOT NULL,
  `create_time` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `update_time` datetime NOT NULL,
  `wallet_flow_detail_id` int(11) NOT NULL,
  `payee_account` varchar(100) NOT NULL,
  `payee_name` varchar(100) NOT NULL,
  `fund_transfer_order_id` int(11) DEFAULT NULL,
  `remark` text,
  `pay_date` varchar(45) DEFAULT NULL,
  `alipay_order_id` varchar(100) DEFAULT NULL,
  `fund_transfer_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`),
  KEY `IDX_WALLET_FLOW_DETAIL_ID` (`wallet_flow_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vfile`
--

DROP TABLE IF EXISTS `vfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vfile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `path` varchar(1000) DEFAULT NULL,
  `src` varchar(1000) DEFAULT NULL,
  `dir` bit(1) DEFAULT b'0',
  `create_time` datetime NOT NULL,
  `size` bigint(100) NOT NULL,
  `suffix` varchar(10) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_PARENT_ID` (`parent_id`),
  KEY `IDX_PATH` (`path`(255)),
  KEY `IDX_CODE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wallet`
--

DROP TABLE IF EXISTS `wallet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `integral` float NOT NULL,
  `consumed_Integral` float NOT NULL,
  `obtained_integral` float NOT NULL,
  `money` float NOT NULL,
  `consumed_money` float NOT NULL,
  `obtained_money` float NOT NULL,
  `version` int(11) NOT NULL,
  `withdraw_money` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1216 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wallet_flow_detail`
--

DROP TABLE IF EXISTS `wallet_flow_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wallet_flow_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `value_type` varchar(45) NOT NULL,
  `before_value` float NOT NULL,
  `after_value` float NOT NULL,
  `value` float NOT NULL,
  `type` varchar(45) NOT NULL,
  `context` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=192253 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `weixin_official_account`
--

DROP TABLE IF EXISTS `weixin_official_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weixin_official_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `encoding_aes_key` varchar(200) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL DEFAULT 'SUB',
  PRIMARY KEY (`id`),
  KEY `IDX_USER_ID` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-28  1:05:53
