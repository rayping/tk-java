package info.batcloud.laxiaoke.open.domain.jd.union;

import java.util.List;

public class ServicePromotionGoodsInfoResult {

    private List<ServicePromotionGoodsInfo> result;

    public List<ServicePromotionGoodsInfo> getResult() {
        return result;
    }

    public void setResult(List<ServicePromotionGoodsInfo> result) {
        this.result = result;
    }
}
