package info.batcloud.laxiaoke.open.request.jd.union;

import info.batcloud.laxiaoke.open.AbstractLxkRequest;
import info.batcloud.laxiaoke.open.response.jd.union.GoodsInfoResponse;

public class GoodsInfoRequest extends AbstractLxkRequest<GoodsInfoResponse> {

    private String skuIds;

    public String getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(String skuIds) {
        this.skuIds = skuIds;
    }

    @Override
    public String getUri() {
        return "/jd-union-api/goods-info";
    }

    @Override
    public Class<GoodsInfoResponse> getResponseClass() {
        return GoodsInfoResponse.class;
    }
}
