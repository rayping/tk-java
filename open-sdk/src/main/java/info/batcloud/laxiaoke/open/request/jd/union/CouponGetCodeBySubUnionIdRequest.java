package info.batcloud.laxiaoke.open.request.jd.union;

import info.batcloud.laxiaoke.open.AbstractLxkRequest;
import info.batcloud.laxiaoke.open.response.jd.union.CouponGetCodeBySubUnionIdResponse;

public class CouponGetCodeBySubUnionIdRequest extends AbstractLxkRequest<CouponGetCodeBySubUnionIdResponse> {

    private String couponUrl;
    private String materialIds;
    private String subUnionId;
    private long positionId;
    private String pid;

    public String getCouponUrl() {
        return couponUrl;
    }

    public void setCouponUrl(String couponUrl) {
        this.couponUrl = couponUrl;
    }

    public String getMaterialIds() {
        return materialIds;
    }

    public void setMaterialIds(String materialIds) {
        this.materialIds = materialIds;
    }

    public String getSubUnionId() {
        return subUnionId;
    }

    public void setSubUnionId(String subUnionId) {
        this.subUnionId = subUnionId;
    }

    public long getPositionId() {
        return positionId;
    }

    public void setPositionId(long positionId) {
        this.positionId = positionId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Override
    public String getUri() {
        return "/jd-union-api/coupon-get-code-by-subunionid";
    }

    @Override
    public Class<CouponGetCodeBySubUnionIdResponse> getResponseClass() {
        return CouponGetCodeBySubUnionIdResponse.class;
    }
}
