package info.batcloud.laxiaoke.open.request.pdd.ddk;

import info.batcloud.laxiaoke.open.LxkRequest;
import info.batcloud.laxiaoke.open.response.pdd.ddk.DdkGoodsDetailResponse;

public class DdkGoodsDetailRequest implements LxkRequest<DdkGoodsDetailResponse> {

    private String goodsIds;

    public String getGoodsIds() {
        return goodsIds;
    }

    public void setGoodsIds(String goodsIds) {
        this.goodsIds = goodsIds;
    }

    @Override
    public String getUri() {
        return "/ddk-api/goods-detail";
    }

    @Override
    public Class<DdkGoodsDetailResponse> getResponseClass() {
        return DdkGoodsDetailResponse.class;
    }
}
