package info.batcloud.laxiaoke.open.domain.jd.union;

import java.util.List;

public class UnionOrderResult {

    private List<UnionOrder> data;
    private int success;
    private boolean hasMore;

    public List<UnionOrder> getData() {
        return data;
    }

    public void setData(List<UnionOrder> data) {
        this.data = data;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public boolean isHasMore() {
        return hasMore;
    }

    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }
}
