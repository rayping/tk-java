package info.batcloud.laxiaoke.open.domain.jd.union;

import java.util.List;

public class CreatePromotionSiteBatchResult {

    private List<CreatePromotionSiteResult> results;

    public List<CreatePromotionSiteResult> getResults() {
        return results;
    }

    public void setResults(List<CreatePromotionSiteResult> results) {
        this.results = results;
    }
}
