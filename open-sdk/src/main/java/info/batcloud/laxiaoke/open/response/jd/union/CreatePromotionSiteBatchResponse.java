package info.batcloud.laxiaoke.open.response.jd.union;

import info.batcloud.laxiaoke.open.LxkResponse;
import info.batcloud.laxiaoke.open.domain.jd.union.CreatePromotionSiteBatchResult;
import info.batcloud.laxiaoke.open.domain.jd.union.CreatePromotionSiteResult;

import java.util.List;

public class CreatePromotionSiteBatchResponse extends LxkResponse<CreatePromotionSiteBatchResult> {
}
