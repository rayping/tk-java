package info.batcloud.laxiaoke.open.response.jd.union;

import info.batcloud.laxiaoke.open.LxkResponse;
import info.batcloud.laxiaoke.open.domain.jd.union.GoodsCategoryResult;

public class GoodsCategoryQueryResponse extends LxkResponse<GoodsCategoryResult> {
}
