package info.batcloud.laxiaoke.open.domain.jd.union;

import java.util.List;

public class UnionOrder {

    //订单完成时间
    private long finishTime;
    //下单设备
    private int orderEmt;

    private String orderId;

    private long orderTime;

    private String parentId;

    private String payMonth;

    private int plus;

    private Long popId;

    private List<Sku> skuList;

    public long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }

    public int getOrderEmt() {
        return orderEmt;
    }

    public void setOrderEmt(int orderEmt) {
        this.orderEmt = orderEmt;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(long orderTime) {
        this.orderTime = orderTime;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getPayMonth() {
        return payMonth;
    }

    public void setPayMonth(String payMonth) {
        this.payMonth = payMonth;
    }

    public int getPlus() {
        return plus;
    }

    public void setPlus(int plus) {
        this.plus = plus;
    }

    public Long getPopId() {
        return popId;
    }

    public void setPopId(Long popId) {
        this.popId = popId;
    }

    public List<Sku> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<Sku> skuList) {
        this.skuList = skuList;
    }

    public static class Sku {
        private float actualCommission;
        private float actualCosPrice;
        private float actualFee;
        private float commissionRate;
        private float estimateCommission;
        private float estimateCosPrice;
        private float estimateFee;
        private float finalRate;
        private long firstLevel;
        private int frozenSkuNum;
        private float payPrice;
        private long pid;
        private float price;
        private long secondLevel;
        private long siteId;
        private long skuId;
        private String skuName;
        private int skuNum;
        private int skuReturnNum;
        private long spId;
        private float subSideRate;
        private String subUnionId;
        private float subsidyRate;
        private long thirdLevel;
        private int traceType;
        private String unionAlias;
        private String unionTag;
        private int unionTrafficGroup;
        private int validCode;

        public float getActualCommission() {
            return actualCommission;
        }

        public void setActualCommission(float actualCommission) {
            this.actualCommission = actualCommission;
        }

        public float getActualCosPrice() {
            return actualCosPrice;
        }

        public void setActualCosPrice(float actualCosPrice) {
            this.actualCosPrice = actualCosPrice;
        }

        public float getActualFee() {
            return actualFee;
        }

        public void setActualFee(float actualFee) {
            this.actualFee = actualFee;
        }

        public float getCommissionRate() {
            return commissionRate;
        }

        public void setCommissionRate(float commissionRate) {
            this.commissionRate = commissionRate;
        }

        public float getEstimateCommission() {
            return estimateCommission;
        }

        public void setEstimateCommission(float estimateCommission) {
            this.estimateCommission = estimateCommission;
        }

        public float getEstimateCosPrice() {
            return estimateCosPrice;
        }

        public void setEstimateCosPrice(float estimateCosPrice) {
            this.estimateCosPrice = estimateCosPrice;
        }

        public float getEstimateFee() {
            return estimateFee;
        }

        public void setEstimateFee(float estimateFee) {
            this.estimateFee = estimateFee;
        }

        public float getFinalRate() {
            return finalRate;
        }

        public void setFinalRate(float finalRate) {
            this.finalRate = finalRate;
        }

        public long getFirstLevel() {
            return firstLevel;
        }

        public void setFirstLevel(long firstLevel) {
            this.firstLevel = firstLevel;
        }

        public int getFrozenSkuNum() {
            return frozenSkuNum;
        }

        public void setFrozenSkuNum(int frozenSkuNum) {
            this.frozenSkuNum = frozenSkuNum;
        }

        public float getPayPrice() {
            return payPrice;
        }

        public void setPayPrice(float payPrice) {
            this.payPrice = payPrice;
        }

        public long getPid() {
            return pid;
        }

        public void setPid(long pid) {
            this.pid = pid;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }

        public long getSecondLevel() {
            return secondLevel;
        }

        public void setSecondLevel(long secondLevel) {
            this.secondLevel = secondLevel;
        }

        public long getSiteId() {
            return siteId;
        }

        public void setSiteId(long siteId) {
            this.siteId = siteId;
        }

        public long getSkuId() {
            return skuId;
        }

        public void setSkuId(long skuId) {
            this.skuId = skuId;
        }

        public String getSkuName() {
            return skuName;
        }

        public void setSkuName(String skuName) {
            this.skuName = skuName;
        }

        public int getSkuNum() {
            return skuNum;
        }

        public void setSkuNum(int skuNum) {
            this.skuNum = skuNum;
        }

        public int getSkuReturnNum() {
            return skuReturnNum;
        }

        public void setSkuReturnNum(int skuReturnNum) {
            this.skuReturnNum = skuReturnNum;
        }

        public long getSpId() {
            return spId;
        }

        public void setSpId(long spId) {
            this.spId = spId;
        }

        public float getSubSideRate() {
            return subSideRate;
        }

        public void setSubSideRate(float subSideRate) {
            this.subSideRate = subSideRate;
        }

        public String getSubUnionId() {
            return subUnionId;
        }

        public void setSubUnionId(String subUnionId) {
            this.subUnionId = subUnionId;
        }

        public float getSubsidyRate() {
            return subsidyRate;
        }

        public void setSubsidyRate(float subsidyRate) {
            this.subsidyRate = subsidyRate;
        }

        public long getThirdLevel() {
            return thirdLevel;
        }

        public void setThirdLevel(long thirdLevel) {
            this.thirdLevel = thirdLevel;
        }

        public int getTraceType() {
            return traceType;
        }

        public void setTraceType(int traceType) {
            this.traceType = traceType;
        }

        public String getUnionAlias() {
            return unionAlias;
        }

        public void setUnionAlias(String unionAlias) {
            this.unionAlias = unionAlias;
        }

        public String getUnionTag() {
            return unionTag;
        }

        public void setUnionTag(String unionTag) {
            this.unionTag = unionTag;
        }

        public int getUnionTrafficGroup() {
            return unionTrafficGroup;
        }

        public void setUnionTrafficGroup(int unionTrafficGroup) {
            this.unionTrafficGroup = unionTrafficGroup;
        }

        public int getValidCode() {
            return validCode;
        }

        public void setValidCode(int validCode) {
            this.validCode = validCode;
        }
    }
}
