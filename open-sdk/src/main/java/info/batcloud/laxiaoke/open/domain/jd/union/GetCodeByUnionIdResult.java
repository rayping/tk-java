package info.batcloud.laxiaoke.open.domain.jd.union;

import java.util.Map;

public class GetCodeByUnionIdResult {

    private String resultCode;
    private String resultMessage;

    private Map<String, String> urlList;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public Map<String, String> getUrlList() {
        return urlList;
    }

    public void setUrlList(Map<String, String> urlList) {
        this.urlList = urlList;
    }
}
