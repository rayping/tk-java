package info.batcloud.laxiaoke.open.request.jd.union;

import info.batcloud.laxiaoke.open.AbstractLxkRequest;
import info.batcloud.laxiaoke.open.response.jd.union.WxsqGetCodeBySubUnionIdResponse;

public class WxsqGetCodeBySubUnionIdRequest extends AbstractLxkRequest<WxsqGetCodeBySubUnionIdResponse> {

    private int proCont;
    private String materialIds;
    private String subUnionId;
    private long positionId;
    private String pid;

    public int getProCont() {
        return proCont;
    }

    public void setProCont(int proCont) {
        this.proCont = proCont;
    }

    public String getMaterialIds() {
        return materialIds;
    }

    public void setMaterialIds(String materialIds) {
        this.materialIds = materialIds;
    }

    public String getSubUnionId() {
        return subUnionId;
    }

    public void setSubUnionId(String subUnionId) {
        this.subUnionId = subUnionId;
    }

    public long getPositionId() {
        return positionId;
    }

    public void setPositionId(long positionId) {
        this.positionId = positionId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Override
    public String getUri() {
        return "/jd-union-api/query-order";
    }

    @Override
    public Class<WxsqGetCodeBySubUnionIdResponse> getResponseClass() {
        return WxsqGetCodeBySubUnionIdResponse.class;
    }
}
