package info.batcloud.laxiaoke.open.request.pdd.ddk;

import info.batcloud.laxiaoke.open.LxkRequest;
import info.batcloud.laxiaoke.open.response.pdd.ddk.DdkOauthGoodsPromUrlGenerateResponse;

public class DdkOauthGoodsPromUrlGenerateRequest implements LxkRequest<DdkOauthGoodsPromUrlGenerateResponse> {

    private String goodsIds;

    private boolean generateShortUrl;

    private String customParameters;

    public String getGoodsIds() {
        return goodsIds;
    }

    public void setGoodsIds(String goodsIds) {
        this.goodsIds = goodsIds;
    }

    public boolean isGenerateShortUrl() {
        return generateShortUrl;
    }

    public void setGenerateShortUrl(boolean generateShortUrl) {
        this.generateShortUrl = generateShortUrl;
    }

    public String getCustomParameters() {
        return customParameters;
    }

    public void setCustomParameters(String customParameters) {
        this.customParameters = customParameters;
    }

    @Override
    public String getUri() {
        return "/ddk-api/oauth-goods-prom-url-generate";
    }

    @Override
    public Class<DdkOauthGoodsPromUrlGenerateResponse> getResponseClass() {
        return DdkOauthGoodsPromUrlGenerateResponse.class;
    }
}
