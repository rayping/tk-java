package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.CityAgentApplyService;
import info.batcloud.fanli.core.service.DistrictAgentApplyService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/agent-apply")
@PreAuthorize("hasRole('USER')")
public class AgentApplyController {

    @Inject
    private CityAgentApplyService cityAgentApplyService;

    @Inject
    private DistrictAgentApplyService districtAgentApplyService;

    @PostMapping("/apply-city-agent")
    public Object cityAgentApply(CityAgentApplyService.ApplyParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        cityAgentApplyService.apply(param);
        return BusinessResponse.ok(true);
    }

    @PostMapping("/apply-district-agent")
    public Object districtAgentApply(DistrictAgentApplyService.ApplyParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        districtAgentApplyService.apply(param);
        return BusinessResponse.ok(true);
    }
}
