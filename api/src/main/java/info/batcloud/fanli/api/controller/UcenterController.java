package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.domain.DefaultUserDetails;
import info.batcloud.fanli.core.helper.SecurityHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/ucenter")
public class UcenterController {

    @GetMapping("/data")
    public Object data() {
        Map<String, Object> model = new HashMap<>();
        DefaultUserDetails loginUser = SecurityHelper.loginUser();
        model.put("loginUser", loginUser);
        return BusinessResponse.ok(model);
    }

}
