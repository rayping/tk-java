package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.api.security.annotation.UserLevelRequire;
import info.batcloud.fanli.core.enums.UserLevel;
import info.batcloud.fanli.core.service.TaobaoNewerService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/taobao-newer")
public class TaobaoNewerController {

    @Inject
    private TaobaoNewerService taobaoNewerService;

    @GetMapping("/share-pic")
    @PreAuthorize("hasRole('USER')")
    @UserLevelRequire(UserLevel.PLUS)
    public Object sharePic(@RequestParam String url, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpg");
        response.setHeader("Content-Type", "image/jpg");
        taobaoNewerService.genSharePic(url, response.getOutputStream());
        response.getOutputStream().close();
        response.flushBuffer();
        return null;
    }

}
