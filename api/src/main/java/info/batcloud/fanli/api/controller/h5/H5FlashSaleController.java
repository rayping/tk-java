package info.batcloud.fanli.api.controller.h5;

import info.batcloud.fanli.api.controller.PageFlashSaleController;
import info.batcloud.fanli.core.dto.FlashSaleItemDTO;
import info.batcloud.fanli.core.enums.FlashSaleItemSort;
import info.batcloud.fanli.core.enums.FlashSaleItemStatus;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.repository.FlashSaleItemRepository;
import info.batcloud.fanli.core.service.FlashSaleItemService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.service.UrlMappingService;
import info.batcloud.fanli.core.settings.FlashSaleSetting;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.text.ParseException;
import java.util.*;

@Controller
@RequestMapping("/h5/flash-sale")
public class H5FlashSaleController {

    @Inject
    private FlashSaleItemService flashSaleItemService;

    @Inject
    private FlashSaleItemRepository flashSaleItemRepository;

    @Inject
    private UrlMappingService urlMappingService;

    @Inject
    private SystemSettingService systemSettingService;

    @GetMapping("/share")
    public Object share(ModelMap map) throws ParseException {
        FlashSaleSetting saleSetting = systemSettingService.findActiveSetting(FlashSaleSetting.class);
        Date now = new Date();
        String date = DateFormatUtils.format(now, "yyyy-MM-dd");
        map.put("date", date);
        List<PageFlashSaleController.SeasonVo> list = new ArrayList<>();
        Collections.sort(saleSetting.getSeasonList(), Comparator.comparing(FlashSaleSetting.Season::getTime));
        PageFlashSaleController.SeasonVo activeSeason = null;
        for (int i = 0; i < saleSetting.getSeasonList().size(); i++) {
            FlashSaleSetting.Season season = saleSetting.getSeasonList().get(i);
            PageFlashSaleController.SeasonVo sv = new PageFlashSaleController.SeasonVo();
            sv.setTime(season.getTime());
            sv.setTitle(season.getTitle());
            String startTimeStr = date + " " + sv.getTime();
            sv.setDate(date);
            try {
                Date startTime = DateUtils.parseDate(startTimeStr.replaceAll(" +", " "), "yyyy-MM-dd HH:mm");
                sv.setRemainSeconds((startTime.getTime() - now.getTime()) / 1000);
            } catch (ParseException e) {
                e.printStackTrace();
                continue;
            }
            PageFlashSaleController.SeasonVo lastSv;
            if (i == 0) {
                lastSv = sv;
            } else {
                lastSv = list.get(list.size() - 1);
            }
            if (sv.getRemainSeconds() > 0 && lastSv.getRemainSeconds() <= 0 && activeSeason == null) {
                lastSv.setActive(true);
                activeSeason = lastSv;
            }
            list.add(sv);
        }
        if(activeSeason == null && list.size() > 0) {
            list.get(0).setActive(true);
        }
        map.put("seasonList", list);
        map.put("flashSaleSetting", saleSetting);
        FlashSaleItemService.SearchParam param = new FlashSaleItemService.SearchParam();
        param.setFinished(true);
        param.setStatus(FlashSaleItemStatus.ONSALE);
        param.setSort(FlashSaleItemSort.ID_DESC);
        param.setPageSize(10);
        List<FlashSaleItemDTO> finishedList = flashSaleItemService.search(param).getResults();
        map.put("finishedList", finishedList);
        param.setFinished(false);
        List<FlashSaleItemDTO> flashSaleItemList = flashSaleItemService.search(param).getResults();
        map.put("flashSaleItemList", flashSaleItemList);
        return "/h5/flash-sale/share";
    }

}
