package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.UserCommissionOrderService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController()
@RequestMapping("/user-commission-order")
@PreAuthorize("hasRole('USER')")
public class UserCommissionOrderController {

    @Inject
    private UserCommissionOrderService UserCommissionOrderService;

    @GetMapping("/search")
    public Object search(UserCommissionOrderService.SearchParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        return BusinessResponse.ok(UserCommissionOrderService.search(param));
    }

}
