package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.service.CommissionItemContentLoader;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/item")
public class ItemController {

    @Inject
    private CommissionItemContentLoader commissionItemContentLoader;

    @GetMapping("/desc-img-list/{id}")
    @ResponseBody
    public Object content(@PathVariable long id) {
        List<String> imgs = commissionItemContentLoader.loadDescImgList(id);
        List<String> imgList = new ArrayList<>();
        for (String img : imgs) {
            imgList.add(img.startsWith("//") ? "http:" + img : img);
        }
        return BusinessResponse.ok(imgList);
    }

}
