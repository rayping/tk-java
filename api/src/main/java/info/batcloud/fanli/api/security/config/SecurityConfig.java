package info.batcloud.fanli.api.security.config;

import info.batcloud.fanli.api.security.filter.PasswordAuthenticationJsonFilter;
import info.batcloud.fanli.api.security.filter.PhoneLoginAuthenticationProcessingFilter;
import info.batcloud.fanli.api.security.handler.JsonLogoutHandler;
import info.batcloud.fanli.api.security.wexin.authentication.WeixinAuthenticationAuthenticationProcessingFilter;
import info.batcloud.fanli.api.security.wexin.authentication.WxaAuthenticationAuthenticationProcessingFilter;
import info.batcloud.fanli.core.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.inject.Inject;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableOAuth2Client
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Inject
    private WxaAuthenticationAuthenticationProcessingFilter wxaAuthenticationAuthenticationProcessingFilter;

    @Inject
    private PasswordAuthenticationJsonFilter passwordAuthenticationJsonFilter;

    @Inject
    private PhoneLoginAuthenticationProcessingFilter phoneLoginAuthenticationProcessingFilter;

    @Inject
    private WeixinAuthenticationAuthenticationProcessingFilter weixinAuthenticationAuthenticationProcessingFilter;

    @Inject
    private UserService userService;

    @Inject
    private JsonLogoutHandler logoutHandler;

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.authenticationProvider(authenticationProvider());
        auth.authenticationProvider(rememberMeAuthenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        passwordAuthenticationJsonFilter.setRememberMeServices(rememberMeService());
        passwordAuthenticationJsonFilter.setAuthenticationManager(this.authenticationManager());
        phoneLoginAuthenticationProcessingFilter.setRememberMeServices(rememberMeService());
        phoneLoginAuthenticationProcessingFilter.setAuthenticationManager(this.authenticationManager());
        weixinAuthenticationAuthenticationProcessingFilter.setAuthenticationManager(this.authenticationManager());
        weixinAuthenticationAuthenticationProcessingFilter.setRememberMeServices(rememberMeService());
        http.cors();
        http.addFilterBefore(rememberMeAuthenticationFilter(), BasicAuthenticationFilter.class);
        http.addFilterBefore(weixinAuthenticationAuthenticationProcessingFilter, BasicAuthenticationFilter.class);
        http.addFilterBefore(phoneLoginAuthenticationProcessingFilter, BasicAuthenticationFilter.class);
        http.addFilter(passwordAuthenticationJsonFilter);
        http.logout().invalidateHttpSession(true).addLogoutHandler(logoutHandler);
        http.rememberMe().tokenRepository(persistentTokenRepository())
                .tokenValiditySeconds(Integer.MAX_VALUE)
                .userDetailsService(userService);
        http.csrf().disable();

    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userService);
        authenticationProvider.setPasswordEncoder(passwordEncoder);
        authenticationProvider.setHideUserNotFoundExceptions(false);
        return authenticationProvider;
    }

    @Bean
    public RememberMeServices rememberMeService() {
        TokenBasedRememberMeServices services = new TokenBasedRememberMeServices("laxiaoke", userService);
        return services;
    }

    @Bean
    public RememberMeAuthenticationFilter rememberMeAuthenticationFilter() throws Exception {
        RememberMeAuthenticationFilter filter = new RememberMeAuthenticationFilter(this.authenticationManager(), rememberMeService());

        return filter;
    }

    @Bean
    public RememberMeAuthenticationProvider rememberMeAuthenticationProvider() {
        RememberMeAuthenticationProvider provider = new RememberMeAuthenticationProvider("laxiaoke");
        return provider;
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl persistentTokenRepository = new JdbcTokenRepositoryImpl();
        persistentTokenRepository.setDataSource(dataSource);
        return persistentTokenRepository;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().regexMatchers("^/(css|images|js)/.+$").regexMatchers("^.+\\.html(\\?.*)??$");
    }
}
