package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.FlashSaleOrderService;
import info.batcloud.fanli.core.service.FreeChargeActivityOrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/flash-sale-order")
public class FlashSaleOrderController {

    @Inject
    private FlashSaleOrderService flashSaleOrderService;

    @GetMapping("/search")
    public Object search(FlashSaleOrderService.SearchParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        return BusinessResponse.ok(flashSaleOrderService.search(param));
    }


}
