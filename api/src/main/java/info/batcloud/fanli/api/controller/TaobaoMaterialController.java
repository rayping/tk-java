package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.service.TaobaoMaterialCatService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/taobao-material")
public class TaobaoMaterialController {

    @Inject
    private TaobaoMaterialCatService taobaoMaterialCatService;

    @GetMapping("/list")
    public Object list(@RequestParam long catId) {
        return BusinessResponse.ok(taobaoMaterialCatService.getMaterialList(catId));
    }

    @GetMapping("/cat/list")
    public Object catList() {
        return BusinessResponse.ok(taobaoMaterialCatService.findAll());
    }

}
