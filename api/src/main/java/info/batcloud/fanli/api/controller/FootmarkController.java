package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.FootmarkType;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.FootmarkService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/footmark")
@PreAuthorize("hasRole('USER')")
public class FootmarkController {

    @Inject
    private FootmarkService footmarkService;

    @GetMapping("/search")
    public Object search(FootmarkService.SearchParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        return BusinessResponse.ok(footmarkService.search(param));
    }

    @PostMapping("/toggle-favor/item/{id}")
    public Object toggleFavor(@PathVariable long id) {
        if(SecurityHelper.loginUserId() != null) {
            return BusinessResponse.ok(footmarkService.toggleFavor(SecurityHelper.loginUserId(), id));
        }
        return BusinessResponse.ok(false);
    }

    @PostMapping("/favor/item/{id}")
    public Object favor(@PathVariable long id) {
        footmarkService.addFootmark(SecurityHelper.loginUserId(), id, FootmarkType.FAVOR);
        return BusinessResponse.ok(true);
    }

    @DeleteMapping("/item/{id}")
    public Object delete(@PathVariable long id) {
        footmarkService.deleteByUserIdAndItemId(SecurityHelper.loginUserId(), id);
        return BusinessResponse.ok(true);
    }

    @DeleteMapping("/clear")
    public Object clear() {
        footmarkService.clearByUserId(SecurityHelper.loginUserId());
        return BusinessResponse.ok(true);
    }

}
