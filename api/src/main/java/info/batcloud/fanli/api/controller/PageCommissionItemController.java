package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.api.vo.CommissionItemVo;
import info.batcloud.fanli.core.dto.CommissionItemDTO;
import info.batcloud.fanli.core.dto.FreeChargeActivityDTO;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.FootmarkType;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.*;

@RestController
@RequestMapping("/page/commissionItem")
public class PageCommissionItemController {

    @Inject
    private CommissionItemService commissionItemService;

    @Inject
    private FootmarkService footmarkService;

    @Inject
    private FreeChargeActivityService freeChargeActivityService;

    @Inject
    private UrlMappingService urlMappingService;

    @Inject
    private CommissionService commissionService;

    @GetMapping("/data")
    public Object data(@RequestParam Long id,
                       @RequestParam(required = false) EcomPlat ecomPlat,
                       @RequestParam(required = false) Long sourceItemId,
                       @RequestParam(required = false) Long freeChargeActivityId) {
        CommissionItemDTO item;
        CommissionItemVo ci = new CommissionItemVo();
        if (id == null || id == 0) {
            item = commissionItemService.findOrFetchBySourceItemIdAndEcomPlat(sourceItemId, ecomPlat);
            id = item.getId();
        } else {
            item = commissionItemService.findById(id);
        }

        ci.setTitle(item.getTitle());
        ci.setItemImgs(new HashSet<>());
        if(item.getImgList() == null || item.getImgList().size() == 0) {
            ci.getItemImgs().add(item.getPicUrl());
        } else {
            ci.getItemImgs().addAll(item.getImgList());
        }
        ci.setOriginPrice(item.getOriginPrice());
        ci.setPrice(item.getPrice());
        ci.setEcomPlat(item.getEcomPlat());
        ci.setCouponValue(item.getCouponValue());
        ci.setFavor(false);
        ci.setSales(item.getSales());
        ci.setDescription(item.getDescription());
        ci.setShopcatId(item.getShopcatId());
        ci.setShareCommission(item.getShareCommission());
        ci.setFreeShipment(item.isFreeShipment());
        ci.setId(id);
        ci.setRewardShareCommission(commissionService.determineRewardCommissionFee(ci.getShareCommission()));
        ci.setCoupon(item.isCoupon());
        ci.setTbkZt(item.isTbkZt());
        Long userId = SecurityHelper.loginUserId();
        boolean favor = false;
        if (userId == null) {

        } else {
            if (id != null) {
                footmarkService.addFootmark(userId, id, FootmarkType.HISTORY);
            }
            favor = footmarkService.isFavor(userId, id);
        }
        ci.setShopTitle(item.getShopTitle());
        Map<String, Object> model = new HashMap<>();
        model.put("commissionItem", ci);
        model.put("favor", favor);
        if(freeChargeActivityId != null) {
            FreeChargeActivityDTO activity = freeChargeActivityService.findValidById(freeChargeActivityId);
            model.put("freeChargeActivityName", activity.getName());
            model.put("freeChargeActivityMarkIcon", activity.getMarkIconUrl());
            model.put("freeChargeActivityBtnTitle", activity.getBtnTitle());
            model.put("freeChargeActivityHelpUrl", activity.getHelpUrl());
        }

        if (id != null) {
            model.put("descUrl", urlMappingService.getCommissionItemDescUrl(id));
        }
        return BusinessResponse.ok(model);
    }

}
