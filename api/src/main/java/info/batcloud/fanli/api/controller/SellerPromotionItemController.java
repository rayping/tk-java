package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.api.security.annotation.UserLevelRequire;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.SellerPromotionItemStatus;
import info.batcloud.fanli.core.enums.UserLevel;
import info.batcloud.fanli.core.exception.BizException;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.SellerPromotionItemOrderService;
import info.batcloud.fanli.core.service.SellerPromotionItemService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/seller-promotion-item")
public class SellerPromotionItemController {

    @Inject
    private SellerPromotionItemService sellerPromotionItemService;

    @Inject
    private SellerPromotionItemOrderService sellerPromotionItemOrderService;

    @GetMapping("/search")
    public Object search(SellerPromotionItemService.SearchParam param) {
        param.setStatus(SellerPromotionItemStatus.ONSALE);
        return BusinessResponse.ok(sellerPromotionItemService.search(param));
    }

    @UserLevelRequire(UserLevel.PLUS)
    @PostMapping("/rush-buy")
    public Object rushBuy(SellerPromotionItemOrderService.RushOrderCreateParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        SellerPromotionItemOrderService.RushOrderCreateResult result = sellerPromotionItemOrderService.rushCreateOrder(param);
        if(!result.isSuccess()) {
            throw new BizException(result.getErrMsg());
        }
        return BusinessResponse.ok(true);
    }
}
