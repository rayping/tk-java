package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.TbPullNewDetailService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@PreAuthorize("hasRole('USER')")
@RequestMapping("/tb-pull-new-detail")
public class TbPullNewDetailController {

    @Inject
    private TbPullNewDetailService tbPullNewDetailService;

    @GetMapping("/search")
    public Object search(TbPullNewDetailService.SearchParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        return BusinessResponse.ok(tbPullNewDetailService.search(param));
    }

}
