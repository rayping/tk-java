package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.SearchFrom;
import info.batcloud.fanli.core.enums.UserLevel;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.AdvSpaceService;
import info.batcloud.fanli.core.service.ChannelService;
import info.batcloud.fanli.core.service.ShopcatService;
import info.batcloud.fanli.core.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/commissionItemSearch")
public class PageCommissionItemSearchController {

    @Inject
    private ChannelService channelService;

    @Inject
    private ShopcatService shopcatService;

    @Inject
    private UserService userService;

    @GetMapping("/data")
    public Object data(Param param) {
        Map<String, Object> model = new HashMap<>();
        Long userId = SecurityHelper.loginUserId();
        if(userId != null) {
            model.put("isPlus", userService.isUserLevel(userId, UserLevel.PLUS));
        }
        model.put("searchFromTitle", (param.getSearchFrom() == null || param.getSearchFrom() == SearchFrom.SITE) ? "" : param.getSearchFrom().getTitle());
        return BusinessResponse.ok(model);
    }

    public static class Param {
        private SearchFrom searchFrom;
        private Long advSpaceId;

        public Long getAdvSpaceId() {
            return advSpaceId;
        }

        public void setAdvSpaceId(Long advSpaceId) {
            this.advSpaceId = advSpaceId;
        }

        public SearchFrom getSearchFrom() {
            return searchFrom;
        }

        public void setSearchFrom(SearchFrom searchFrom) {
            this.searchFrom = searchFrom;
        }
    }

}
