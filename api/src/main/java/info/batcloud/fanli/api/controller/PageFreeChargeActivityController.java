package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.dto.FreeChargeActivityDTO;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.service.FreeChargeActivityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/free-charge-activity")
public class PageFreeChargeActivityController {

    @Inject
    private FreeChargeActivityService freeChargeActivityService;

    @GetMapping("/data")
    public Object data(DataForm form) {
        Map<String, Object> model = new HashMap<>();
        FreeChargeActivityDTO freeChargeActivity = freeChargeActivityService.findById(form.getId());
        model.put("freeChargeActivityName", freeChargeActivity.getName());
        model.put("helpUrl", freeChargeActivity.getHelpUrl());
        model.put("markIcon", freeChargeActivity.getMarkIconUrl());
        model.put("materialConfig", freeChargeActivity.getMaterialConfig());
        return BusinessResponse.ok(model);
    }

    public static class DataForm {
        private Long id;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

    }

}
