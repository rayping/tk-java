package info.batcloud.fanli.api.controller.h5;

import info.batcloud.fanli.core.dto.CommissionItemDTO;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.service.CommissionItemContentLoader;
import info.batcloud.fanli.core.service.CommissionItemService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/h5/commission-item")
public class H5CommissionItemController {

    @Inject
    private CommissionItemService commissionItemService;

    @Inject
    private CommissionItemContentLoader commissionItemContentLoader;

    @GetMapping("/share/{id}-{userId}")
    public String share(@PathVariable long id, @PathVariable long userId, ModelMap model) {
        CommissionItemService.ShareInfo shareInfo = commissionItemService.findShareInfo(id, userId);
        model.put("shareInfo", shareInfo);
        CommissionItemDTO item = commissionItemService.findById(id);
        if(item.getEcomPlat() == EcomPlat.JD
                || item.getEcomPlat() == EcomPlat.PDD) {
            return "redirect:" + shareInfo.getClickUrl();
        }
        model.put("item", item);
//        String userAgent = request.getHeader("user-agent");
//        if(userAgent.indexOf("MicroMessenger") == -1
//                && StringUtils.isNotBlank(shareInfo.getClickUrl())) {
//            //说明可能是在浏览器中打开的。
//            return "redirect:" + shareInfo.getClickUrl();
//        }
        return "h5/commission-item/share";
    }

    @GetMapping("/desc/{id}")
    public String desc(@PathVariable long id, ModelMap modelMap) {
        List<String> list = commissionItemContentLoader.loadDescImgList(id);
        return "h5/commission-item/desc";
    }
}
