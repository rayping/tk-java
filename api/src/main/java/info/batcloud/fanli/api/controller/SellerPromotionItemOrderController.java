package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.SellerPromotionItemOrderService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/seller-promotion-item-order")
public class SellerPromotionItemOrderController {

    @Inject
    private SellerPromotionItemOrderService sellerPromotionItemOrderService;

    @PutMapping("/fill-trade-no/{id}")
    public Object fillTradeNo(@PathVariable long id, @RequestParam String tradeNo) {
        sellerPromotionItemOrderService.fillTradeNo(SecurityHelper.loginUserId(), id, tradeNo);
        return BusinessResponse.ok(true);
    }

    @GetMapping("/search")
    public Object search(SellerPromotionItemOrderService.SearchParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        return BusinessResponse.ok(sellerPromotionItemOrderService.search(param));
    }

}
