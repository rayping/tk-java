package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.WalletFlowDetailService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/wallet-flow-detail")
@PreAuthorize("hasRole('USER')")
public class WalletFlowDetailController {

    @Inject
    private WalletFlowDetailService walletFlowDetailService;

    @GetMapping("/search")
    public Object search(WalletFlowDetailService.SearchParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        return BusinessResponse.ok(walletFlowDetailService.search(param));
    }

}
