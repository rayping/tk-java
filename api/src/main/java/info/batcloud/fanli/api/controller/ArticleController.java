package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.ArticleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/article")
public class ArticleController {

    @Inject
    private ArticleService articleService;

    @GetMapping("/search")
    public Object search(ArticleService.SearchParam param) {
        return BusinessResponse.ok(articleService.search(param));
    }

    @GetMapping("/share/{id}")
    @PreAuthorize("hasRole('USER')")
    public Object share(@PathVariable long id) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", SecurityHelper.loginUserId());
        articleService.addShareTimes(id, 1);
        return BusinessResponse.ok(map);
    }
}
