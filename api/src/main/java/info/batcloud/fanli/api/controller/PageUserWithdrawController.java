package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.dto.UserWithdrawDTO;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.service.FundTransferOrderService;
import info.batcloud.fanli.core.service.UserWithdrawService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/user-withdraw")
@PreAuthorize("hasRole('USER')")
public class PageUserWithdrawController {

    @Inject
    private UserWithdrawService userWithdrawService;

    @Inject
    private FundTransferOrderService fundTransferOrderService;

    @GetMapping("/data/{id}")
    public Object data(@PathVariable long id) {

        Map<String, Object> map = new HashMap<>();
        UserWithdrawDTO userWithdraw = userWithdrawService.findById(id);
        map.put("userWithdraw", userWithdraw);
        return BusinessResponse.ok(map);
    }

}
