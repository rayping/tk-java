package info.batcloud.fanli.api.vo;

public class CommissionItemShareSvgVo {

    private String title;
    private Float price;
    private Float originPrice;
    private String econPlatName;
    private Float couponValue;
    private String url;
    private String picUrl;

    public String getEconPlatName() {
        return econPlatName;
    }

    public void setEconPlatName(String econPlatName) {
        this.econPlatName = econPlatName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(Float originPrice) {
        this.originPrice = originPrice;
    }

    public Float getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(Float couponValue) {
        this.couponValue = couponValue;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
}
