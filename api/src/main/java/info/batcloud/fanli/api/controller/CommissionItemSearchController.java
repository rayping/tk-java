package info.batcloud.fanli.api.controller;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.dto.CommissionItemSearchDTO;
import info.batcloud.fanli.core.service.CommissionItemSearchService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/search/commission-item")
public class CommissionItemSearchController {

    @Inject
    private CommissionItemSearchService commissionItemSearchService;

    @GetMapping
    public Object execute(Param param) {
//        param.setTbkZt(true);
        Paging<CommissionItemSearchDTO> paging = commissionItemSearchService.search(param);
        return BusinessResponse.ok(paging);
    }

    @GetMapping("/same-style/{id}")
    public Object sameStyle(@PathVariable long id) {
        return BusinessResponse.ok(commissionItemSearchService.findSameStyleById(id, 20));
    }

    public static class Param extends CommissionItemSearchService.SearchParam {

    }

}
