package info.batcloud.fanli.api.vo;

import info.batcloud.fanli.core.enums.EcomPlat;

import java.util.Set;

public class CommissionItemVo {

    private long id;
    private String title;
    private String picUrl;
    private Set<String> itemImgs;
    private Float originPrice;
    private Float price;
    private Float couponValue;
    private EcomPlat ecomPlat;
    private boolean favor;
    private int sales;
    private Long shopcatId;
    private float shareCommission;
    private float rewardShareCommission;
    private String shopTitle;
    private boolean freeShipment;
    private boolean coupon;
    private String content;
    private String description;
    private boolean tbkZt;

    public boolean isTbkZt() {
        return tbkZt;
    }

    public void setTbkZt(boolean tbkZt) {
        this.tbkZt = tbkZt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCoupon() {
        return coupon;
    }

    public void setCoupon(boolean coupon) {
        this.coupon = coupon;
    }

    public boolean isFreeShipment() {
        return freeShipment;
    }

    public void setFreeShipment(boolean freeShipment) {
        this.freeShipment = freeShipment;
    }

    public float getRewardShareCommission() {
        return rewardShareCommission;
    }

    public void setRewardShareCommission(float rewardShareCommission) {
        this.rewardShareCommission = rewardShareCommission;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public float getShareCommission() {
        return shareCommission;
    }

    public void setShareCommission(float shareCommission) {
        this.shareCommission = shareCommission;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Set<String> getItemImgs() {
        return itemImgs;
    }

    public void setItemImgs(Set<String> itemImgs) {
        this.itemImgs = itemImgs;
    }

    public Float getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(Float originPrice) {
        this.originPrice = originPrice;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(Float couponValue) {
        this.couponValue = couponValue;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public boolean isFavor() {
        return favor;
    }

    public void setFavor(boolean favor) {
        this.favor = favor;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public Long getShopcatId() {
        return shopcatId;
    }

    public void setShopcatId(Long shopcatId) {
        this.shopcatId = shopcatId;
    }
}
