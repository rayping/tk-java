package info.batcloud.fanli.api.controller;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.dto.UserRedPacketDTO;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.RedPacketService;
import info.batcloud.fanli.core.service.UserRedPacketService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.ArrayList;

@RestController
@RequestMapping("/user-red-packet")
public class UserRedPacketController {

    @Inject
    private RedPacketService redPacketService;

    @Inject
    private UserRedPacketService userRedPacketService;

    @GetMapping("/grant-list")
    public Object listByCurrentUser() {
        Long userId = SecurityHelper.loginUserId();
        if(userId == null) {
            return BusinessResponse.ok(new ArrayList());
        }
        redPacketService.grantToUser(userId);
        UserRedPacketService.SearchParam param = new UserRedPacketService.SearchParam();
        param.setUserId(userId);
        param.setDraw(false);
        Paging<UserRedPacketDTO> rs = userRedPacketService.search(param);
        return BusinessResponse.ok(rs);
    }

    @PutMapping("/draw/{id}")
    public Object draw(@PathVariable long id) {
        userRedPacketService.drawByIdAndUserId(id, SecurityHelper.loginUserId());
        return BusinessResponse.ok(true);
    }
}
