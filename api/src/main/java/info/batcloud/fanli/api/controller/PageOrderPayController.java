package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.dto.OrderForPay;
import info.batcloud.fanli.core.service.OrderService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/order-pay")
@PreAuthorize("hasRole('USER')")
public class PageOrderPayController {

    @Inject
    private OrderService orderService;

    @GetMapping("/data/{orderId}")
    public Object data(@PathVariable long orderId) {
        Map<String, Object> model = new HashMap<>();
        OrderForPay orderForPay = orderService.findPayInfo(orderId);
        model.put("content", orderForPay.getContent());
        model.put("id", orderId);
        model.put("totalFee", orderForPay.getTotalFee());
        model.put("statusTitle", orderForPay.getStatusTitle());
        model.put("canPay", orderForPay.isCanPay());
        return BusinessResponse.ok(model);
    }

}
