package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.api.controller.vo.EnumVo;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.SearchFrom;
import info.batcloud.fanli.core.service.HotSearchService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/page/search")
public class PageSearchController {

    @Inject
    private HotSearchService hotSearchService;

    @GetMapping("/data")
    public Object data() {
        Map<String, Object> map = new HashMap<>();
        map.put("hotKeywords", hotSearchService.findHotKeywords());
        List<EnumVo> list = new ArrayList<>();
        for (SearchFrom searchFrom : SearchFrom.values()) {
            list.add(new EnumVo(searchFrom.getTitle(), searchFrom.name(), searchFrom.getHelp()));
        }
        map.put("searchFromList", list);
        return BusinessResponse.ok(map);
    }

}
