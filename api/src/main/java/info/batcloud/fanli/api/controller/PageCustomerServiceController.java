package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.CustomerServiceSetting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/customer-service")
public class PageCustomerServiceController {

    @Inject
    private SystemSettingService systemSettingService;

    @GetMapping("/data")
    public Object data() {
        Map<String, Object> map = new HashMap<>();
        map.put("customerServiceSetting", systemSettingService.findActiveSetting(CustomerServiceSetting.class));
        return BusinessResponse.ok(map);
    }

}
