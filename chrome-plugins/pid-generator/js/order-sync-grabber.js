var PAY_TYPE = {
    '': '全部订单',
    '3': '订单结算'
}
var QUERY_TYPE = {
    '1': '创建时间',
    '3': '计算时间'
}
function OrderGrabber() {
    this.contextMap = {}
}
OrderGrabber.prototype = {
    init() {
      this.contextMap = JSON.parse(localStorage.getItem('_ORDER_GRABBER_CONTEXT_')) || {}
        return this
    },
    addContext(context) {
        context.id = new Date().getTime() + ''
        this.contextMap[context.id] = context
        this._saveContext()
        return context
    },
    getContextMap() {
      return this.contextMap
    },
    getPayStatus(payStatus) {
        return PAY_TYPE[payStatus]
    },
    getQueryType(queryType) {
        return QUERY_TYPE[queryType]
    },
    removeContext(id) {
        window.clearInterval(this.contextMap[id]._windowInterval)
        delete this.contextMap[id]
    },
    run() {
        for (var key in this.contextMap) {
            if(this.contextMap[key].status != 'STOP') {
                this.startContext(this.contextMap[key])
            }
        }
        return this
    },
    startContext(context) {
        var self = this;
        if(context._windowInterval) {
            window.clearInterval(context._windowInterval)
        }
        function doRun() {
            var flag = self.runContext(context)
            self._saveContext()
            if(!flag) {
                window.clearInterval(context._windowInterval)
            }
        }
        doRun()
        context._windowInterval = setInterval(function() {
            doRun()
        }, context.interval * 1000 * 60)
    },
    stopContext(id) {
        window.clearInterval(this.contextMap[id]._windowInterval)
    },
    stop() {
        for (var key in this.contextMap) {
            this.contextMap[key].status = 'STOP'
            window.clearInterval(this.contextMap[key]._windowInterval)
        }
        this._saveContext()
    },
    clear() {
        this.stop();
        this.contextMap = {}
        this._saveContext()
    },
    runContext(context) {
        var startTime = $.format.parseDate(context.startTime).date
        var endTime;
        if(context.endTime) {
            endTime = $.format.parseDate(context.endTime).date
        } else {
            endTime = new Date()
        }
        var fetchEndTime = new Date(startTime.getTime() + 1000 * 60 * 60 * 24); //1天
        context.fetchEndTime = $.format.date(fetchEndTime, 'yyyy-MM-dd HH:mm:ss')
        sendMessage("orderFetchMsg", "开始执行订单同步：" + $.format.date(startTime, 'yyyy-MM-dd HH:mm:ss') + ' - ' +
            $.format.date(fetchEndTime, 'yyyy-MM-dd HH:mm:ss') + ' ' + PAY_TYPE[context.payStatus] + " " + QUERY_TYPE[context.queryType]
            , LOG_LEVEL.INFO);
        this._fetch({
            startTime: startTime,
            endTime: fetchEndTime,
            payStatus: context.payStatus,
            queryType: context.queryType
        })
        context.startTime = $.format.date(fetchEndTime, 'yyyy-MM-dd HH:mm:ss')
        return endTime.getTime() > fetchEndTime.getTime()
    },
    _fetch(params) {
        var url = "http://pub.alimama.com/report/getTbkPaymentDetails.json?DownloadID=DOWNLOAD_REPORT_INCOME_NEW&queryType={queryType}" +
            "&payStatus={payStatus}&startTime={startTime}&endTime={endTime}"
        url = url.replace(/{(.+?)}/gi, function($0, $1) {
            return params[$1]
        })
        var xhr          = new XMLHttpRequest();
        xhr.open("get", url, true);
        xhr.responseType = "blob";
        xhr.onload = function() {
            if (this.status == 200) {
                var formdata = new FormData();
                formdata.append("file", this.response, new Date().getTime() + ".xls");
                $.ajax({
                    type:'POST',
                    url: _config.serverUrl + '/api/taobao-union/import',
                    data: formdata,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    mimeType: "multipart/form-data",
                    success:function(data){
                        sendMessage("orderFetchMsg", "订单导入成功：" + $.format.date(params.startTime, 'yyyy-MM-dd HH:mm:ss') + ' - ' +
                            $.format.date(params.endTime, 'yyyy-MM-dd HH:mm:ss') + ' ' + PAY_TYPE[params.payStatus] + " " + QUERY_TYPE[params.queryType]
                            +"，共导入订单" + data.totalNum, LOG_LEVEL.INFO);
                    },
                    fail(res) {
                        sendMessage("orderFetchMsg", "订单导入出错：" + $.format.date(params.startTime, 'yyyy-MM-dd HH:mm:ss') + ' - ' +
                            $.format.date(params.endTime, 'yyyy-MM-dd HH:mm:ss') + ' ' + PAY_TYPE[params.payStatus] + " "
                            + QUERY_TYPE[params.queryType] + JSON.stringify(res), LOG_LEVEL.ERROR);
                    }
                });
            } else {

            }
        };
        xhr.send();
        // var blob = new Blob();
        // url = URL.createObjectURL(blob);
        // chrome.downloads.download({
        //     url: url // The object URL can be used as download URL
        //     //...
        // }, function() {
        //     console.info(blob)
        // });
        // // chrome.downloads.download({
        // //     url: url,
        // //     filename: 'tkorder-' + new Date().getTime() + '.xls',
        // //     saveAs: false,
        // //     conflictAction: 'uniquify'
        // // }, function(id) {
        // //
        // //     chrome.downloads.search({
        // //         id: id
        // //     }, function(item) {
        // //         // console.info(item)
        // //     })
        // // })
    },
    _saveContext() {
        localStorage.setItem("_ORDER_GRABBER_CONTEXT_" , JSON.stringify(this.contextMap))
    }
}
// chrome.downloads.onCreated.addListener(function(item) {
//     console.info(item)
// })