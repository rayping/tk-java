var Utils = {
    serializeToObject: function (form) {
        if (!form.is("form")) {
            return {};
        }
        var data = {};
        var arrays = form.serializeArray();
        $.each(arrays, function (i, n) {
            var key = n.name;
            var value = n.value;
            var _value = data[key];
            if (_value === undefined) {
                data[key] = value;
                return;
            }
            if ($.isArray(_value)) {
                _value.push(value);
            } else {
                data[key] = [_value, value];
            }
        });
        return data;
    }
}