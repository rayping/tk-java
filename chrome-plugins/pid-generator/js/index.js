var bg = chrome.extension.getBackgroundPage();
function checkLogin(flag) {
    bg.checkLogin().done(function() {
        $("#actionBar").fadeIn();
    }).fail(function() {
        flag && alert("云喆淘客服务还未登录，请先登录")
    })
}
$(function(){

    $("#saveSetting").click(function() {
        bg.config({
            serverUrl: $("#serverUrl").val()
        })
        checkLogin(true)
    })
    var config = bg.config()
    $("#serverUrl").val(config.serverUrl)
    checkLogin()
})