package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.settings.FlashSaleSetting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/flashSaleItemSetting")
public class FlashSaleItemSettingController {

    @Inject
    private SystemSettingService systemSettingService;

    @GetMapping("/seasonList")
    public Object seasonList() {

        FlashSaleSetting flashSaleSetting = systemSettingService.findActiveSetting(FlashSaleSetting.class);

        return flashSaleSetting.getSeasonList();

    }

}
