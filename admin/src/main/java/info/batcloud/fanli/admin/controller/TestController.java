package info.batcloud.fanli.admin.controller;

import com.aliyuncs.exceptions.ClientException;
import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.dto.CommissionItemSearchDTO;
import info.batcloud.fanli.core.entity.ChannelCommissionItem;
import info.batcloud.fanli.core.enums.CommissionItemSort;
import info.batcloud.fanli.core.repository.ChannelCommissionItemRepository;
import info.batcloud.fanli.core.service.ChannelCommissionItemService;
import info.batcloud.fanli.core.service.CommissionItemSearchService;
import info.batcloud.fanli.core.service.CommissionItemService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/test")
public class TestController {

    @Inject
    private CommissionItemSearchService commissionItemSearchService;

    @Inject
    private CommissionItemService commissionItemService;

    @Inject
    private ChannelCommissionItemRepository channelCommissionItemRepository;

    @Inject
    private ChannelCommissionItemService channelCommissionItemService;

    @GetMapping("/push")
    public Object push() throws ClientException {
        CommissionItemSearchService.SearchParam param = new CommissionItemSearchService.SearchParam();
        param.setChannelIds("11");
        param.setSort(CommissionItemSort.PRICE_DESC);
        int page = 1;
        while (true) {
            param.setPage(page++);
            Paging<CommissionItemSearchDTO> paging =  commissionItemSearchService.search(param);
            for (CommissionItemSearchDTO searchBO : paging.getResults()) {
                ChannelCommissionItem ci = new ChannelCommissionItem();
                ci.setChannelId(12l);
                ci.setCommissionItemId(searchBO.getId());
                channelCommissionItemRepository.save(ci);
            }
            if(paging.getResults().size() <= 0) {
                break;
            }
        }
        this.channelCommissionItemService.deleteByChannelId(11l);
        return 1;
    }

}
