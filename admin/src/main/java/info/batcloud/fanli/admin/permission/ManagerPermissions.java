package info.batcloud.fanli.admin.permission;

import info.batcloud.fanli.core.settings.*;

import java.util.ArrayList;
import java.util.List;

public enum ManagerPermissions implements PermissionItem {

    NONE("无权限", null),
    OPERATION_CENTER("运营管理", null),
    ADV_MANAGE("广告管理", OPERATION_CENTER),
    ARTICLE_MANAGE("素材管理", OPERATION_CENTER),
    MOBILE_PUSH("消息推送", OPERATION_CENTER),
    CHANNEL_MANAGE("频道管理", OPERATION_CENTER),
    ITEM_SELECTION_MANAGE("选品库管理", OPERATION_CENTER),
    INDEX_SETTING("首页配置", OPERATION_CENTER, IndexBannerSetting.class, IndexAdvSetting.class,
            IndexFloorSetting.class, IndexItemShowSetting.class, IndexQuickNavSetting.class),
    BOOT_PAGE_SETTING("引导页配置", OPERATION_CENTER, BootPageSetting.class),
    INVITE_GALLERY_SETTING("邀请图集配置", OPERATION_CENTER, RegisterSetting.class),
    HOT_SEARCH_SETTING("热搜配置", OPERATION_CENTER, HotSearchSetting.class),
    OPERATION("操作中心", OPERATION_CENTER),
    USER_AUTHORITY_MANAGE("用户权限管理", OPERATION_CENTER),
    INTEGRAL_SETTING("积分配置", OPERATION_CENTER, IntegralSetting.class),
    OSS_MANAGE("文件管理", OPERATION_CENTER),

    ACTIVITY_MANAGE("活动管理", null),
    RED_PACKET_MANAGE("红包管理", ARTICLE_MANAGE),
    CROWD_MANAGE("人群管理", ARTICLE_MANAGE),
    MATERIAL_MANAGE("物料管理", ARTICLE_MANAGE),

    ORDER_MANAGER("订单管理", null),
    SELLER_PROMOTION_ITEM_DEPOSIT_ORDER_MANAGE("PLUS专区充值管理", ORDER_MANAGER),
    COMMISSION_ORDER_MANAGE("淘客订单管理", ORDER_MANAGER),
    USER_COMMISSION_ORDER_MANAGE("分佣订单管理", ORDER_MANAGER),
    FREE_CHARGE_ACTIVITY_ORDER_MANAGE("免单活动订单", ORDER_MANAGER),
    SELLER_PROMOTION_ITEM_ORDER_MANAGE("PLUS专区订单", ORDER_MANAGER),

    USER_CENTER("用户中心", null),
    USER_MANAGE("用户管理", USER_CENTER),
    AGENT_MANAGE("代理管理", USER_CENTER),
    AGENT_APPLY_MANAGE("申请管理", AGENT_MANAGE),
    RELATION_AGENT_MANAGE("服务商管理", AGENT_MANAGE),
    CITY_AGENT_MANAGE("市代管理", AGENT_MANAGE),
    DISTRICT_AGENT_MANAGE("区代管理", AGENT_MANAGE),

    SELLER_CENTER("卖家中心", null),
    SELLER_SERVICE_MANAGE("卖家服务", SELLER_CENTER),
    SELLER_SERVICE_ITEM_MANAGE("服务管理", SELLER_CENTER),


    ITEM_MANAGE("商品管理", null),
    COMMISSION_ITEM_MANAGE("优惠券管理", ITEM_MANAGE),
    FLASH_SALE_ITEM_MANAGE("限时抢购商品管理", ITEM_MANAGE),
    SELLER_PROMOTION_ITEM_MANAGE("PLUS专区商品管理", ITEM_MANAGE),

    FINANCE_CENTER("财务中心", null),
    USER_WITHDRAW_MANAGE("提现管理", FINANCE_CENTER),
    USER_COMMISSION_EARNING("会员佣金收益", FINANCE_CENTER),

    SYSTEM_MANAGE("系统管理", null),
    TAOBAO_MATERIAL_MANAGE("淘宝物料管理", SYSTEM_MANAGE),
    SHOPCAT_MANAGE("店铺分类", SYSTEM_MANAGE),
    MANAGER_CENTER("账号管理", SYSTEM_MANAGE),
    MANAGER_MANAGE("管理员管理", MANAGER_CENTER),
    MANAGER_ROLE_MANAGE("角色管理", MANAGER_CENTER),
    TAOBAO_PID_MANAGE("淘宝PID管理", SYSTEM_MANAGE),

    CONFIG_MANAGE("配置管理", null),
    BUNDLE_JS("BundleJs管理", CONFIG_MANAGE),
    TAOBAO_MATERIAL_SETTING("淘宝物料配置", CONFIG_MANAGE),
    ItemSelectionSetting("选品配置", CONFIG_MANAGE),
    COMMISSION_ALLOT_SETTING("佣金配置", CONFIG_MANAGE, CommissionAllotSetting.class),
    COMMISSION_SETTLEMENT_SETTING("佣金结算配置", CONFIG_MANAGE, CommissionSettlementSetting.class),
    SIGN_SETTING("签到配置", CONFIG_MANAGE, SignSetting.class),
    REGISTER_SETTING("注册配置", CONFIG_MANAGE, RegisterSetting.class),
    TAOBAO_UNION_SETTING("淘宝联盟配置", CONFIG_MANAGE, TaobaoUnionSetting.class),
    TBK_APP_SETTING("联盟应用配置", CONFIG_MANAGE, TbkAppSetting.class),
    JD_UNION_SETTING("京东联盟配置", CONFIG_MANAGE, JdUnionSetting.class),
    DDK_SETTING("多多客配置", CONFIG_MANAGE, DdkSetting.class),
    USER_UPGRADE_SETTING("会员升级配置", CONFIG_MANAGE, UserUpgradeSetting.class),
    SELLER_PROMOTION_ITEM_SETTING("PLUS专区配置", CONFIG_MANAGE,
            SellerPromotionItemSetting.class),
    FLASH_SALE_SETTING("限时抢购配置", CONFIG_MANAGE, FlashSaleSetting.class),
    CUSTOMER_SERVICE_SETTING("客服配置", CONFIG_MANAGE, CustomerServiceSetting.class),
    WEIXIN_SETTING("微信配置管理", CONFIG_MANAGE, WeixinSetting.class),

    OPEN_AUTH_MANAGE("平台授权管理", CONFIG_MANAGE),
    TBK_SESSION_SETTING("淘宝客SESSION", CONFIG_MANAGE),
    JD_AUTH_SETTING("京东授权管理", OPEN_AUTH_MANAGE, JdAuthSetting.class),
    DDK_AUTH_SETTING("多多客授权管理", OPEN_AUTH_MANAGE, DdkAuthSetting.class),
    CONTEXT_SETTING("CONTEXT配置", CONFIG_MANAGE),
    JD_UNION_CONTEXT_SETTING("京东联盟CONTEXT配置", CONTEXT_SETTING, JdUnionContextSetting.class),
    DDK_CONTEXT_SETTING("多多客CONTEXT配置", CONTEXT_SETTING, DdkContextSetting.class),
    SMS_SETTING("短信配置", CONFIG_MANAGE, SmsSetting.class),
    ALIPAY_SETTING("支付宝配置", CONFIG_MANAGE, AlipaySetting.class),
    URL_MAPPING_SETTING("页面映射配置", CONFIG_MANAGE, UrlMappingSetting.class),
    URL_SCHEME_SETTING("URL_SCHEME配置", CONFIG_MANAGE, UrlSchemeSetting.class),
    BASE_SETTING("基础配置", CONFIG_MANAGE, BaseSetting.class),
    WITHDRAW_SETTING("提现管理", CONFIG_MANAGE, WithdrawSetting.class),
    REGION_SYNC("地区同步", CONFIG_MANAGE);

    public String name;
    public String parentId;
    public String id;
    public List<PermissionItem> children;

    public Class[] settingTypes;

    ManagerPermissions(String name, PermissionItem parent) {
        this.name = name;
        this.id = this.name();

        if (parent != null) {
            this.parentId = parent.getId();
        }
        this.children = new ArrayList<>();
    }

    ManagerPermissions(String name, PermissionItem parent, Class... settingTypes) {
        this(name, parent);
        this.settingTypes = settingTypes;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getParentId() {
        return parentId;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Class[] settingTypes() {
        return this.settingTypes;
    }


    @Override
    public List<PermissionItem> getChildren() {
        return children;
    }

    @Override
    public void addChild(PermissionItem item) {
        this.children.add(item);
    }
}
