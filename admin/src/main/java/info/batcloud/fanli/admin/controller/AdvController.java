package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.AdvService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/adv")
@Permission(ManagerPermissions.ADV_MANAGE)
public class AdvController {

    @Inject
    private AdvService advService;

    @GetMapping("/search")
    public Object search(AdvService.SearchParam param) {

        return advService.search(param);
    }

    @PostMapping("")
    public Object save(AdvService.AdvAddParam param) {
        advService.saveAdv(param);
        return 1;
    }

    @PutMapping("/{id}")
    public Object update(@PathVariable long id, AdvService.AdvUpdateParam param) {
        param.setId(id);
        advService.updateAdv(param);
        return 1;
    }

    @PutMapping("/valid/{id}-{flag}")
    public Object valid(@PathVariable long id, @PathVariable boolean flag) {
        if(flag) {
            advService.validById(id);
        } else {
            advService.invalidById(id);
        }
        return 1;
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable long id) {
        advService.deleteById(id);
        return 1;
    }

}
