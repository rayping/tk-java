package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.enums.UserServiceSubscriptionStatus;
import info.batcloud.fanli.core.service.UserServiceSubscriptionService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/userServiceSubscription")
public class UserServiceSubscriptionController {

    @Inject
    private UserServiceSubscriptionService userServiceSubscriptionService;

    @GetMapping("/search")
    public Object search(UserServiceSubscriptionService.SearchParam param) {
        return userServiceSubscriptionService.search(param);
    }

    @PostMapping
    public Object save(UserServiceSubscriptionService.UserServiceSubscriptionAddParam param) {

        userServiceSubscriptionService.addUserServiceSubscription(param);
        return 1;
    }

    @PutMapping("/status/{id}-{status}")
    public Object setStatus(@PathVariable long id, @PathVariable UserServiceSubscriptionStatus status) {
        userServiceSubscriptionService.setStatus(id, status);
        return 1;
    }

}
