package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.service.FreeChargeActivityOrderService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/api/free-charge-activity-order")
public class FreeChargeActivityOrderController {

    @Inject
    private FreeChargeActivityOrderService freeChargeActivityOrderService;

    @GetMapping("/search")
    public Object search(FreeChargeActivityOrderService.SearchParam param) {
        return freeChargeActivityOrderService.search(param);
    }

    @PostMapping("/settle")
    public Object settle(SettleForm form) {
        freeChargeActivityOrderService.settle(form.getIdList());
        return true;
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable long id) {
        freeChargeActivityOrderService.deleteById(id);
        return true;
    }

    public static class SettleForm {
        private List<Long> idList;

        public List<Long> getIdList() {
            return idList;
        }

        public void setIdList(List<Long> idList) {
            this.idList = idList;
        }
    }
}
