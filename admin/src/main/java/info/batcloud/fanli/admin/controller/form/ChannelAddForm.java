package info.batcloud.fanli.admin.controller.form;

public class ChannelAddForm {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
