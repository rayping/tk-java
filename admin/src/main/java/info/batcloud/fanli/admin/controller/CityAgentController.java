package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.CityAgentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/cityAgent")
@Permission(ManagerPermissions.CITY_AGENT_MANAGE)
public class CityAgentController {

    @Inject
    private CityAgentService agentService;

    @GetMapping("/search")
    public Object search(CityAgentService.SearchParam param) {

        return agentService.search(param);
    }

    @PostMapping()
    public Object add(CityAgentService.AgentAddParam param) {
        agentService.addAgent(param);
        return 1;
    }

}
