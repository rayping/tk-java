package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.component.LxkClientFactory;
import info.batcloud.laxiaoke.open.request.pdd.PddGoodsCatsGetRequest;
import info.batcloud.laxiaoke.open.response.pdd.PddGoodsCatsGetResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/pdd")
public class PddController {

    @Inject
    private LxkClientFactory lxkClientFactory;

    @GetMapping("/goodsCat/list/{parentId}")
    public Object listItemCat(@PathVariable long parentId) {
        PddGoodsCatsGetRequest req = new PddGoodsCatsGetRequest();
        req.setParentCatId(parentId);
        PddGoodsCatsGetResponse rsp = lxkClientFactory.getLxkClient().execute(req);
        return rsp.getData().getData().getGoodsCatList();
    }

}
