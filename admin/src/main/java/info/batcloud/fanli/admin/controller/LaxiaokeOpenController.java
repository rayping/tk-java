package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.service.DdkService;
import info.batcloud.fanli.core.service.JdUnionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/laxiaoke-open")
public class LaxiaokeOpenController {

    @Inject
    private DdkService ddkService;

    @Inject
    private JdUnionService jdUnionService;

    @GetMapping("/refresh-jd-access-token")
    public Object refreshJdAccessToken() {
        return jdUnionService.refreshAccessToken();
    }

    @GetMapping("/refresh-pdd-access-token")
    public Object refreshPddAccessToken() {
        return ddkService.refreshAccessToken();
    }

}
