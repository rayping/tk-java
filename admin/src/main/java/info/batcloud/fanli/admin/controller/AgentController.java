package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.enums.AgentStatus;
import info.batcloud.fanli.core.service.AgentService;
import info.batcloud.fanli.core.service.RelationAgentService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/agent")
public class AgentController {

    @Inject
    private AgentService agentService;

    @PutMapping("/status/{id}-{status}")
    public Object setStatus(@PathVariable long id, @PathVariable AgentStatus status) {
        agentService.setStatus(id, status);
        return 1;
    }
}
